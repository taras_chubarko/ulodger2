<?php
use Faker\Factory as Faker;
use Geocoder\Laravel\Facades\Geocoder;
/*
 * function setMyLoc
 * @param $arg
 */

function setMyLoc()
{
    $value = session('myloc');
    
    if(is_null($value) || is_null(session('myloc.locality')))
    {
        $geo = geoip()->getLocation();
        $geo = app('geocoder')->reverse($geo['lat'],$geo['lon'])->all();
        $gg = $geo[0];
        $gg = $gg->toarray();
        
        $value = session(['myloc' => [
            'lat' 	    => $gg['latitude'],
            'lng' 	    => $gg['longitude'],
            'locality'      => $gg['locality'],
            'region'        => $gg['adminLevels'][1]['name'],
        ]]);
    }
    
    \JavaScript::put(['myloc' => $value]);
}
/*
 *
 */
function active_route($route, $active = ' active')
{
    if(is_array($route))
    {
        return (in_array(Request::url(), $route)) ? $active : '';
    }
    else
    {
        return ($route == Request::url()) ? $active : '';
    }
}
/*
 * function numsArr
 * @param $arg
 */

function monthArr()
{
    $m = [
        '' => '- выбрать -',
        1 => 'Январь',
        2 => 'Февраль',
        3 => 'Март',
        4 => 'Апрель',
        5 => 'Май',
        6 => 'Июнь',
        7 => 'Июль',
        8 => 'Август',
        9 => 'Сентябрь',
        10 => 'Октябрь',
        11 => 'Ноябрь',
        12 => 'Декабрь',
    ];
    return $m;
}
/*
 * function month2Arr
 * @param $arg
 */

function month2Arr($arg)
{
    $m = [
        1 => 'января',
        2 => 'февраля',
        3 => 'марта',
        4 => 'апреля',
        5 => 'майя',
        6 => 'июня',
        7 => 'июля',
        8 => 'августа',
        9 => 'сентября',
        10 => 'октября',
        11 => 'ноября',
        12 => 'декабря',
    ];
    return $m[$arg];
}
/*
 * function dayArr
 * @param $arg
 */

function dayArr()
{
    $day = [];
    $day[''] = '- выбрать -';
    for ($i = 1; $i <= 31; $i++) {
        $day[$i] = $i;
    }
    return $day;
}
/*
 * function yearArr
 * @param $arg
 */

function yearArr()
{
    $year = [];
    $year[''] = '- выбрать -';
    for ($i = 1950; $i <= date('Y'); $i++) {
        $year[$i] = $i;
    }
    return $year;
}
/*
 * function letterArr
 * @param $arg
 */

function letterArr($arg)
{
    $abc = array(); 
    foreach (range(chr(0xC0),chr(0xDF)) as $v) 
    $abc[] = iconv('CP1251','UTF-8',$v); 
    return $abc[$arg];
}
/* public function googleGeocode
 * @param $id
 *-----------------------------------
 *|
 *-----------------------------------
 */
function googleGeocode($address)
{
    $data['address'] = $address;
    $data['key'] = 'AIzaSyBBziS3HCZ6We5DhI3VdlGfQ4mHF_QvIcs';
    $geo = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?'.http_build_query($data)));
    return $geo;
}
/*
 * function getLocality
 * @param $arg
 */

function getLocality($geocode)
{
    $geo = json_decode(file_get_contents('https://geocode-maps.yandex.ru/1.x/?format=json&kind=locality&results=1&geocode='.$geocode));
    return $geo->response->GeoObjectCollection;
}

/*
 * function getAddress
 * @param $arg
 */

function getAddress($address)
{
    $geo = getLocality($address)->featureMember[0]->GeoObject;
    $point = $geo->Point->pos;
    $point = explode(' ', $point);
    
    $data = new \stdClass;
    $data->country = $geo->metaDataProperty->GeocoderMetaData->AddressDetails->Country->CountryName;
    $data->region = $geo->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->AdministrativeAreaName;
    $data->locality = $geo->name;
    $data->lat = $point[1];
    $data->lng = $point[0];
    
    return $data;
}
/*
 * function rusishData
 * @param $arg
 */

function rusishData($data, $time = false)
{
    $dt = null;
    
    if($data)
    {
        $d = \Carbon::parse($data)->format('d');
        $m = month2Arr(\Carbon::parse($data)->format('n'));
        $y = \Carbon::parse($data)->format('Y');
        
        if($time == true)
        {
            $dt = $d.' '.$m.' '.$y.' '.\Carbon::parse($data)->format('H:i');
        }
        else
        {
            $dt = $d.' '.$m.' '.$y;
        }
    }
    
    return $dt;
}
/*
 * function genAddress
 * @param $arg
 */

function genAddress()
{
    $faker = Faker::create('ru_RU');
    $rand = $faker->randomElement(range(1,716851));
    $street = \DB::table('geo_streets')
        ->where('id', $rand)
        ->first();
    $town =  \DB::table('geo_towns')
        ->where('guid', $street->parentguid)
        ->first();
    if($town)
    {
        $obl =  \DB::table('geo_obl')
        ->where('guid', $town->parentguid)
        ->first();
    }
    
    if(!empty($street) && !empty($town) && !empty($obl))
    {
        $address = [
            'address'   => $obl->shortname.'. '.$obl->name.', '.$town->shortname.'. '.$town->name.', '.$street->shortname.'. '.$street->name,
            'region'    => $obl->name,
            'city'      => $town->name,
            'street'    => $street->name,
            'lat'       => getAddress($obl->shortname.'. '.$obl->name.', '.$town->shortname.'. '.$town->name.', '.$street->shortname.'. '.$street->name)->lat,
            'lng'       => getAddress($obl->shortname.'. '.$obl->name.', '.$town->shortname.'. '.$town->name.', '.$street->shortname.'. '.$street->name)->lng,
        ];
    }
    else
    {
        $address = null;
    }
    return $address;
}
/*
 * function data_get_age
 * @param $arg
 */

function data_get_age($data, $empty = null)
{
    //$god = data_get($data, 'birthday.0.pivot.birthday');
    
    if($data)
    {
        $userBirthday = date('d.m.Y', strtotime($data)); // День рождение юзера
        $birthday = strtotime($userBirthday); // Получаем unix timestamp нашего дня рождения
        $years = date('Y') - date('Y',$birthday); // Вычисляем возраст БЕЗ учета текущего месяца и дня
        $now = time(); // no comments
        $nowBirthday = mktime(0,0,0,date('m',$birthday),date('d',$birthday),date('Y')); // Получаем день рождение пользователя в этом году
        if ($nowBirthday > $now) {
           $years --; // Если дня рождения ещё не было то вычитаем один год
        }
        return $years . num2word($years, array(' год', ' года', ' лет'));
    }
    else
    {
        return $empty;
    }
}
/*
 *
 */
function num2word($num, $words){
    
    $num = $num % 100;
    
    if ($num > 19)
    {
        $num = $num % 10;
    }
    
    switch ($num)
    {
        case 1: 
            return($words[0]);
        break;
            
        case 2: case 3: case 4:
            return($words[1]);
        break;
            
        default:
            return($words[2]);
    }
}
/*
 * function diffDate
 * @param $arg
 */

function diffDate($date)
{
    $dt =\Carbon::parse($date);//->addMonths(5);
    $diff = $dt->diff(\Carbon::now());//->format('%y Year, %m Months and %d Days %H');
    
    $y = num2word($diff->y, array(' год', ' года', ' лет'));
    $m = num2word($diff->m, array(' месяц', ' месяца', ' месяцев'));
    $d = num2word($diff->d, array(' день', ' дня', ' дней'));
    $h = num2word($diff->h, array(' час', ' часа', ' часов'));
    $i = num2word($diff->i, array(' митуна', ' минуты', ' минут'));
    
    $left = $diff->y.' '.$y.' '.$diff->d.' '.$d;
    
    if($diff->y == 0 && $diff->m == 0 && $diff->d == 0 && $diff->h == 0)
    {
        $left = $diff->i.' '.$i;
    }
    
    if($diff->y == 0 && $diff->m == 0 && $diff->d == 0 && $diff->h != 0)
    {
        $left = $diff->h.' '.$h;
    }
    
    if($diff->y == 0 && $diff->m == 0 && $diff->d != 0 && $diff->h != 0)
    {
        $left = $diff->d.' '.$d.' '.$diff->h.' '.$h;
    }
    
    if($diff->y == 0 && $diff->m != 0 && $diff->d != 0 && $diff->h != 0)
    {
        $left = $diff->m.' '.$m.' '.$diff->d.' '.$d;
    }
    
    return $left;
}
/*
 *
 */
function setUserSession()
{
    \DB::table('sessions')
    ->where('id', \Session::getId())
    ->update(['user_id' => \Sentinel::check() ? \Sentinel::getUser()->id : 0]);
}
/*
 * function buttonCreateJobsToUser
 * @param $arg
 * Кнопка Создать заявку исполнителю.
 */

function buttonCreateJobsToUser($user)
{
    $button = null;
    if($user->inRole('executor'))
    if ($currentUser = \Sentinel::check())
    {
        if($currentUser->slug != $user->slug)
        {
            $button = '<div class="user-button"><a href="'.route('user.add.job', $user->id).'" class="create-job-user btn btn-lg btn-primary col-md-12 col-sm-12 col-xs-12">Создать заявку исполнителю</a></div>';
        }
    }
    else
    {
        $button = '<div class="user-button"><button class="btn btn-lg btn-primary col-md-12 col-sm-12 col-xs-12" data-toggle="modal" data-target=".bs-login">Создать заявку исполнителю</button></div>';
    }
    return $button;
}
/*
 * function defaults
 * @param $arg
 */

function defaults($arg, $arg2 = null)
{
    $user = \Sentinel::getUser();
    switch($arg)
    {
        case 'telefonJobAdd':
            
            if($user)
            {
                $user->load('telefon', 'social');
                
                $telefonSocial = data_get($user, 'social.0.pivot.phone');
                
                $telefon = data_get($user, 'telefon.0.pivot.telefon', $telefonSocial);
                
                return $telefon;
                
            }
            else
            {
                return null;
            }
            
        break;
    
        case 'telefon':
            $user = $arg2;
            $telefonSocial = data_get($user, 'social.0.pivot.phone');
            $telefon = data_get($user, 'telefon.0.pivot.telefon', $telefonSocial);
            return $telefon;
        break;
        
        default: return null;
    }
}
/*
 * function buttonBanUser
 * @param $arg
 */

function buttonBanUser($user)
{
    $activation = \Activation::exists($user);
    $status = '<a href="'.route('admin.users.activate', $user->id).'" class="m-r-10 text-success user-activate"><i class="fa fa-check-circle-o" aria-hidden="true"></i></a>';
    if($activation == false)
    {
        $status = '<a href="'.route('admin.users.activate', $user->id).'" class="m-r-10 text-success user-activate"><i class="fa fa-check-circle-o" aria-hidden="true"></i></a>';
    }
    if($activation && $activation->completed == 0)
    {
        $status = '<a href="'.route('admin.users.activate', $user->id).'" class="m-r-10 text-success user-activate"><i class="fa fa-check-circle-o" aria-hidden="true"></i></a>';
    }
    
    if ($activation = \Activation::completed($user))
    {
        $status = '<a href="'.route('admin.users.ban', $user->id).'" class="m-r-10 user-ban"><i class="fa fa-unlock-alt" aria-hidden="true"></i></a>';
    }
    else
    {
        $status = '<a href="'.route('admin.users.activate', $user->id).'" class="m-r-10 text-success user-activate"><i class="fa fa-check-circle-o" aria-hidden="true"></i></a>';
    }
    
    if($user->ban->count())
    {
        $status = '<a href="'.route('admin.users.unban', $user->id).'" class="m-r-10 user-unban"><i class="fa fa-unlock" aria-hidden="true"></i></a>';
    }
    
    return $status;
}
/*
 * function buttonSwishRole
 * @param $arg
 */

function buttonSwishRole($user)
{
    if($user->inRole('customer') && $user->inRole('executor') || $user->inRole('admin'))
    {
	if(\Session::has('inRole'))
	{
            
            if(session('inRole') == 'executor')
            {
                return '<li><a href="'.route('user.switchto.role', [$user->id, 'customer']).'">Войти как заказчик</a></li>';
            }
            if(session('inRole') == 'customer')
            {
                return '<li><a href="'.route('user.switchto.role', [$user->id, 'executor']).'">Войти как исполнитель</a></li>';
            }
	}
        return '<li><a href="'.route('user.switchto.role', [$user->id, 'customer']).'">Войти как заказчик</a></li>';
    }
}
/*
 * function title_class
 * @param $arg
 */

function title_class()
{
    $class = 'title';
    if ($user = \Sentinel::check())
    {
	if($user->inRole('admin'))
	{
	    $class = 'title';
	}
	if($user->inRole('moderator'))
	{
	    $class = 'title';
	}
	if($user->inRole('executor'))
	{
	    $class = 'title-yellow';
	}
	if($user->inRole('customer'))
	{
	    $class = 'title';
	}
	if($user->inRole('customer') && $user->inRole('executor'))
	{
	    $class = 'title-yellow';
	    if(\Session::has('inRole'))
	    {
		if(session('inRole') == 'executor')
		{
		    $class = 'title-yellow';
		}
		if(session('inRole') == 'customer')
		{
		    $class = 'title';
		}
	    }
	}
    }
    return $class;
}
/*
 *
 */
function title_class2()
{
    $class = 'title';
    if ($user = \Sentinel::check())
    {
	if($user->inRole('admin'))
	{
	    $class = 'title';
	}
	if($user->inRole('moderator'))
	{
	    $class = 'title';
	}
	if($user->inRole('executor'))
	{
	    $class = 'title';
	}
	if($user->inRole('customer'))
	{
	    $class = 'title-yellow';
	}
	if($user->inRole('customer') && $user->inRole('executor'))
	{
	    $class = 'title';
	    if(\Session::has('inRole'))
	    {
		if(session('inRole') == 'executor')
		{
		    $class = 'title';
		}
		if(session('inRole') == 'customer')
		{
		    $class = 'title-yellow';
		}
	    }
	}
    }
    return $class;
}
/*
 *
 */
function m_class()
{
    $class = '';
    if ($user = \Sentinel::check())
    {
	if($user->inRole('admin'))
	{
	    $class = '';
	}
	if($user->inRole('moderator'))
	{
	    $class = '';
	}
	if($user->inRole('executor'))
	{
	    $class = 'yellow';
	}
	if($user->inRole('customer'))
	{
	    $class = '';
	}
	if($user->inRole('customer') && $user->inRole('executor'))
	{
	    $class = 'yellow';
	    if(\Session::has('inRole'))
	    {
		if(session('inRole') == 'executor')
		{
		    $class = 'yellow';
		}
		if(session('inRole') == 'customer')
		{
		    $class = '';
		}
	    }
	}
    }
    return $class;
}

/*
 * function default_job
 * @param $arg
 */

function default_job($field = null)
{
    $job = session('same');
    switch($field)
    {
        case 'category[0]':
            if($job)
            {
                return $job->fieldCategory->categoryId;
            }
            if(\Request::segment(1) == 'add' && \Request::segment(2) == 'jobs' && !empty(\Request::segment(3)))
            {
                $term = \TaxonomyTerm::findBySlug(\Request::segment(3));
                return $term->id;
            }
            return null;
        break;
        //
        case 'category[1]':
            if($job)
            {
                return $job->fieldCategory->subcategoryId;
            }
            return null;
        break;
        //
        case 'name':
            if($job)
            {
                return $job->name;
            }
            return null;
        break;
        //
        case 'body':
            if($job)
            {
                return data_get($job, 'body.0.pivot.body');
            }
            return null;
        break;
        //
        case 'price':
            if($job)
            {
                return data_get($job, 'price.0.pivot.amount');
            }
            return null;
        break;
        
        //
        default: return null;
    }
}
/*
 * function filter_data
 * @param $arg
 */

function filter_data()
{
    $filter = '- любая -';
    if(\Request::has('date'))
    {
        if(\Request::input('date.0') && \Request::input('date.1'))
        {
            $filter = 'от '.\Request::input('date.0').' до '.\Request::input('date.1');
        }
    }
    return $filter;
}
/*
 * function filter_price
 * @param $arg
 */

function filter_price()
{
    $filter = '- любая -';
    if(\Request::has('price'))
    {
        if(\Request::input('price.0') && \Request::input('price.1'))
        {
            $filter = 'от '.\Request::input('price.0').' до '.\Request::input('price.1');
        }
    }
    return $filter;
}
/*
 * function filter_locality
 * @param $arg
 */

function filter_locality()
{
    $region = (\Request::get('region')) ? \Request::get('region') : session('myloc.region');
    $locality = (\Request::get('locality')) ? \Request::get('locality') : session('myloc.locality');
    return $region.'<br>'.$locality;
}
/*
 * function lastAddress
 * @param $arg
 */

function lastAddress($field = null, $current = null)
{
    if(\Request::segment(1) == 'add' && \Request::segment(2) == 'jobs')
    {
        if($user = \Sentinel::getUser())
        {
            $user->load('jobs');
            if($user->jobs->count())
            {
                $job = $user->jobs->last();
                $job->load('address');
                $address = $job->address->first();
                $data = data_get($address, 'pivot.'.$field);
                return $data;
            }
            return null;
        }
        return null;
    }
    else
    {
        return $current;
    }
}

/*
 * function default_telefon
 * @param $arg
 */

function default_telefon()
{
    if ($user = \Sentinel::check())
    {
        $telefon = data_get($user, 'telefon.0.pivot.telefon');
    }
    else
    {
        $telefon = null;
    }
    return $telefon;
}

/*
 * function default_email
 * @param $arg
 */

function default_email()
{
    if ($user = \Sentinel::check())
    {
        $email = $user->email;
    }
    else
    {
        $email = null;
    }
    return $email;
}

/* public function default_field
 * @param $id
 *-----------------------------------
 *|
 *-----------------------------------
 */
function default_field($job, $field, $val = null)
{
    if($job)
    {
        if($field->type == 'text')
        {
            $table = \DB::table('field_'.$field->name)->where('entity', 'jobs')->where('entity_id', $job->id)->first();
            $table = collect($table)->toArray();
            return $table[$field->name];
        }
        
        if($field->type == 'radio' || $field->type == 'checkbox')
        {
            $table = \DB::table('field_'.$field->name)->where('entity', 'jobs')->where('entity_id', $job->id)->where($field->name, $val)->first();
            if($table)
            {
                return 'checked';
            }
        }
        
        if($field->type == 'select' && $field->multiple == 0)
        {
            $table = \DB::table('field_'.$field->name)->where('entity', 'jobs')->where('entity_id', $job->id)->first();
            $table = collect($table)->toArray();
            return $table[$field->name];
        }
        
        if($field->type == 'select' && $field->multiple == 1)
        {
            $table = \DB::table('field_'.$field->name)->where('entity', 'jobs')->where('entity_id', $job->id)->get();
            //$table = collect($table)->toArray();
            //return $table[$field->name];
            $arr = [];
            foreach($table as $tab)
            {
                $tab = collect($tab)->toArray();
                $arr[$tab[$field->name]] = $tab[$field->name];
            }
            return $arr;
        }
        
    }
    return null;
}

/*
 * function task_link
 * @param $arg
 */

function task_link($user)
{
    $link = '#!';
    if ($user = \Sentinel::check())
    {
	if($user->inRole('admin'))
	{
	    $link = route('user.task', [$user->id, 'active']);
	}
	if($user->inRole('moderator'))
	{
	    $link = route('user.task', [$user->id, 'active']);
	}
	if($user->inRole('executor'))
	{
	    $link = route('user.task', [$user->id, 'executable']);
	}
	if($user->inRole('customer'))
	{
	    $link = route('user.task', [$user->id, 'active']);
	}
	if($user->inRole('customer') && $user->inRole('executor'))
	{
	    if(\Session::has('inRole'))
	    {
		if(session('inRole') == 'executor')
		{
		    $link = route('user.task', [$user->id, 'executable']);
		}
		if(session('inRole') == 'customer')
		{
		    $link = route('user.task', [$user->id, 'active']);
		}
	    }
	}
    }
    return $link;
}

function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
    $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
    $str_end = "";
    if ($lower_str_end) {
      $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
    }
    else {
      $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
    }
    $str = $first_letter . $str_end;
    return $str;
}

/* public function user_link
 * @param $id
 *-----------------------------------
 *|
 *-----------------------------------
 */
function user_link($user)
{
    $link = '#!';
    
    if($user->inRole('admin'))
    {
        $link = route('user.slug.customer', $user->slug);
    }
    if($user->inRole('moderator'))
    {
        $link = route('user.slug.customer', $user->slug);
    }
    if($user->inRole('executor'))
    {
        $link = route('user.slug.executor', $user->slug);
    }
    if($user->inRole('customer'))
    {
        $link = route('user.slug.customer', $user->slug);
    }
    if($user->inRole('customer') && $user->inRole('executor'))
    {
        if(\Session::has('inRole'))
        {
            if(session('inRole') == 'executor')
            {
                $link = route('user.slug.executor', $user->slug);
            }
            if(session('inRole') == 'customer')
            {
                $link = route('user.slug.customer', $user->slug);
            }
        }
    }
    return $link;
}
/*
 *
 */
function paginateCollection($collection, $perPage, $pageName = 'path', $fragment = null)
{
    $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
    $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
    parse_str(request()->getQueryString(), $query);
    unset($query[$pageName]);
    $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
        $currentPageItems,
        $collection->count(),
        $perPage,
        $currentPage,
        [
            'pageName' => $pageName,
            'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
            'query' => $query,
            'fragment' => $fragment
        ]
    );

    return $paginator;
}

/*
 * function current_role
 * @param $arg
 */

function current_role()
{
    $role = 'guest';
    if ($user = \Sentinel::check())
    {
        if($user->inRole('customer') && $user->inRole('executor'))
	{
	    if(\Session::has('inRole'))
	    {
		if(session('inRole') == 'executor')
		{
		   $role = 'executor';
		}
		if(session('inRole') == 'customer')
		{
		    $role = 'customer';
		}
	    }
	}
	elseif($user->inRole('admin'))
	{
	    $role = 'admin';
	}
	elseif($user->inRole('moderator'))
	{
            $role = 'moderator';
	}
	elseif($user->inRole('executor'))
	{
	    $role = 'executor';
	}
	elseif($user->inRole('customer'))
	{
	    $role = 'customer';
	}
    }
    return $role;
}
/*
 *
 */
function current_role_user($user)
{
    $role = 'guest';
    if($user->inRole('customer') && $user->inRole('executor'))
	{
	    if(\Session::has('inRole'))
	    {
		if(session('inRole') == 'executor')
		{
		   $role = 'executor';
		}
		if(session('inRole') == 'customer')
		{
		    $role = 'customer';
		}
	    }
	}
	elseif($user->inRole('admin'))
	{
	    $role = 'admin';
	}
	elseif($user->inRole('moderator'))
	{
            $role = 'moderator';
	}
	elseif($user->inRole('executor'))
	{
	    $role = 'executor';
	}
	elseif($user->inRole('customer'))
	{
	    $role = 'customer';
	}
    return $role;
}

/*
 * function account_active
 * @param $arg
 */

function account_active($user = null)
{
    if($user)
    {
        $arr[] = route('account');
        $arr[] = route('account.payment.methods', $user->id);
        $arr[] = route('account.payout.preferences', $user->id);
        $arr[] = route('account.transaction.history', $user->id);
        $arr[] = route('account.privacy', $user->id);
        $arr[] = route('account.security', $user->id);
        $arr[] = route('account.settings', $user->id);
        if(in_array(Request::url(), $arr))
        {
            return 'class=active';
        }
    }
    else
    {
        return (route('account') == Request::url()) ? 'class=active' : '';
    }
}

/*
 * function account_active
 * @param $arg
 */

function profile_active($user = null)
{
    if($user)
    {
        $arr[] = route('profile');
        $arr[] = route('profile.photos', $user->id);
        $arr[] = route('profile.verification', $user->id);
        $arr[] = route('profile.reviews', $user->id);
        $arr[] = route('profile.references', $user->id);
        if(in_array(Request::url(), $arr))
        {
            return 'class=active';
        }
    }
    else
    {
        return (route('profile') == Request::url()) ? 'class=active' : '';
    }
}
/*
 * function month_array
 * @param $arg
 */

function month_array()
{
    $months = array(
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July ',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December',
    );
    return $months;
}
/*
 *
 */
function random_number($size = 4)
{
    $random_number='';
    $count=0;
    while ($count < $size ) 
        {
            $random_digit = mt_rand(0, 9);
            $random_number .= $random_digit;
            $count++;
        }
    return $random_number;  
}

/*
 * function lang_collent
 * @param $arg
 */

function lang_collent()
{
    $lang = collect(config('lang'));
    $lang = $lang->chunk(2);
    
    return $lang;
}
/*
 * function current_step
 * @param $arg
 */

function current_step()
{
    $step = 0;
    if(session('hostStep1'))
    {
        $step = 0;
    }
    
    if(session('hostStep2'))
    {
        $step = 1;
    }
    
    return $step;
}

/*
 * function slugsArr
 * @param $arg
 */

function slugsArr()
{
    //about|delivery|news|forum|faq|contact|zapchast/kato
    $slug = [];
    $slug[] = 'about';
    $slug[] = 'contact';
    $slug[] = 'operations';
    $slug[] = 'host';
    $slug[] = 'login';
    $slug[] = 'dashboard';
    $slug[] = 'sign-up';
    $slug[] = 'forgot-password';
    $slug[] = 'account';
    $slug[] = 'account/payment-methods';
    $slug[] = 'account/privacy';
    $slug[] = 'find/hosts';
    $slug[] = 'profile';
    $slug[] = 'yuor-listing';
    $slug[] = 'inbox';
    $slug[] = 'profile/photos';
    $slug[] = 'profile/verification';
    $slug[] = 'profile/reviews';
    $slug[] = 'profile/references';
    $slug[] = 'account/settings';
    $slug[] = 'account/security';
    $slug[] = 'yuor-reservations';
    $slug[] = 'reservation-requests';
    $slug[] = 'add/host';
    $slug[] = 'add/host/step-2';
    $slug[] = 'add/host/step-3';
    $slug[] = 'add/host/step-4';
    $slug[] = 'add/host/step-5';
    $slug[] = 'add/host/step-6';
    $slug[] = 'add/host/step-7';
    $slug[] = 'add/host/finish';
    //
    $users = \Modules\User\Entities\User::all();
    foreach($users as $user)
    {
        $slug[] = 'user/'.$user->slug;
    }
    
    $reminders = \DB::table('reminders')->get();
    foreach($reminders as $reminder)
    {
        $slug[] = 'reminder/'.$reminder->code;
    }
    
    $hosts = \Host::all();
    foreach($hosts as $host)
    {
        $slug[] = 'host/'.$host->slug;
        $slug[] = 'host/'.$host->slug.'/overview';
        $slug[] = 'edit/host/'.$host->slug.'/step-1';
        $slug[] = 'edit/host/'.$host->slug.'/step-2';
        $slug[] = 'edit/host/'.$host->slug.'/step-3';
        $slug[] = 'edit/host/'.$host->slug.'/step-4';
        $slug[] = 'edit/host/'.$host->slug.'/step-5';
        $slug[] = 'edit/host/'.$host->slug.'/step-6';
        $slug[] = 'edit/host/'.$host->slug.'/step-7';
    }
    
    //dd($slug);
    
    return implode('|', $slug);
    
}

/* public function dist
 * @param $id
 *-----------------------------------
 *|
 *-----------------------------------
 */
function distance($lat, $lng, $distance)
{
    $ids = \DB::select('SELECT host_id, ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians( lat ) ) ) ) AS distance FROM host_location HAVING distance < '.$distance.' ORDER BY distance LIMIT 0 , 20;');
    $ids = collect($ids)->pluck('host_id')->toArray();
    return $ids;
}

/* public function setAliases
    * @param $id
    *-----------------------------------
    *|
    *-----------------------------------
    */
    function setAliases()
   {
        if(\Request::segment(1) == 'admin')
        {
           return 'admin/*';
        }
        elseif(\Request::segment(1) == 'api')
        {
            return 'api/*';
        }
        elseif(\Request::segment(1) == 'test')
        {
            return 'test/*';
        }
        elseif(\Request::segment(1) == 'billing')
        {
            return 'billing/*';
        }
        elseif(\Request::segment(1) == 'verify')
        {
           return 'verify/*';
        }
        elseif(\Request::segment(1) == 'socialite')
        {
            return 'socialite/*';
        }
        else
        {
           return '[\/\w\.-]*';
        }
   }

function get_furl($url)
{
    $furl = false;

    // First check response headers
    $headers = get_headers($url);

    // Test for 301 or 302
    if(preg_match('/^HTTP\/\d\.\d\s+(301|302)/',$headers[0]))
    {
        foreach($headers as $value)
        {
            if(substr(strtolower($value), 0, 9) == "location:")
            {
                $furl = trim(substr($value, 9, strlen($value)));
            }
        }
    }
    // Set final URL
    $furl = ($furl) ? $furl : $url;

    return $furl;
}


