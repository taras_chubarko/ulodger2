<?php namespace Modules\Host\Listeners;

use Illuminate\Http\Request;
use Illuminate\Contracts\Mail\Mailer;

class HostEventListener
{
    public $request;
    protected $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request, Mailer $mailer)
    {
        $this->request = $request;
        $this->mailer = $mailer;
    }
    /* public function calcProcess
     * @param $id
     *-----------------------------------
     *| Калькуляция процеса создания хоста
     *-----------------------------------
     */
    public function calcProcess($event)
    {
        $host = $event->host;
        $host->step = $this->request->current;
        
        $progress = [];
        
        if($host->title)
        {
            $progress[] = 6.25;
        }
        //
        if($host->rentOut->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->propertyType->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->description->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->smoking->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->guests->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->bedrooms->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->beds->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->bathType->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->bathrooms->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->location->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->schoolslocaton->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->amenities->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->spaces->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->photo->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->available->count())
        {
            $progress[] = 6.25;
        }
        //
        if($host->price->count())
        {
            $progress[] = 6.25;
        }
        //
        $host->progress = array_sum($progress);
        $host->save();
        
        
        session(['hostProgress' => $host->progress]);
        
    }
    /* public function calcRating
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function calcRating($event)
    {
        \Host::calcRating($event->id);
    }
    /**
     * Регистрация слушателей для подписки.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Modules\Host\Events\HostCalcEvent',
            'Modules\Host\Listeners\HostEventListener@calcProcess'
        );
        
        $events->listen(
            'Modules\Host\Events\HostCalcRating',
            'Modules\Host\Listeners\HostEventListener@calcRating'
        );
        
       
    }

}