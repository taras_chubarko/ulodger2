<?php namespace Modules\Host\Facades;

use Illuminate\Support\Facades\Facade;

class Host extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Modules\Host\Repositories\HostRepository';
    }
}