<?php

namespace Modules\Host\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Host extends Model implements Transformable
{
    use SoftDeletes;
    use TransformableTrait;
    use Sluggable;
    
    protected $table = 'host';

    protected $fillable = [];
    
    protected $guarded = ['_token', 'meta'];
    
    protected $dates = ['deleted_at'];
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slughost'
            ]
        ];
    }
    /* public function getSlugjobAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getSlughostAttribute()
    {
        $gen = 9 - strlen($this->id);
        $numer = str_pad(mt_rand(0, 999999), $gen, '0', STR_PAD_LEFT).$this->id;
        return 'id-'.$numer;
    }
    /* public function rent_out
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function rentOut()
    {
        return $this->belongsToMany('Modules\Taxonomy\Entities\TaxonomyTerm', 'host_rent_out', 'host_id', 'rent_out')
        ->withPivot('rent_out');  
    }
    /* public function propertyType
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function propertyType()
    {
        return $this->belongsToMany('Modules\Taxonomy\Entities\TaxonomyTerm', 'host_property_type', 'host_id', 'property_type')
        ->withPivot('property_type');  
    }
    /* public function description
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function description()
    {
        return $this->belongsToMany('Modules\Host\Entities\Host', 'host_description')
        ->withPivot('description');
    }
    /* public function smoking
     * @param $id
     *-----------------------------------
     *| host_smoking
     *-----------------------------------
     */
    public function smoking()
    {
        return $this->belongsToMany('Modules\Host\Entities\Host', 'host_smoking')
        ->withPivot('smoking');
    }
    /* public function guests
     * @param $id
     *-----------------------------------
     *| host_guests
     *-----------------------------------
     */
    public function guests()
    {
        return $this->belongsToMany('Modules\Host\Entities\Host', 'host_guests')
        ->withPivot('guests');
    }
    /* public function bedrooms
     * @param $id
     *-----------------------------------
     *| host_bedrooms
     *-----------------------------------
     */
    public function bedrooms()
    {
        return $this->belongsToMany('Modules\Host\Entities\Host', 'host_bedrooms')
        ->withPivot('bedrooms');
    }
    /* public function beds
     * @param $id
     *-----------------------------------
     *| host_beds
     *-----------------------------------
     */
    public function beds()
    {
        return $this->belongsToMany('Modules\Host\Entities\Host', 'host_beds')
        ->withPivot('beds');
    }
    /* public function bathType
     * @param $id
     *-----------------------------------
     *| host_bath_type
     *-----------------------------------
     */
    public function bathType()
    {
        return $this->belongsToMany('Modules\Host\Entities\Host', 'host_bath_type')
        ->withPivot('bath_type');
    }
    /* public function bathrooms
     * @param $id
     *-----------------------------------
     *| host_bathrooms
     *-----------------------------------
     */
    public function bathrooms()
    {
        return $this->belongsToMany('Modules\Host\Entities\Host', 'host_bathrooms')
        ->withPivot('bathrooms');
    }
    /* public function location
     * @param $id
     *-----------------------------------
     *| host_location
     *-----------------------------------
     */
    public function location()
    {
        return $this->belongsToMany('Modules\Host\Entities\Host', 'host_location')
        ->withPivot('address', 'locality', 'region', 'lat', 'lng');
    }
    /* public function schoolslocaton
     * @param $id
     *-----------------------------------
     *| host_schoolslocaton
     *-----------------------------------
     */
    public function schoolslocaton()
    {
        return $this->belongsToMany('Modules\Host\Entities\Host', 'host_schoolslocaton')
        ->withPivot('name', 'address', 'locality', 'region', 'lat', 'lng', 'sort');
    }
    /* public function amenities
     * @param $id
     *-----------------------------------
     *| host_amenities
     *-----------------------------------
     */
    public function amenities()
    {
        return $this->belongsToMany('Modules\Taxonomy\Entities\TaxonomyTerm', 'host_amenities', 'host_id', 'amenities')
        ->withPivot('amenities', 'sort');
    }
    /* public function spaces
     * @param $id
     *-----------------------------------
     *| host_spaces
     *-----------------------------------
     */
    public function spaces()
    {
        return $this->belongsToMany('Modules\Taxonomy\Entities\TaxonomyTerm', 'host_spaces', 'host_id', 'spaces')
        ->withPivot('spaces', 'sort');
    }
    /* public function photo
     * @param $id
     *-----------------------------------
     *| host_photo
     *-----------------------------------
     */
    public function photo()
    {
        return $this->belongsToMany('Modules\Filem\Entities\Filem', 'host_photo', 'host_id', 'fid')
        ->withPivot('fid', 'nameid', 'sort');
    }
    /* public function available
     * @param $id
     *-----------------------------------
     *| host_available
     *-----------------------------------
     */
    public function available()
    {
        return $this->belongsToMany('Modules\Host\Entities\Host', 'host_available')
        ->withPivot('from', 'to');
    }
    /* public function price
     * @param $id
     *-----------------------------------
     *| host_price
     *-----------------------------------
     */
    public function price()
    {
        return $this->belongsToMany('Modules\Host\Entities\Host', 'host_price')
        ->withPivot('price');
    }
    /* public function user
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user()
    {
        return $this->hasOne('Modules\User\Entities\User', 'id', 'user_id');
    }
    /* public function amenities
     * @param $id
     *-----------------------------------
     *| host_amenities
     *-----------------------------------
     */
    public function rating()
    {
        return $this->belongsToMany('Modules\Host\Entities\Host', 'host_rating')
        ->withPivot('reviews', 'rating', 'accuracy', 'communication', 'cleanliness', 'location', 'check_in', 'value');
    }
    /* public function reviews
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function reviews()
    {
        return $this->hasMany('Modules\Reviews\Entities\Reviews');
    }
    /* public function getFieldPhotoAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldPhotoAttribute()
    {
        $field = new \stdClass;
        
        $field->one = ($this->photo->count()) ? $this->photo[0]->filename : 'noim-min.png';
        $field->onebig = ($this->photo->count()) ? $this->photo[0]->filename : 'nophoto3.png';
        
        return $field;
    }
    /* public function getFieldPriceAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldPriceAttribute()
    {
        $field = new \stdClass;
        $field->price = data_get($this, 'price.0.pivot.price');
        $field->normal = number_format($field->price, 0, '.', '');
        return $field;
    }
    /* public function getFieldSmokingAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldSmokingAttribute()
    {
        $field = new \stdClass;
        $field->id = data_get($this, 'smoking.0.pivot.smoking');
        $field->name = config('host.smoking.'.$field->id);
        return $field;
    }
    /* public function getFieldAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldBathTypeAttribute()
    {
        $field = new \stdClass;
        $field->id = data_get($this, 'bathType.0.pivot.bath_type');
        $field->name = config('host.bath_type.'.$field->id);
        return $field;
    }
    /* public function getFieldBathroomsAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldBathroomsAttribute()
    {
        $field = new \stdClass;
        $field->id = number_format(data_get($this, 'bathrooms.0.pivot.bathrooms'), 0, '.', '');
        $field->name = config('host.bathrooms.'.$field->id);
        return $field;
    }
    /* public function getFieldRatingAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldRatingAttribute()
    {
        $field = new \stdClass;
        $field->reviews       = data_get($this, 'rating.0.pivot.reviews', 0);
        $field->rating        = data_get($this, 'rating.0.pivot.rating', 0);
        $field->accuracy      = data_get($this, 'rating.0.pivot.accuracy', 0);
        $field->communication = data_get($this, 'rating.0.pivot.communication', 0);
        $field->cleanliness   = data_get($this, 'rating.0.pivot.cleanliness', 0);
        $field->location      = data_get($this, 'rating.0.pivot.location', 0);
        $field->check_in      = data_get($this, 'rating.0.pivot.check_in', 0);
        $field->value         = data_get($this, 'rating.0.pivot.value', 0);
        return $field;
    }
    /* public function getFieldReviewsAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldReviewsAttribute()
    {
        $field = new \stdClass;
        $field->highest = 0;
        $field->lowest  = 0;
        
        if($this->reviews->count())
        {
            $field->highest = $this->reviews->whereIn('rating', [4,5])->count();
            $field->lowest = $this->reviews->whereIn('rating', [1,2,3])->count();
        }
        return $field;
    }
    
}
