<?php

namespace Modules\Host\Entities;

use Illuminate\Database\Eloquent\Model;

class HostBook extends Model
{
    protected $table = 'host_book';

    protected $fillable = [];
    
    protected $guarded = ['_token', 'meta'];
    
//    protected $attributes = array(
//        'fieldDaysLeft' => '',
//    );
//
//    protected $appends = ['fieldDaysLeft'];
    
    /* public function host
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function host()
    {
        return $this->hasOne('Modules\Host\Entities\Host', 'id', 'host_id'); 
    }
    /* public function user
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user()
    {
        return $this->hasOne('Modules\User\Entities\User', 'id', 'user_id'); 
    }
    /* public function review
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function review()
    {
        return $this->hasOne('Modules\Reviews\Entities\Reviews', 'book_id', 'id'); 
    }
    /* public function getFieldDaysLeftAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldDaysLeftAttribute()
    {
        $now = \Carbon::now();
        $end = \Carbon::parse($this->check_out);
        $left = $end->diff($now);
        
        if($left->invert == 0)
        {
            $days = 0;
        }
        //
        else if($left->invert == 1 && $left->days == 0)
        {
            $days = $left->h.':'.$left->i.' hour';
        }
        else
        {
            $days = $left->days;
        }
        
        return $days;
    }
    
}
