<?php

namespace Modules\Host\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Host\Entities\HostBook;

class ChekBookCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'checkbook';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check complete book.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    //public function __construct()
    //{
    //    parent::__construct();
    //}

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $books = HostBook::where('status', 1)->get();
        foreach($books as $book)
        {
            $now = \Carbon::now();
            $dt = \Carbon::parse($book->check_out);
            if($dt <= $now)
            {
                $book->status = 2;
                $book->save();
            }
        }
        $this->info('Success!');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    //protected function getArguments()
    //{
    //    //return [
    //    //    ['example', InputArgument::REQUIRED, 'An example argument.'],
    //    //];
    //}

    /**
     * Get the console command options.
     *
     * @return array
     */
    //protected function getOptions()
    //{
    //    //return [
    //    //    ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
    //    //];
    //}
}
