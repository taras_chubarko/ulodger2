<?php

namespace Modules\Host\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class HostFindCriteria
 * @package namespace App\Criteria;
 */
class HostFindCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('status', 1)->orderBy('created_at', 'DESC');
        
        //rentOut
        if(\Request::has('rent_out') && !empty(\Request::get('rent_out')) )
        {
            $model = $model->whereHas('rentOut', function($query){
                if(is_array(\Request::get('rent_out')))
                {
                    $query->whereIn('rent_out', \Request::get('rent_out'));
                }
                else
                {
                    $query->where('rent_out', \Request::get('rent_out'));
                }
            });
        }
        //price
        if(\Request::has('price_min') && \Request::has('price_max'))
        {
            $model = $model->whereHas('price', function($query){
                $query->whereBetween('price', [\Request::get('price_min'), \Request::get('price_max')]);
            });
        }
        else
        {
            $model = $model->whereHas('price', function($query){
                $query->whereBetween('price', [50, 5000]);
            });
        }
        
        //bathType
        if(\Request::has('bath_type') && !empty(\Request::get('bath_type')))
        {
            $model = $model->whereHas('bathType', function($query){
                if(is_array(\Request::get('bath_type')))
                {
                    $query->whereIn('bath_type', \Request::get('bath_type'));
                }
                else
                {
                    $query->where('bath_type', \Request::get('bath_type'));
                }

            });
        }
        //beds
        if(\Request::has('rooms'))
        {
            $model = $model->whereHas('beds', function($query){
                $query->where('beds', \Request::get('rooms'));
            });
        }
        //available
        if(\Request::has('move_in') && \Request::has('move_out'))
        {
            $model = $model->whereHas('available', function($query){
                $query->whereDate('from', '<=', \Carbon::parse(\Request::get('move_in')));
                $query->whereDate('to', '>=', \Carbon::parse(\Request::get('move_out')));
            });
        }
        //distance
        if(\Request::has('school') && \Request::has('distance'))
        {
            $ids = $this->distance(\Request::get('lat'), \Request::get('lng'), \Request::get('distance'));
            $model = $model->whereIn('id', $ids);
        }
        
        return $model;
    }
    
    /* public function distance
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function distance($lat, $lng, $distance = 1)
    {
        $ids = \DB::select('SELECT host_id, ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians( lat ) ) ) ) AS distance FROM host_location HAVING distance < '.$distance.' ORDER BY distance LIMIT 0 , 20;');
        $ids = collect($ids)->pluck('host_id')->toArray();
        return $ids;
    }
    
}
