<?php

namespace Modules\Host\Events;

use Illuminate\Queue\SerializesModels;

class HostCalcEvent
{
    use SerializesModels;
    
    public $host;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($host)
    {
        $this->host = $host;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
