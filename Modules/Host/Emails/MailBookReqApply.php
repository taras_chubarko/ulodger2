<?php

namespace Modules\Host\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailBookReqApply extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $user, $host)
    {
        $this->request = $request;
        $this->user = $user;
        $this->host = $host;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $request = $this->request;
        $user = $this->user;
        $host = $this->host;


        return $this->view('site::user.mail-book-req-apply', compact('request','user', 'host'))
            ->to($user->email)
            ->subject('Your Lodging Request Status "'.config('app.name').'"');
    }
}
