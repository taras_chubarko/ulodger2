<?php

namespace Modules\Host\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class HostPM extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $user, $host)
    {
        $this->request = $request;
        $this->user = $user;
        $this->host = $host;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $request = $this->request;
        $user = $this->user;
        $host = $this->host;
        
        $emails = str_replace(' ','',$request->emails);
        $emails = explode(',',$emails);
        
        foreach($emails as $email)
        {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                return $this->view('site::host.mail-personal-message', compact('request', 'user', 'host'))
                ->to($email)
                ->subject('Personal Message "'.config('app.name').'"');
            }
        }
        
    }
}
