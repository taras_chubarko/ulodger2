<?php

namespace Modules\Host\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Host\Repositories\HostRepository;
use Modules\Host\Entities\Host;
use Modules\Host\Entities\HostBook;

/**
 * Class HostRepositoryEloquent
 * @package namespace App\Repositories;
 */
class HostRepositoryEloquent extends BaseRepository implements HostRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Host::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /* public function allSlug
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function allSlug()
    {
        $slugs = $this->model->all();
        
        $items = [];
        
        if($slugs->count())
        {
            foreach($slugs as $slug)
            {
                $items[] = $slug->slug;
            }
            
            $route = implode('|', $items);
            
        }
        else
        {
            $route = '[a-z]+';
        }
        return $route;
    }
    /* public function book($data)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function book($request)
    {
        $user = \Sentinel::getUser();
        $book = HostBook::where('user_id', $request->user_id)
        ->where('host_id', $request->host_id)
        ->where('status', 1)
        ->first();
        //
        $result = new \stdClass;
        //
        if($book)
        {
            $result->status = 'error';
            $result->message = 'You have already booked this lodger.';
        }
        else
        {
            $host = $this->model->find($request->host_id);
            $book = HostBook::create([
                'host_id'       => $host->id,
                'user_id'       => $request->user_id,
                'host_user_id'  => $host->user_id,
                'check_in'      => \Carbon::parse($request->check_in),
                'check_out'     => \Carbon::parse($request->check_out),
                'guests'        => $request->guests,
                'status'        => 1,
            ]);
            
            $result->status = 'success';
            $result->message = 'Lodger booked!';
            $result->book = $book;
        }
        return $result;
    }
    /* public function calcRating
     * @param $id
     *-----------------------------------
     *| Калькуляция рейтинга
     *-----------------------------------
     */
    public function calcRating($id)
    {
        $host = $this->model->find($id);
        $reviews = \Reviews::findWhere(['host_id' => $id, 'status' => 1]);
        //
        $reviews_count = $reviews->count();
        //
        $accuracy_sum = $reviews->pluck('accuracy')->sum();
        $accuracy = $accuracy_sum / $reviews_count;
        //
        $communication_sum = $reviews->pluck('communication')->sum();
        $communication = $communication_sum / $reviews_count;
        //
        $cleanliness_sum = $reviews->pluck('cleanliness')->sum();
        $cleanliness = $cleanliness_sum / $reviews_count;
        //
        $location_sum = $reviews->pluck('location')->sum();
        $location = $location_sum / $reviews_count;
        //
        $check_in_sum = $reviews->pluck('check_in')->sum();
        $check_in = $check_in_sum / $reviews_count;
        //
        $value_sum = $reviews->pluck('value')->sum();
        $value = $value_sum / $reviews_count;
        //
        $rating = ($accuracy + $communication + $cleanliness + $location + $check_in + $value) / 6;
        //
        $host->rating()->detach();
        $host->rating()->attach($host->id, [
            'reviews'       => $reviews_count,
            'rating'        => round($rating),
            'accuracy'      => round($accuracy),
            'communication' => round($communication),
            'cleanliness'   => round($cleanliness),
            'location'      => round($location),
            'check_in'      => round($check_in),
            'value'         => round($value),
        ]);
        
        //dd($accuracy_sum, $communication_sum, $cleanliness_sum, $location_sum, $check_in_sum, $value_sum);
    }
}
