<?php

namespace Modules\Host\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HostRepository
 * @package namespace App\Repositories;
 */
interface HostRepository extends RepositoryInterface
{
    public function allSlug();
    /*
     *
     */
    public function book($request);
    /*
     *
     */
    public function calcRating($id);
}
