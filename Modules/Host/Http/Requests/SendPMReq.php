<?php

namespace Modules\Host\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendPMReq extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['emails']    = 'required';
        $rules['message']   = 'required';
        
        return $rules;
    }
    /*
     *
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        
        $rules = [];
        $rules['emails']    = '"Enter email addresses, separated by commas"';
        $rules['message']   = '"Personal Message"';
        
        $validator->setAttributeNames($rules);
     
        return $validator;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
