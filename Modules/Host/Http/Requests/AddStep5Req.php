<?php

namespace Modules\Host\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddStep5Req extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        //$rules['spaces'] = 'required';
        
        return $rules;
    }
    /*
     *
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        
        $rules = [];
        $rules['spaces'] = '"What spaces can guests use?"';
        
        $validator->setAttributeNames($rules);
     
        return $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
