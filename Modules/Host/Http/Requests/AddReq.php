<?php

namespace Modules\Host\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddReq extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        

            $rules['rent_out']        = 'required';
            $rules['property_type']   = 'required';
            $rules['title']           = 'required';
            $rules['description']     = 'required';

        
        
        return $rules;
    }
    /*
     *
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
     
        $rules = [];
        $rules['rent_out']        = '"I want to rent out"';
        $rules['property_type']   = '"Property Type"';
        $rules['title']           = '"Title"';
        $rules['description']     = '"Description"';
        
        $validator->setAttributeNames($rules);
     
        return $validator;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
