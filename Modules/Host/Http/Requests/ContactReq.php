<?php

namespace Modules\Host\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactReq extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['check_inc']      = 'required';
        $rules['check_outc']     = 'required|different:check_in|after:check_in';
        $rules['guestsc']        = 'required';
        $rules['messagec']       = 'required';
        
        return $rules;
    }
    /*
     *
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        
        $rules = [];
        $rules['check_inc']      = '"Check in"';
        $rules['check_outc']     = '"Check out"';
        $rules['guestsc']        = '"Lodgers"';
        $rules['messagec']       = '"Message host"';
        
        $validator->setAttributeNames($rules);
     
        return $validator;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
