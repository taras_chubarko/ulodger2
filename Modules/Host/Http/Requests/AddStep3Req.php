<?php

namespace Modules\Host\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddStep3Req extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['location.locality']         = 'required';
        $rules['location.address']          = 'required';
        $rules['schoolslocaton.0.address']  = 'required';
        
        return $rules;
    }
    /*
     *
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        
        $rules = [];
        $rules['location.locality']         = '"Locality"';
        $rules['location.address']          = '"Address"';
        $rules['schoolslocaton.0.address']  = '"School name, metro or city"';
        
        $validator->setAttributeNames($rules);
     
        return $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
