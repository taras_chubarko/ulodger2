<?php

namespace Modules\Host\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddStep7Req extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['availablefrom']    = 'required';
        $rules['availableto']      = 'required';
        $rules['price']             = 'required|integer';
        
        return $rules;
    }
    /*
     *
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        
        $rules = [];
        $rules['availablefrom']    = '"My space is available From"';
        $rules['availableto']      = '"My space is available To"';
        $rules['price']             = '"I want to rent this stay for"';
        
        $validator->setAttributeNames($rules);
     
        return $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
