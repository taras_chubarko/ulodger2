<?php

namespace Modules\Host\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddStep2Req extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['smoking']       = 'required';
        $rules['guests']        = 'required';
        $rules['bedrooms']      = 'required';
        $rules['beds']          = 'required';
        $rules['bath_type']     = 'required';
        $rules['bathrooms']     = 'required';
        
        
        return $rules;
    }
    /*
     *
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        
        $rules = [];
        $rules['smoking']       = '"This room is for"';
        $rules['guests']        = '"Number of guests available"';
        $rules['bedrooms']      = '"How many bedrooms can guests use?"';
        $rules['beds']          = '"How many beds can guests use?"';
        $rules['bath_type']     = '"Bath Type"';
        $rules['bathrooms']     = '"How many bathrooms?"';
        
        $validator->setAttributeNames($rules);
     
        return $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
