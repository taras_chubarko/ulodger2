<?php

namespace Modules\Host\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Host\Repositories\HostRepository;
use Modules\Host\Events\HostCalcEvent;
use Modules\Host\Emails\HostPM;
use Modules\Host\Entities\HostBook;
use Modules\Host\Entities\Host;

class HostController extends Controller
{
    /*
     *
     */
    protected $host;
    /*
     *
     */
    public function __construct(HostRepository $host)
    {
        $this->host = $host;
    }
    /* public function upload_photo
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function upload_photo(Request $request)
    {
        //dd($request->all());
        $user = \Sentinel::getUser();
        $file = \Filem::make($request->image, 'photo', ($user) ? $user->id : 0, $status = 1);
        $result['id'] = $request->id;
        $result['src'] = '/uploads/photo/'.$file->filename;
        $result['fid'] = $file->id;
        $result['loading'] = false;
        return response()->json($result, 200);
    }
    /* public function add_save
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function add_step1_save(\Modules\Host\Http\Requests\AddStep1Req $request)
    {
        $user = \Sentinel::getUser();

        if($request->has('edit_host'))
        {
            $host = $this->host->find($request->edit_host);
            $host->title    = $request->title;
            $host->progress = $request->progress;
            $host->step     = $request->step;
            $host->save();
        }
        else
        {
            $host = $this->host->create([
                'user_id'   => ($user) ? $user->id : 0,
                'title'     => $request->title,
                'progress'  => $request->progress,
                'step'      => $request->step,
                'status'    => 0,
            ]);
        }
        //
        $host->rentOut()->detach();
        $host->rentOut()->attach($host->id, [
            'rent_out' => $request->rent_out,
        ]);
        //
        $host->propertyType()->detach();
        $host->propertyType()->attach($host->id, [
            'property_type' => $request->property_type,
        ]);
        //
        $host->description()->detach();
        $host->description()->attach($host->id, [
            'description' => $request->description,
        ]);
        //
        $result['host'] = $host;

        return response()->json($result, 200);
    }
    /* public function add_step2_save
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function add_step2_save(\Modules\Host\Http\Requests\AddStep2Req $request)
    {
        $host = $this->host->find($request->edit_host);
        $host->progress = $request->progress;
        $host->step     = $request->step;
        $host->save();
        //
        $host->smoking()->detach();
        $host->smoking()->attach($host->id, [
            'smoking' => $request->smoking,
        ]);
        //
        $host->guests()->detach();
        $host->guests()->attach($host->id, [
            'guests' => $request->guests,
        ]);
        //
        $host->bedrooms()->detach();
        $host->bedrooms()->attach($host->id, [
            'bedrooms' => $request->bedrooms,
        ]);
        //
        $host->beds()->detach();
        $host->beds()->attach($host->id, [
            'beds' => $request->beds,
        ]);
        //
        $host->bathType()->detach();
        $host->bathType()->attach($host->id, [
            'bath_type' => $request->bath_type,
        ]);
        //
        $host->bathrooms()->detach();
        $host->bathrooms()->attach($host->id, [
            'bathrooms' => $request->bathrooms,
        ]);
        //
        $result['host'] = $host;
        return response()->json($result, 200);
    }
    /* public function add_step3_save
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function add_step3_save(\Modules\Host\Http\Requests\AddStep3Req $request)
    {
        $host = $this->host->find($request->edit_host);
        $host->progress = $request->progress;
        $host->step     = $request->step;
        $host->save();

        $host->location()->detach();
        $host->location()->attach($host->id, $request->location);
        //
        $host->schoolslocaton()->detach();
        foreach($request->schoolslocaton as $schoolslocaton)
        {
            //if($schoolslocaton['name'])
            //{
            $host->schoolslocaton()->attach($host->id, $schoolslocaton);
            //}
        }
        //
        $result['host'] = $host;
        return response()->json($result, 200);
    }
    /* public function add_step4_save
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function add_step4_save(\Modules\Host\Http\Requests\AddStep4Req $request)
    {
        $host = $this->host->find($request->edit_host);
        $host->progress = $request->progress;
        $host->step     = $request->step;
        $host->save();

        $host->amenities()->detach();
        if($request->has('amenities'))
        {
            $i = 0;
            foreach($request->amenities as $amenities)
            {
                $host->amenities()->attach($host->id, [
                    'amenities' => $amenities,
                    'sort' => $i,
                ]);
                $i++;
            }
        }
        //
        $result['host'] = $host;
        return response()->json($result, 200);
    }
    /* public function add_step5_save
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function add_step5_save(\Modules\Host\Http\Requests\AddStep5Req $request)
    {
        $host = $this->host->find($request->edit_host);
        $host->progress = $request->progress;
        $host->step     = $request->step;
        $host->save();

        $host->spaces()->detach();
        if($request->has('spaces'))
        {
            $i = 0;
            foreach($request->spaces as $spaces)
            {
                $host->spaces()->attach($host->id, [
                    'spaces' => $spaces,
                    'sort' => $i,
                ]);
                $i++;
            }
        }
        //
        $result['host'] = $host;
        return response()->json($result, 200);
    }
    /* public function add_step6_save
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function add_step6_save(Request $request)
    {
        $host = $this->host->find($request->edit_host);
        $host->progress = $request->progress;
        $host->step     = $request->step;
        $host->save();

        if($request->has('images'))
        {
            $i = 0;
            foreach($request->images as $key => $photo)
            {
                $host->photo()->attach($host->id, [
                    'fid'       => $photo['fid'],
                    'nameid'    => $photo['id'],
                    'sort'      => $i,
                ]);
                $i++;
            }
        }
        //
        $result['host'] = $host;
        return response()->json($result, 200);
    }
    /* public function add_step7_save
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function add_step7_save(\Modules\Host\Http\Requests\AddStep7Req $request)
    {
        $host = $this->host->find($request->edit_host);
        $host->progress = $request->progress;
        $host->step     = $request->step;
        $host->status   = 1;
        $host->save();

        $host->available()->detach();
        if($request->has('availablefrom'))
        {
            $host->available()->attach($host->id, [
                'from'  => \Carbon::parse($request->availablefrom),
                'to'    => \Carbon::parse($request->availableto),
            ]);
        }
        $host->price()->detach();
        if($request->has('price'))
        {
            $host->price()->attach($host->id, [
                'price'  => $request->price,
            ]);
        }
        //
        $result['host'] = $host;
        return response()->json($result, 200);
    }
    /* public function remove
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function remove($id)
    {
        $this->host->delete($id);
    }
    /* public function slug
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function slug($slug)
    {
        $user = \Sentinel::getUser();
        //
        $host = \Host::with(config('host.relations.all'))
            ->findWhere(['slug' => $slug])->first()->toArray();
        return response()->json($host, 200);
    }
    /* public function load_tab
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function load_tab($id, $tab)
    {
        $host = \Host::find($id);
        $host->load(config('host.relations.all'));

        $result['view'] = view('site::host.block-host-'.$tab, compact('host'))->render();
        return response()->json($result, 200);
    }
    /* public function found
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function found(Request $request)
    {
        \MetaTag::set('title', 'Hosts Found');
        $this->host->pushCriteria(app('Modules\Host\Criteria\HostFindCriteria'));
        $hosts = $this->host->with(['photo', 'price', 'location'])->paginate(36);

        $h = \Host::with('price')->findWhere(['status' => 1]);
        $priceMin = $h->pluck('price.0.pivot.price')->min();
        $priceMax = $h->pluck('price.0.pivot.price')->max();

        //
        if($request->ajax())
        {
            $result['result'] = [];
            $result['zoom'] = 9;
            $result['view'] = 'No results.';
            $result['total'] = 0;
            $result['mapCenter'] = ['lat' => 0, 'lng' => -0];

            //
            if($hosts->count())
            {
                $minLat = $hosts->pluck('location.0.pivot.lat')->min();
                $maxLat = $hosts->pluck('location.0.pivot.lat')->max();
                $minLng = $hosts->pluck('location.0.pivot.lng')->min();
                $maxLng = $hosts->pluck('location.0.pivot.lng')->max();

                $lat = ($minLat + $maxLat) / 2;
                $lng = ($minLng + $maxLng) / 2;

                $result['mapCenter'] = ['lat' => $lat, 'lng' => $lng];
                $items = [];
                foreach($hosts as $host)
                {
                    $items[] = [
                        'id'    => $host->id,
                        'title' => $host->title,
                        'price' => $host->fieldPrice->normal,
                        'location' => [
                            'lat' => data_get($host, 'location.0.pivot.lat'),
                            'lng' => data_get($host, 'location.0.pivot.lng'),
                        ],
                    ];
                }

                $result['result'] = $items;
                $result['zoom'] = 2;//$request->has('zoom') ? $request->zoom : 2;
                $result['view'] = view('site::host.block-find-hosts', compact('hosts'))->render();
                $result['total'] = $hosts->total();
            }


            //$result['result'] = $hosts->toJson();
            return response()->json($result, 200);
        }
    }
    /* public function message
     * @param $   
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function message(\Modules\Host\Http\Requests\SendPMReq $request, $slug)
    {
        if ($user = \Sentinel::check())
        {
            $host = \Host::with(config('host.relations.all'))
                ->findWhere(['slug' => $slug])->first();

            \Mail::send(new HostPM($request, $user, $host));
            $result['closeModal'] = true;
            $result['success2'] = 'Message sent!';
            return response()->json($result, 200);
        }
        else
        {
            return response()->json(['error' => [0=> 'Must be logged in.']], 442);
        }
    }
    /* public function contact
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function contact(Request $request, $slug)
    {
        $user = \Sentinel::getUser();
        $host = \Host::findWhere(['slug' => $slug])->first();

        $inbox = \Inbox::create([
            'user_id_from'  => $user->id,
            'user_id_to'    => $host->user_id,
            'type'          => 'message',//'requests',
            'subject'       => '<span>New Message</span> - '.$host->title,
            'message'       => $request->messagec,
            'status'        => 1,
            'parent_id'     => 0,
            'read'          => 0,
            'request_id'    => null,//$bookRequest->id,
        ]);
        //
        $result['success']     = 'Message sent.';

        return response()->json($result, 200);
    }
    /* public function find_host
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function find_host(Request $request)
    {
        $this->host->pushCriteria(app('Modules\Host\Criteria\HostFindCriteria'));
        $hosts = $this->host->with(['photo', 'price', 'location', 'rating'])->paginate(36);
        $hosts->appends($request->all());

        return response()->json($hosts, 200);
    }
    /* public function hosted
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function hosted($user_id)
    {
        $user = \Sentinel::findById($user_id);
        $user->load('reviews');

        return response()->json($user, 200);
    }
    /*
     * function book
     * @param $arg
     */

    public function book(Request $request, $id)
    {
        $user = \Sentinel::getUser();
        $host = \Host::find($id);
        //
        $book = HostBook::where('user_id', $user->id)
            ->where('host_id', $id)
            ->whereIn('status', [0,1])
            ->first();
        //
        if(!$book)
        {
            $bookRequest = HostBook::create([
                'host_id'       => $host->id,
                'user_id'       => $user->id,
                'host_user_id'  => $host->user_id,
                'check_in'      => \Carbon::parse($request->check_in),
                'check_out'     => \Carbon::parse($request->check_out),
                'guests'        => $request->lodgers,
                'status'        => 0,
            ]);
            //
            $inbox = \Inbox::create([
                'user_id_from'  => $user->id,
                'user_id_to'    => $host->user_id,
                'type'          => 'requests',
                'subject'       => '<span>New Reservation</span> - '.$host->title,
                'message'       => 'I want to reservation this Host.',
                'status'        => 1,
                'parent_id'     => 0,
                'read'          => 0,
                'request_id'    => $bookRequest->id,
            ]);
            //
            $result['success'] = 'Booking request sent.';
            return response()->json($result, 200);
        }
        else
        {
            return response()->json(['error' => 'You have already created a reservation request.'], 442);
        }
    }
    /* public function host_settings
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function host_settings($step)
    {
        switch($step)
        {
            case 'step1':
                $settings['rent_out'] = \TaxonomyTerm::findWhere(['taxonomy_id' => 1])->toArray();
                $settings['property_type'] = \TaxonomyTerm::findWhere(['taxonomy_id' => 2])->toArray();
                $result['settings'] = $settings;
                return response()->json($result, 200);
                break;

            case 'step2':
                $settings['guests'] = config('host.guests');
                $settings['bedrooms'] = config('host.bedrooms');
                $settings['beds'] = config('host.beds');
                $settings['bathrooms'] = config('host.bathrooms');
                $result['settings'] = $settings;
                return response()->json($result, 200);
                break;

            case 'step4':
                $settings['amenities4'] = \TaxonomyTerm::findByField('taxonomy_id', 4)->toArray();
                $settings['amenities5'] = \TaxonomyTerm::findByField('taxonomy_id', 5)->toArray();
                $result['settings'] = $settings;
                return response()->json($result, 200);
                break;

            case 'step5':
                $settings['spaces'] = \TaxonomyTerm::findByField('taxonomy_id', 6)->toArray();
                $result['settings'] = $settings;
                return response()->json($result, 200);
                break;
        }
    }
    /* public function host_edit
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function host_edit($id)
    {
        $host = $this->host->find($id);

        $result['step1'] = [
            'progress'      => 14.29,
            'step'          => 1,
            'rent_out'      => data_get($host, 'rentOut.0.pivot.rent_out'),
            'property_type' => data_get($host, 'propertyType.0.pivot.property_type'),
            'title'         => $host->title,
            'description'   => data_get($host, 'description.0.pivot.description'),
            'edit_host'     => $host->id,
            'next'          => '/edit/host/'.$host->slug.'/step-2',
        ];
        //
        $result['step2'] = [
            'progress'      => 28.58,
            'step'          => 2,
            'smoking'       => data_get($host, 'smoking.0.pivot.smoking'),
            'guests'        => data_get($host, 'guests.0.pivot.guests'),
            'bedrooms'      => data_get($host, 'bedrooms.0.pivot.bedrooms'),
            'beds'          => data_get($host, 'beds.0.pivot.beds'),
            'bath_type'     => data_get($host, 'bathType.0.pivot.bath_type'),
            'bathrooms'     => data_get($host, 'bathrooms.0.pivot.bathrooms'),
            'edit_host'     => $host->id,
            'next'          => '/edit/host/'.$host->slug.'/step-3',
            'prev'          => '/edit/host/'.$host->slug.'/step-1',
        ];
        //
        $schoolslocaton = [];
        if($host->schoolslocaton->count())
        {
            foreach($host->schoolslocaton as $var)
            {
                $schoolslocaton[] = [
                    'name'      => $var->pivot->name,
                    'locality'  => $var->pivot->locality,
                    'address'   => $var->pivot->address,
                    'region'    => $var->pivot->region,
                    'lat'       => $var->pivot->lat,
                    'lng'       => $var->pivot->lng,
                ];
            }
        }
        $result['step3'] = [
            'progress'      => 42.87,
            'step'          => 3,
            'location'      => [
                'locality'  => data_get($host, 'location.0.pivot.locality'),
                'address'   => data_get($host, 'location.0.pivot.address'),
                'region'    => data_get($host, 'location.0.pivot.region'),
                'lat'       => data_get($host, 'location.0.pivot.lat'),
                'lng'       => data_get($host, 'location.0.pivot.lng'),
            ],
            'schoolslocaton' => [
                0 => [
                    'name'      => data_get($host, 'schoolslocaton.0.pivot.name'),
                    'locality'  => data_get($host, 'schoolslocaton.0.pivot.locality'),
                    'address'   => data_get($host, 'schoolslocaton.0.pivot.address'),
                    'region'    => data_get($host, 'schoolslocaton.0.pivot.region'),
                    'lat'       => data_get($host, 'schoolslocaton.0.pivot.lat'),
                    'lng'       => data_get($host, 'schoolslocaton.0.pivot.lng'),
                ],
                1 => [
                    'name'      => data_get($host, 'schoolslocaton.1.pivot.name'),
                    'locality'  => data_get($host, 'schoolslocaton.1.pivot.locality'),
                    'address'   => data_get($host, 'schoolslocaton.1.pivot.address'),
                    'region'    => data_get($host, 'schoolslocaton.1.pivot.region'),
                    'lat'       => data_get($host, 'schoolslocaton.1.pivot.lat'),
                    'lng'       => data_get($host, 'schoolslocaton.1.pivot.lng'),
                ],
                2 => [
                    'name'      => data_get($host, 'schoolslocaton.2.pivot.name'),
                    'locality'  => data_get($host, 'schoolslocaton.2.pivot.locality'),
                    'address'   => data_get($host, 'schoolslocaton.2.pivot.address'),
                    'region'    => data_get($host, 'schoolslocaton.2.pivot.region'),
                    'lat'       => data_get($host, 'schoolslocaton.2.pivot.lat'),
                    'lng'       => data_get($host, 'schoolslocaton.2.pivot.lng'),
                ]
            ],
            'edit_host'      => $host->id,
            'next'           => '/edit/host/'.$host->slug.'/step-4',
            'prev'           => '/edit/host/'.$host->slug.'/step-2',
        ];
        //
        $amenities = [];
        if($host->amenities->count())
        {
            foreach($host->amenities as $var)
            {
                $amenities[] = $var->pivot->amenities;
            }
        }
        $result['step4'] = [
            'progress'      => 57.16,
            'step'          => 4,
            'amenities'     => $amenities,
            'edit_host'     => $host->id,
            'next'          => '/edit/host/'.$host->slug.'/step-5',
            'prev'          => '/edit/host/'.$host->slug.'/step-3',
        ];
        //
        $spaces = [];
        if($host->spaces->count())
        {
            foreach($host->spaces as $var)
            {
                $spaces[] = $var->pivot->spaces;
            }
        }
        $result['step5'] = [
            'progress'      => 71.45,
            'step'          => 5,
            'spaces'        => $spaces,
            'edit_host'     => $host->id,
            'next'          => '/edit/host/'.$host->slug.'/step-6',
            'prev'          => '/edit/host/'.$host->slug.'/step-4',
        ];
        //
        $images = [];
        if($host->photo->count())
        {
            foreach($host->photo as $var)
            {
                $images[] = [
                    'id'        => $var->pivot->nameid,
                    'src'       => '/uploads/photo/'.$var->filename,
                    'fid'       => $var->pivot->fid,
                    'loading'   => false,
                ];
            }
        }
        $result['step6'] = [
            'progress'      => 85.74,
            'step'          => 6,
            'images'        => $images,
            'edit_host'     => $host->id,
            'next'          => '/edit/host/'.$host->slug.'/step-7',
            'prev'          => '/edit/host/'.$host->slug.'/step-5',
        ];
        //
        $result['step7'] = [
            'progress'      => 100,
            'step'          => 7,
            'availablefrom' => \Carbon::parse(data_get($host, 'available.0.pivot.from'))->format('m/d/Y'),
            'availableto'   => \Carbon::parse(data_get($host, 'available.0.pivot.to'))->format('m/d/Y'),
            'price'         => $host->fieldPrice->normal,
            'edit_host'     => $host->id,
            'next'          => '/edit/host/'.$host->slug.'/updated',
            'prev'          => '/edit/host/'.$host->slug.'/step-6',
        ];
        //
        $result['host'] = $host;
        $result['edit'] = true;
        $result['goto'] = '/edit/host/'.$host->slug.'/step-'.$host->step;

        return response()->json($result, 200);
    }
    /* public function host_inspired
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function host_inspired()
    {
        $q = Host::orderBy('created_at', 'DESC');
        $q->with('photo', 'price', 'rating');
        $q->where('status', 1);
        $q->whereHas('photo', function($q)
        {
            $q->whereNotNull('fid');
        });
        $hosts = $q->take(2)->get();

        return response()->json($hosts, 200);
    }
}
