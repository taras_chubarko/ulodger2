<?php

Route::group(['middleware' => 'web', 'prefix' => 'host', 'namespace' => 'Modules\Host\Http\Controllers'], function()
{
    //Route::get('/', 'HostController@index');
//    Route::get('/add', [
//        'middleware' => 'login',
//	'as'    => 'host.add',
//	'uses' 	=> 'HostController@add'
//    ]);
//    
//    Route::get('/add/{step?}', [
//        'middleware' => 'login',
//	'as'    => 'host.add.step',
//	'uses' 	=> 'HostController@add_step'
//    ]);
    
    Route::post('/add/step1', [
	'as'    => 'host.add.step1',
	'uses' 	=> 'HostController@add_step1_save'
    ]);
    
    Route::post('/add/step2', [
	'as'    => 'host.add.step2',
	'uses' 	=> 'HostController@add_step2_save'
    ]);
    
    Route::post('/add/step3', [
	'as'    => 'host.add.step3',
	'uses' 	=> 'HostController@add_step3_save'
    ]);
    
    Route::post('/add/step4', [
	'as'    => 'host.add.step4',
	'uses' 	=> 'HostController@add_step4_save'
    ]);
    
    Route::post('/add/step5', [
	'as'    => 'host.add.step5',
	'uses' 	=> 'HostController@add_step5_save'
    ]);
    
    Route::post('/add/step6', [
	'as'    => 'host.add.step6',
	'uses' 	=> 'HostController@add_step6_save'
    ]);
    
    Route::post('/add/step7', [
	'as'    => 'host.add.step7',
	'uses' 	=> 'HostController@add_step7_save'
    ]);
//    
//    
//    Route::post('/{slug}/finish', [
//	'as'    => 'host.finish',
//	'uses' 	=> 'HostController@finish'
//    ]);
//    
//    Route::post('/{id}/remove', [
//	'as'    => 'host.remove',
//	'uses' 	=> 'HostController@remove'
//    ]);
    
//    Route::get('/{slug}', [
//	'as'    => 'host.slug',
//	'uses' 	=> 'HostController@slug'
//    ])->where('slug', Host::allSlug());
//    
//    Route::get('/{id}/load/{tab}', [
//	'as'    => 'host.load.tab',
//	'uses' 	=> 'HostController@load_tab'
//    ]);
//    
//    Route::get('found', [
//	'as'    => 'host.found',
//	'uses' 	=> 'HostController@found'
//    ]);
//    
//    Route::get('find', [
//	'as'    => 'host.find',
//	'uses' 	=> 'HostController@find'
//    ]);
//    
//    Route::post('/{slug}/message', [
//	'as'    => 'host.slug.message',
//	'uses' 	=> 'HostController@message'
//    ]);
//    
//    
//    Route::get('/{slug}/edit/{step?}', [
//	'as'    => 'host.slug.edit',
//	'uses' 	=> 'HostController@edit'
//    ]);to
//    
    
    Route::post('{slug}', [
	'as'    => 'host.slug',
	'uses' 	=> 'HostController@slug'
    ]);
    
    Route::post('/hosted/{user_id}', [
	'as'    => 'host.slug.hosted',
	'uses' 	=> 'HostController@hosted'
    ]);
    
    Route::post('{id}/book', [
	'as'    => 'host.book',
	'uses' 	=> 'HostController@book'
    ]);
    
    Route::post('{id}/remove', [
	'as'    => 'host.remove',
	'uses' 	=> 'HostController@remove'
    ]);
    
    Route::post('settings/{step}', [
	'as'    => 'host.settings',
	'uses' 	=> 'HostController@host_settings'
    ]);
    
    Route::post('/upload/photo', [
	'as'    => 'host.upload.photo',
	'uses' 	=> 'HostController@upload_photo',
    ]);
    
    Route::post('edit/{id}', [
	'as'    => 'host.edit',
	'uses' 	=> 'HostController@host_edit'
    ]);
    
    Route::post('get/inspired', [
	'as'    => 'host.inspired',
	'uses' 	=> 'HostController@host_inspired'
    ]);
    
    Route::post('/{slug}/contact', [
	'as'    => 'host.slug.contact',
	'uses' 	=> 'HostController@contact'
    ]);
    
    
});

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Host\Http\Controllers'], function()
{
    Route::post('/find/hosts', [
	'as'    => 'host.find.post',
	'uses' 	=> 'HostController@find_host'
    ]);
});
