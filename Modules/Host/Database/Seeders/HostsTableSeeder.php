<?php

namespace Modules\Host\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Host\Events\HostCalcEvent;
use Faker\Factory as Faker;

class HostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('en_US');
       
        //$address = preg_replace("/\r\n|\r|\n/",'',$faker->address);
        
        foreach(range(1, 100) as $i)
        {
            $data['address']    = $faker->streetAddress;
            $data['key']        = 'AIzaSyBBziS3HCZ6We5DhI3VdlGfQ4mHF_QvIcs';
            $data['language']   = 'en';
            $response = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?'.http_build_query($data)));
            //
            $users = \User::findWhere([['id', '<>', 0]]);
            $users = $users->pluck('id')->toArray();
            //
            if(!empty($response->results[0]))
            {
                
                $host = \Host::create([
                    'user_id'   => $faker->randomElement($users),
                    'title'     => $faker->sentence($nbWords = 6, $variableNbWords = true),
                    'step'      => 7,
                    'status'    => 1,
                ]);
                //
                $rentOut = array_flip(\TaxonomyTerm::get(1));
                $host->rentOut()->attach($host->id, [
                    'rent_out' => $faker->randomElement($rentOut),
                ]);
                //
                $propertyType = array_flip(\TaxonomyTerm::get(2));
                $host->propertyType()->attach($host->id, [
                    'property_type' => $faker->randomElement($propertyType),
                ]);
                //
                $host->description()->attach($host->id, [
                    'description' => $faker->text(500) ,
                ]);
                //
                $host->smoking()->attach($host->id, [
                    'smoking' => $faker->randomElement([1,2,3]),
                ]);
                //
                $host->guests()->attach($host->id, [
                    'guests' => $faker->randomElement(array_flip(config('host.guests'))),
                ]);
                //
                $host->bedrooms()->attach($host->id, [
                    'bedrooms' => $faker->randomElement(array_flip(config('host.bedrooms'))),
                ]);
                //
                $host->beds()->attach($host->id, [
                    'beds' => $faker->randomElement(array_flip(config('host.beds'))),
                ]);
                //
                $host->bathType()->attach($host->id, [
                    'bath_type' => $faker->randomElement([1,2]),
                ]);
                //
                $host->bathrooms()->attach($host->id, [
                    'bathrooms' => $faker->randomElement(array_flip(config('host.bathrooms'))),
                ]);
                //
                $geo = $response->results[0];
                foreach($geo->address_components as $address)
                {
                    if($address->types[0] == 'locality')
                    {
                        $locality = $address->long_name;
                    }
                    if($address->types[0] == 'administrative_area_level_1')
                    {
                        $administrative_area_level_1 = $address->long_name;
                    }
                }
                
                $host->location()->attach($host->id, [
                    'address'   => $geo->formatted_address,
                    'locality'  => $locality,
                    'region'    => $administrative_area_level_1,
                    'lat'       => $geo->geometry->location->lat,
                    'lng'       => $geo->geometry->location->lng,
                ]);
                //
                $data2['address']    = $faker->streetAddress;
                $data2['key']        = 'AIzaSyBBziS3HCZ6We5DhI3VdlGfQ4mHF_QvIcs';
                $data2['language']   = 'en';
                $response2 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?'.http_build_query($data2)));
                if(!empty($response2->results[0]))
                {
                    $geo2 = $response2->results[0];
                    foreach($geo2->address_components as $address2)
                    {
                        if($address2->types[0] == 'locality')
                        {
                            $locality2 = $address2->long_name;
                        }
                        if($address2->types[0] == 'administrative_area_level_1')
                        {
                            $administrative_area_level_12 = $address2->long_name;
                        }
                    }
                    
                    $host->schoolslocaton()->attach($host->id, [
                        'address'   => $geo2->formatted_address,
                        'locality'  => $locality2,
                        'region'    => $administrative_area_level_12,
                        'lat'       => $geo2->geometry->location->lat,
                        'lng'       => $geo2->geometry->location->lng,
                    ]);
                }
                //
                $amenities = array_flip(\TaxonomyTerm::get(4));
                $amenities = $faker->randomElements($amenities, 4);
                $u = 0;
                foreach($amenities as $ame)
                {
                    $host->amenities()->attach($host->id, [
                        'amenities' => $ame,
                        'sort' => $u,
                    ]);
                    $u++;
                }
                //
                $spaces = array_flip(\TaxonomyTerm::get(6));
                $spaces = $faker->randomElements($spaces, 4);
                $v = 0;
                foreach($spaces as $sp)
                {
                    $host->spaces()->attach($host->id, [
                        'spaces' => $sp,
                        'sort' => $v,
                    ]);
                    $v++;
                }
                //
                $addDays = $faker->randomElement([5,10,20,30,50,90,180]);
                $host->available()->attach($host->id, [
                    'from'  => \Carbon::now(),
                    'to'    => \Carbon::now()->addDays($addDays),
                ]);
                //
                $host->price()->attach($host->id, [
                    'price'  => $faker->numberBetween(50, 1000),
                ]);
                
                
                event(new HostCalcEvent($host));
            }
        }
        
        dd($host);
        
    }
}
