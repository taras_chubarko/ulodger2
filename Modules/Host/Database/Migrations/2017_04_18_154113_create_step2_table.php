<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStep2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('host_smoking', function (Blueprint $table) {
            $table->integer('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('host')->onDelete('cascade');
            $table->integer('smoking')->nullable();
        });
        
        Schema::create('host_guests', function (Blueprint $table) {
            $table->integer('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('host')->onDelete('cascade');
            $table->integer('guests')->nullable();
        });
        
        Schema::create('host_bedrooms', function (Blueprint $table) {
            $table->integer('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('host')->onDelete('cascade');
            $table->integer('bedrooms')->nullable();
        });
        
        Schema::create('host_beds', function (Blueprint $table) {
            $table->integer('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('host')->onDelete('cascade');
            $table->integer('beds')->nullable();
        });
        
        Schema::create('host_bath_type', function (Blueprint $table) {
            $table->integer('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('host')->onDelete('cascade');
            $table->integer('bath_type')->nullable();
        });
        
        Schema::create('host_bathrooms', function (Blueprint $table) {
            $table->integer('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('host')->onDelete('cascade');
            $table->decimal('bathrooms', 10, 2)->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('step2');
    }
}
