<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHost3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('host_location', function (Blueprint $table) {
            $table->integer('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('host')->onDelete('cascade');
            $table->string('address')->nullable();
            $table->string('locality')->nullable();
            $table->string('region')->nullable();
            $table->decimal('lat', 10,6)->nullable();
            $table->decimal('lng', 10,6)->nullable();
        });
        
        Schema::create('host_schoolslocaton', function (Blueprint $table) {
            $table->integer('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('host')->onDelete('cascade');
            $table->string('address')->nullable();
            $table->string('locality')->nullable();
            $table->string('region')->nullable();
            $table->decimal('lat', 10,6)->nullable();
            $table->decimal('lng', 10,6)->nullable();
            $table->integer('sort')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('host3');
    }
}
