<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostStep6Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('host_photo', function (Blueprint $table) {
            $table->integer('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('host')->onDelete('cascade');
            $table->integer('fid')->nullable();
            $table->integer('sort')->nullable();
        });
        
        Schema::create('host_available', function (Blueprint $table) {
            $table->integer('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('host')->onDelete('cascade');
            $table->date('from')->nullable();
            $table->date('to')->nullable();
        });
        
        Schema::create('host_price', function (Blueprint $table) {
            $table->integer('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('host')->onDelete('cascade');
            $table->decimal('price', 10,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('host_step6');
    }
}
