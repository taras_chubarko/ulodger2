<?php

namespace Modules\Host\Providers;

use Illuminate\Support\ServiceProvider;

class HostServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
	
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
	$this->commands([
	    \Modules\Host\Console\ChekBookCommand::class
	]);
	
        $this->app->bind(
	    'Modules\Host\Repositories\HostRepository',
	    'Modules\Host\Repositories\HostRepositoryEloquent'
	);
	    
	$this->app->booting(function()
	{
	    $loader = \Illuminate\Foundation\AliasLoader::getInstance();
	    $loader->alias('Host', 'Modules\Host\Facades\Host');
        });
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('host.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'host'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/host');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/host';
        }, \Config::get('view.paths')), [$sourcePath]), 'host');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/host');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'host');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'host');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
