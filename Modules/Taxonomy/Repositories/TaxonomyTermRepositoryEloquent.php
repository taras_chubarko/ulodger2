<?php

namespace Modules\Taxonomy\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Taxonomy\Repositories\TaxonomyTermRepository;
use Modules\Taxonomy\Entities\TaxonomyTerm;

/**
 * Class TaxonomyTermRepositoryEloquent
 * @package namespace App\Repositories;
 */
class TaxonomyTermRepositoryEloquent extends BaseRepository implements TaxonomyTermRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TaxonomyTerm::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /*
    /* public function itemsArray($taxonomy_id);
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function itemsArray($taxonomy_id)
    {
        $items = $this->model->where('taxonomy_id', $taxonomy_id)->get()->toHierarchy()->toArray();
        
        $menus = $this->parseArray($items);
        
        $arr = [];
        $arr[0] = '- Корень -';
        
        foreach($menus as $item)
        {
            $depth = str_repeat('-', $item['depth']);
            
            $arr[$item['id']] = $depth . $item['name'];
        }
        
        return $arr;
    }
    /*
     *
     */
    public static function parseArray($parseArray, $parentID = 0)
    {
        $return = array();
        foreach ($parseArray as $subArray) {
            $returnSubSubArray = array();
            if (isset($subArray['children'])) {
                $returnSubSubArray = self::parseArray($subArray['children'], $subArray['id']);
            }
            $return[] = array(
                'id'        => $subArray['id'],
                'parent_id' => $parentID,
                'name'      => $subArray['name'],
                'depth'     => $subArray['depth'],
            );
            $return = array_merge($return, $returnSubSubArray);
        }
        return $return;
    }
    /* public function get($taxonomy_id, $parent_id, $params = array())
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get($taxonomy_id, $parent_id = null, $params = array())
    {
        $items = $this->model->where('taxonomy_id', $taxonomy_id)->where('parent_id', $parent_id)->orderBy('sort', 'ASC')->get();
        
        $terms = array();
        
        if(!empty($params['title']))
        {
            $terms[''] = $params['title'];
        }
        
        if(!empty($params['group']) && $params['group'] == true)
        {
            foreach($items as $item)
            {
                $groups = $this->get($taxonomy_id, $item->id);
                
                //foreach($groups as $group)
                //{
                //    $groupsItem[$group->id] = $group->name;
                //}
                
                $terms[$item->name] = $groups;
            }
        }
        else
        {
            foreach($items as $item)
            {
                $terms[$item->id] = $item->name;
            }
        }
        return $terms;
    }
    /* public function tree($taxonomy_id);
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function tree($taxonomy_id)
    {
        return $this->model->where('taxonomy_id', $taxonomy_id)->get()->toHierarchy();
    }
    /* public function treeAll($taxonomy_id);
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function treeAll($taxonomy_id)
    {
        return $this->model->where('taxonomy_id', $taxonomy_id)->get()->toHierarchy();
    }
    /* public function make
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function make($request)
    {
        if($request->parent_id == 0)
        {
            $term = $this->model->create([
                'taxonomy_id' 	=> $request->taxonomy_id,
                'name' 		=> $request->name,
                'sort' 		=> $request->sort,
                'description'   => $request->description,
            ]);
        }
        else
        {
            $term = $this->model->create([
                'taxonomy_id' 	=> $request->taxonomy_id,
                'name' 		=> $request->name,
                'sort' 		=> $request->sort,
                'description'   => $request->description,
            ]);
            $term->makeChildOf($request->parent_id);
        }
        //
        if(!empty($request->image))
        {
            $term->image()->attach($term->id, [
                'fid' => $request->image['fid'],
            ]);
            \Filem::setStatus($request->image['fid'], $status = 1);
        }
        //
        return $term;
    }
    /* public function change
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function change($request, $id)
    {
        $term = $this->model->find($id);
        $term->name 		= $request->name;
        $term->parent_id 	= ($request->parent_id == 0) ? null : $request->parent_id;
        $term->sort 		= $request->sort;
        $term->description      = $request->description;
        $term->save();
        //
        if($request->image)
        {
            $term->image()->detach();
            $term->image()->attach($term->id, [
                'fid' => $request->image['fid'],
            ]);
            \Filem::setStatus($request->image['fid'], $status = 1);
        }
        //
        return $term;
    }
    /* public function destroy
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function destroy($id)
    {
        $term = $this->model->find($id);
        $term->load('image');
        
        $im = data_get($term, 'image.0.id');
        if($im)
        {
            \Filem::destroy($im);
        }
        
        $term->image()->detach();
        
        $term->delete();
        return $term;
    }
    /* public function findBySlug($slug)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }
    /* public function get($taxonomy_id, $parent_id, $params = array())
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get2($taxonomy_id, $parent_id = null)
    {
        $items = $this->model->where('taxonomy_id', $taxonomy_id)->where('parent_id', $parent_id)->orderBy('sort', 'ASC')->get();
        
        $terms = array();
        
        foreach($items as $item)
        {
            $terms[] = $item;
        }
        
        return collect($terms);
    }
}
