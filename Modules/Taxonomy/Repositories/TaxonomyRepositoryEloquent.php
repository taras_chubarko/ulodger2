<?php

namespace Modules\Taxonomy\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Taxonomy\Repositories\TaxonomyRepository;
use Modules\Taxonomy\Entities\Taxonomy;

/**
 * Class TaxonomyRepositoryEloquent
 * @package namespace App\Repositories;
 */
class TaxonomyRepositoryEloquent extends BaseRepository implements TaxonomyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Taxonomy::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
    
    
    
}
