<?php

namespace Modules\Taxonomy\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Taxonomy\Repositories\TaxonomyTermRepository;

class TaxonomyTermController extends Controller
{
    /*
     *
     */
    protected $term;
    /* public function __construct
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct(TaxonomyTermRepository $term)
    {
        $this->term = $term;
    }
    /* public function create
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function create() {
        return view('admin::taxonomy.terms.create');
    }
    
    /* public function store
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function store(\Modules\Taxonomy\Http\Requests\CreateItemReq $request) {
        $term = $this->term->make($request);
        return redirect()->route('admin.taxonomy.show', $request->taxonomy_id)->with('success', 'Термин добавлен.');
    }
    
    /* public function edit
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function edit($id) {
        $term = $this->term->find($id);
        return view('admin::taxonomy.terms.edit', compact('term'));
    }
    
    /* public function update
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function update(\Modules\Taxonomy\Http\Requests\CreateItemReq $request, $id) {
        $term = $this->term->change($request, $id);
        return redirect()->route('admin.taxonomy.show', $term->taxonomy_id)->with('success', 'Термин обновлен.');
    }
    
    /* public function destroy
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function destroy($id) {
        $term = $this->term->destroy($id);
    }
    
    
    /* public function sort
     * @param 
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function sort(Request $request) {
            
            $items = $this->parseJsonArray($request->sort);
            
            foreach($items as $key => $item)
            {
                    $menu = $this->term->find($item['id']);
                    
                    if($item['parent_id'] == 0)
                    {
                            $menu->makeRoot();
                            $menu->sort = $key;
                            $menu->save();
                    }
                    else
                    {
                            $menu->sort = $key;
                            $menu->parent_id = $item['parent_id'];
                            $menu->save();
                    }
                    
            }
            
            return 'OK'; // All the Input fields
            
    }
    
    
    public function parseJsonArray($jsonArray, $parentID = 0)
    {
        $return = array();
        foreach ($jsonArray as $subArray) {
            $returnSubSubArray = array();
            if (isset($subArray['children'])) {
                $returnSubSubArray = $this->parseJsonArray($subArray['children'], $subArray['id']);
            }
            $return[] = array('id' => $subArray['id'], 'parent_id' => $parentID);
            $return = array_merge($return, $returnSubSubArray);
        }
        return $return;
    }
}
