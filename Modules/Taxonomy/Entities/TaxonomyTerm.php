<?php namespace Modules\Taxonomy\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Baum;
//use Watson\Rememberable\Rememberable;

class TaxonomyTerm extends Baum\Node implements Transformable {
    
    use Sluggable;
    use TransformableTrait;
    //use Rememberable;
    
    protected $fillable = [];
    
    protected $table = 'taxonomy_term';
    
    protected $scoped = array('taxonomy_id');
    
    protected $orderColumn = 'sort';
    
    protected $guarded = ['_token'];
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source'    => 'name',
                'onUpdate'  => true,
            ]
        ];
    }
    /* public function image
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function image()
    {
        return $this->belongsToMany('Modules\Filem\Entities\Filem', 'taxonomy_term_image', 'taxonomy_term_id', 'fid')//->remember(60)
        ->withPivot('fid');
    }
    /* public function description
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function description()
    {
        return $this->belongsToMany('Modules\Taxonomy\Entities\TaxonomyTerm', 'taxonomy_term_description')
        ->withPivot('description');
    }
    /* public function getChkAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getChkAttribute()
    {
        $data = array();
        $data[] = ($this->chk1 == 1) ? '<small>в списке</small>' : '';
        $data[] = ($this->chk2 == 1) ? '<small>в блоке</small>' : '';
        return implode(', ', $data);
    }
    

}