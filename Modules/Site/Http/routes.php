<?php
/*
 *
 */
//Route::group(['prefix' => LaravelLocalization::setLocale()], function()
//{
//    Route::group(['middleware' => 'web', 'namespace' => 'Modules\Site\Http\Controllers'], function()
//    {
//        Route::get('/', 'SiteController@index');
//        /*
//        *
//        */
//        Route::get('trans/{string}', [
//            'as' 	=> 'trans.string',
//            'uses' 	=> 'SiteController@trans_string'
//        ]);
//    });
//});

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Site\Http\Controllers'], function()
{
    Route::get('/{vue?}', 'SiteController@index')->where('vue', setAliases());//->where('vue', '[\/\w\.-]*');//->where('vue', slugsArr());
    /*
    *
    */
    //Route::get('trans/{string}', [
    //    'as' 	=> 'trans.string',
    //    'uses' 	=> 'SiteController@trans_string'
    //]);
    //
    Route::get('test/1', [
        'as' 	=> 'test',
        'uses' 	=> 'SiteController@test'
    ]);
    //
    //Route::get('host', [
    //    'as' 	=> 'host',
    //    'uses' 	=> 'SiteController@host'
    //]);
    //
    //Route::get('about', [
    //    'as' 	=> 'about',
    //    'uses' 	=> 'SiteController@about'
    //]);
    //
    //Route::get('operations', [
    //    'as' 	=> 'operations',
    //    'uses' 	=> 'SiteController@operations'
    //]);
    //
    //Route::get('contact', [
    //    'as' 	=> 'contact',
    //    'uses' 	=> 'SiteController@contact'
    //]);
    
    Route::get('import', [
        'as' 	=> 'import',
        'uses' 	=> 'SiteController@import'
    ]);
    
    Route::post('import', [
        'as' 	=> 'import.post',
        'uses' 	=> 'SiteController@import_post'
    ]);
    
    Route::get('parse', [
        'as' 	=> 'parse',
        'uses' 	=> 'SiteController@parse'
    ]);
    
    Route::post('contact', [
        'as' 	=> 'contact',
        'uses' 	=> 'SiteController@contact_send'
    ]);
    
    
});


Route::group(['middleware' => ['web', 'Modules\Admin\Http\Middleware\AdminMDW']], function()
{    
   Route::get('/filemanager/show', 'App\Http\Controllers\FilemanagerLaravelController@getShow');
   Route::get('/filemanager/connectors', 'App\Http\Controllers\FilemanagerLaravelController@getConnectors');
   Route::post('/filemanager/connectors', 'App\Http\Controllers\FilemanagerLaravelController@postConnectors');
});






