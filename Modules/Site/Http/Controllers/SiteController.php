<?php

namespace Modules\Site\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Emails\RegisterToAdminMail;
use FastSimpleHTMLDom\Document;
use Modules\Geo\Entities\GeoUniversities;
use Modules\Site\Emails\Contact;
use Ixudra\Curl\Facades\Curl;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        //\MetaTag::set('title', 'Главная');
        return view('site::layouts.index');
    }
    /* public function trans_string
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function trans_string($string)
    {
        $items = [];
        
        $dataEn['key']    = 'trnsl.1.1.20160605T082848Z.19cfd2b576c35bc8.bffce6fe83b12c23f95143c11abebc818dc11419';
        $dataEn['lang']   = 'ru-en';
        $dataEn['format'] = 'plain';
        $dataEn['text']   = $string;
        $responseEn = json_decode(file_get_contents('https://translate.yandex.net/api/v1.5/tr.json/translate?'.http_build_query($dataEn)));
        $en = $responseEn->text[0];
        
        $dataUk['key']    = 'trnsl.1.1.20160605T082848Z.19cfd2b576c35bc8.bffce6fe83b12c23f95143c11abebc818dc11419';
        $dataUk['lang']   = 'ru-uk';
        $dataUk['format'] = 'plain';
        $dataUk['text']   = $string;
        $responseUk = json_decode(file_get_contents('https://translate.yandex.net/api/v1.5/tr.json/translate?'.http_build_query($dataUk)));
        $uk = $responseUk->text[0];
        
        $limit = str_random(20);
        
        $items['ru'] = "'$limit' " .'=>'. " '$string',";
        $items['en'] = "'$limit' " .'=>'. " '$en',";
        $items['uk'] = "'$limit' " .'=>'. " '$uk',";
        dd($items);
    }
    /* public function test
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function test()
    {
//        $account = [
//            'account_id' => '1950646076',
//            'short_description' => 'pay for lodger',
//            'type' => 'service',
//            'amount' => 500,
//            'currency' => 'USD',
//        ];
//
//        $user = \WePayLaravel::checkout()->create($account);
//        dd($user);

        $html = view('site::invoice.invoice')->render();
        return \PDF::load($html)->show();

    }
    /* public function distance
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function distance($lat, $lng, $distance)
    {
        $ids = \DB::select('SELECT host_id, ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians( lat ) ) ) ) AS distance FROM host_location HAVING distance < '.$distance.' ORDER BY distance LIMIT 0 , 20;');
        $ids = collect($ids)->pluck('host_id')->toArray();
        
        return $ids;
    }
    /* public function parse
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function parse()
    {
        $univ = GeoUniversities::whereNull('address')->first();
        $data['query']      =  $univ->name;
        //$data['types']      = 'university|establishment|school';
        $data['key']        = 'AIzaSyA-D9ab5aN_5JZGP05u5izNtgvS0c-gezI';
        $data['language']   = 'en';
        // $data['input']      = 'Marine Biological Laboratory';
        //https://maps.googleapis.com/maps/api/place/autocomplete/json?
        $response = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/place/textsearch/json?'.http_build_query($data)));
        
        if($response->results)
        {
            $results = $response->results[0];
            $univ->address = $results->formatted_address;
            $univ->lat = $results->geometry->location->lat;
            $univ->lng = $results->geometry->location->lng;
            $univ->save();
        }
        dd($response);
    }
    /* public function host
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function host()
    {
        \MetaTag::set('title', 'Host');
        
        return view('site::pages.page-host');
    }
    /* public function about
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function about()
    {
        \MetaTag::set('title', 'About');
        
        return view('site::pages.page-about');
    }
    /* public function operations
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function operations()
    {
        \MetaTag::set('title', 'Operations');
        
        return view('site::pages.page-operations');
    }
    /* public function contact
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function contact()
    {
        \MetaTag::set('title', 'Contact');
        
        return view('site::pages.page-contact');
    }
    /* public function import
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function import()
    {
        return view('site::dev.import');
    }
    /* public function import_post
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function import_post(Request $request)
    {
        $data = preg_split('/\r\n|\r|\n/', $request->data);
        foreach($data as $k => $item)
        {
            $term = new \stdClass;
            $term->taxonomy_id 	= 2;
            $term->parent_id = $request->id;
            $term->name = $item;
            $term->sort = $k;
            \TaxonomyTerm::make($term);
        }
        return redirect()->back();
    }
    /* public function contact_send
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function contact_send(Request $request)
    {
        \Mail::send(new Contact($request));
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
