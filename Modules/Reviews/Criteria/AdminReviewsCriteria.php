<?php

namespace Modules\Reviews\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class AdminReviewsCriteria
 * @package namespace App\Criteria;
 */
class AdminReviewsCriteria implements CriteriaInterface
{
     /*
     *
     */
    protected $request;
    /* public function __construct
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct($request)
    {
        $this->request = $request;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->orderBy('created_at', 'DESC');
        //
        if($this->request->from_user)
        {
            $model = $model->where('from_user', $this->request->from_user);
        }
        //
        if($this->request->to_user)
        {
            $model = $model->where('to_user', $this->request->to_user);
        }
        //
        if($this->request->status)
        {
            $model = $model->where('status', $this->request->status);
        }
        return $model;
    }
}
