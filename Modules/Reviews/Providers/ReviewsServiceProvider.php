<?php

namespace Modules\Reviews\Providers;

use Illuminate\Support\ServiceProvider;

class ReviewsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->event();
        
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Modules\Reviews\Repositories\ReviewsRepository',
            'Modules\Reviews\Repositories\ReviewsRepositoryEloquent'
        );
        
        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Reviews', 'Modules\Reviews\Facades\Reviews');
        });
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('reviews.php'),
        ]);
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'reviews'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/reviews');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/reviews';
        }, \Config::get('view.paths')), [$sourcePath]), 'reviews');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/reviews');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'reviews');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'reviews');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
    /* public function event
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function event()
    {
        
    }
}
