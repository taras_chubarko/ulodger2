<?php

namespace Modules\Reviews\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Reviews\Repositories\ReviewsRepository;
use Modules\Host\Events\HostCalcRating;


class ReviewsController extends Controller
{
    /*
     *
     */
    protected $reviews;
    /*
     *
     */
    public function __construct(ReviewsRepository $reviews)
    {
        $this->reviews = $reviews;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('reviews::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('reviews::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(\Modules\Reviews\Http\Requests\MakeReq $request)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('reviews::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    /* public function more
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function more($id)
    {
        $reviews = $this->reviews->whereUser($id);
        $result['result'] = view('site::reviews.more-reviews', compact('reviews'))->render();
        return response()->json($result, 200);
    }
    /* public function add_to_host
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function add_to_host(\Modules\Reviews\Http\Requests\CreateReq $request, $id)
    {
        $host = \Host::find($id);
        $user = \Sentinel::getUser();
        //$reviews = $this->reviews->find
        $review = $this->reviews->create([
            'from_user'     => $user->id,
            'to_user'       => $host->user_id,
            'host_id'       => $host->id,
            'body'          => $request->body,
            'rating'        => round(($request->accuracy+$request->communication+$request->cleanliness+$request->location+$request->check_in+$request->value)/6),
            'accuracy'      => $request->accuracy,
            'communication' => $request->communication,
            'cleanliness'   => $request->cleanliness,
            'location'      => $request->location,
            'check_in'      => $request->check_in,
            'value'         => $request->value,
            'book_id'       => $request->book_id,
            'status'        => 1,
        ]);
        
        event(new HostCalcRating($host->id));
        
        return response()->json($review, 200);
    }
    /* public function reviews_host_json
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function reviews_host_json($host_id, $type)
    {
        $reviews = $this->reviews->host($host_id, $type);
        
        return response()->json($reviews, 200);
    }
    /* public function reviews_host
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function reviews_host($host_id)
    {
        $reviews = $this->reviews->orderBy('created_at', 'DESC')->findWhere(['host_id' => $host_id, 'status' => 1]);
        $reviews->load('fromUser', 'helpfull');
        $highest = $reviews->whereIn('rating', [4,5]);
        $lowest = $reviews->whereIn('rating', [1,2,3]);
        
        $result = [
            'all' => [
                'count' => $reviews->count(),
                'data'  => $reviews,
            ],
            'highest' => [
                'count' => $highest->count(),
                'data'  => $highest,
            ],
            'lowest' => [
                'count' => $lowest->count(),
                'data'  => $lowest,
            ]
        ];
        
        
        return response()->json($result, 200);
    }
}
