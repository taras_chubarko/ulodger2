<?php

namespace Modules\Reviews\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Reviews\Repositories\ReviewsRepository;

class AdminReviewsController extends Controller
{
    /*
     *
     */
    protected $reviews;
    /*
     *
     */
    public function __construct(ReviewsRepository $reviews)
    {
        $this->reviews = $reviews;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $this->reviews->pushCriteria(app('Modules\Reviews\Criteria\AdminReviewsCriteria', [$request]));
        $reviews = $this->reviews->paginate();
        return view('admin::reviews.index', compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::reviews.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(\Modules\Reviews\Http\Requests\MakeReq $request)
    {
        $reviews = $this->reviews->make($request);
        return redirect()->route('admin.reviews.index')->with('success', trans('site.reviews.jIp7wuW8M1vexLgI2ACU'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $review = $this->reviews->find($id);
        return view('admin::reviews.edit', compact('review'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(\Modules\Reviews\Http\Requests\MakeReq $request, $id)
    {
        $reviews = $this->reviews->change($request, $id);
        return redirect()->route('admin.reviews.index')->with('success', trans('site.reviews.5C6lDhNfpbSnjeyEOkS3'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $review = $this->reviews->find($id);
        
       $this->reviews->delete($id);
        event(new \Modules\Reviews\Events\DeleteEvent($review));
    }
}
