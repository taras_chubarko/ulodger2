<?php

Route::group(['middleware' => 'web', 'prefix' => 'reviews', 'namespace' => 'Modules\Reviews\Http\Controllers'], function()
{
    Route::get('/', 'ReviewsController@index');
    
    Route::post('{id}/more', [
	'as' 	=> 'reviews.more',
	'uses' 	=> 'ReviewsController@more'
    ]);
    
    Route::post('{id}/host', [
	'as' 	=> 'reviews.host',
	'uses' 	=> 'ReviewsController@add_to_host'
    ]);
    
    Route::get('{host_id}/host/{type}/json', [
	'as' 	=> 'reviews.host.json',
	'uses' 	=> 'ReviewsController@reviews_host_json'
    ]);
    
    Route::post('/host/{host_id}', [
	'as' 	=> 'reviews.host',
	'uses' 	=> 'ReviewsController@reviews_host'
    ]);
    
});
/*
 *
 */
Route::group(['middleware' => 'web', 'namespace' => 'Modules\Reviews\Http\Controllers'], function()
{
    Route::resource('reviews', 'ReviewsController');
});
/*
 * Админка
 */
Route::group(['middleware' => ['web', 'Modules\Cms\Http\Middleware\Admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\Reviews\Http\Controllers'], function()
{
    Route::resource('reviews', 'AdminReviewsController');
});