<?php

namespace Modules\Reviews\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateReq extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'accuracy'      => 'required',
            'communication' => 'required',
            'cleanliness'   => 'required',
            'location'      => 'required',
            'check_in'      => 'required',
            'value'         => 'required',
            'body'          => 'required',
        ];
    }
    /*
     *
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
     
        $validator->setAttributeNames([
            'accuracy'      => '"Accuracy"',
            'communication' => '"Communication"',
            'cleanliness'   => '"Cleanliness"',
            'location'      => '"Location"',
            'check_in'      => '"Check In"',
            'value'         => '"Value"',
            'body'          => '"Enter yuour comment"',
        ]);
     
        return $validator;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
