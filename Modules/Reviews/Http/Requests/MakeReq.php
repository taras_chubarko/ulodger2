<?php

namespace Modules\Reviews\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MakeReq extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from_user' => 'required',
            'to_user'   => 'required',
            'jobs_id'   => 'required',
            'body'      => 'required',
            'comity'    => 'required',
            'cost'      => 'required',
            'quality'   => 'required',
        ];
    }
    /*
     *
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
     
        $validator->setAttributeNames([
            'from_user' => '"Заказчик"',
            'to_user'   => '"Исполнитель"',
            'jobs_id'   => '"Задание"',
            'body'      => '"Отзыв"',
            'comity'    => '"Вежливость"',
            'cost'      => '"Стоимость"',
            'quality'   => '"Качество"',
        ]);
     
        return $validator;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
