<?php

namespace Modules\Reviews\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class ReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('en_US');
        
        $users = \User::findWhere([['id', '<>', 0]]);
        $users = $users->pluck('id')->toArray();
        
        foreach(range(1, 100) as $i)
        {
            $review = \Reviews::create([
                'from_user'     => $faker->randomElement($users),
                'to_user'       => 11,
                'host_id'       => 65,
                'body'          => $faker->text(500),
                'accuracy'      => $faker->randomElement([1,2,3,4,5]),
                'communication' => $faker->randomElement([1,2,3,4,5]),
                'cleanliness'   => $faker->randomElement([1,2,3,4,5]),
                'location'      => $faker->randomElement([1,2,3,4,5]),
                'check_in'      => $faker->randomElement([1,2,3,4,5]),
                'value'         => $faker->randomElement([1,2,3,4,5]),
                'book_id'       => 1,
                'status'        => 1,
            ]);
            
            $review->rating = round(($review->accuracy+$review->communication+$review->cleanliness+$review->location+$review->check_in+$review->value)/6);
            $review->save();
            
        }
    }
}
