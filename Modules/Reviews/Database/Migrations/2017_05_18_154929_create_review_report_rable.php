<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewReportRable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews_report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reviews_id')->unsigned()->nullable();
            $table->foreign('reviews_id')->references('id')->on('reviews')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('type')->unsigned()->nullable();
            $table->string('ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
}
