<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from_user')->unsigned()->nullable();
            $table->foreign('from_user')->references('id')->on('users')->onDelete('cascade');
            $table->integer('to_user')->unsigned()->nullable();
            $table->foreign('to_user')->references('id')->on('users')->onDelete('cascade');
            $table->integer('host_id')->unsigned()->nullable();
            $table->foreign('host_id')->references('id')->on('host')->onDelete('cascade');
            $table->text('body');
            $table->integer('accuracy');
            $table->integer('communication');
            $table->integer('cleanliness');
            $table->integer('location');
            $table->integer('check_in');
            $table->integer('value');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
