<?php namespace Modules\Reviews\Listeners;

use Illuminate\Http\Request;
use Illuminate\Contracts\Mail\Mailer;

class ReviewsEventListener
{
    public $request;
    protected $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request, Mailer $mailer)
    {
        $this->request = $request;
        $this->mailer = $mailer;
    }
    /* public function onMake
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function onMake($event)
    {
        $review = $event->review;
        // Калькуляция рейтинга
        \Reviews::calcRating($review);
        // Отправка почты при создании
        \Reviews::sendMail($review);
    }
    /* public function onChange
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function onChange($event)
    {
        $review = $event->review;
        // Калькуляция рейтинга
        \Reviews::calcRating($review);
    }
    /* public function onDelete
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function onDelete($event)
    {
        $review = $event->review;
        // Калькуляция рейтинга
        \Reviews::calcRating($review);
    }
    /**
     * Регистрация слушателей для подписки.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Modules\Reviews\Events\MakeEvent',
            'Modules\Reviews\Listeners\ReviewsEventListener@onMake'
        );
        
        $events->listen(
            'Modules\Reviews\Events\ChangeEvent',
            'Modules\Reviews\Listeners\ReviewsEventListener@onChange'
        );
        
        $events->listen(
            'Modules\Reviews\Events\DeleteEvent',
            'Modules\Reviews\Listeners\ReviewsEventListener@onDelete'
        );
        
        
        
    }

}