<?php

namespace Modules\Reviews\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Reviews\Repositories\ReviewsRepository;
use Modules\Reviews\Entities\Reviews;
// Event
use Modules\Reviews\Events\MakeEvent;
use Modules\Reviews\Events\ChangeEvent;

/**
 * Class ReviewsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ReviewsRepositoryEloquent extends BaseRepository implements ReviewsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Reviews::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /* public function make($request)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function make($request)
    {
        $user = \Sentinel::getUser();
        $review = $this->model->create([
            'from_user' => ($request->from_user) ? $request->from_user : $user->id,
            'to_user'   => $request->to_user,
            'jobs_id'   => $request->jobs_id,
            'bid_id'    => $request->bid_id,
            'body'      => $request->body,
            'comity'    => $request->comity,
            'quality'   => $request->quality,
            'cost'      => $request->cost,
            'status'    => ($request->status) ? $request->status : 1,
        ]);
        event(new MakeEvent($review));
        return $review;
    }
    /* public function change($request, $id);
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function change($request, $id)
    {
        $user = \Sentinel::getUser();
        $review = self::update([
            'from_user' => ($request->from_user) ? $request->from_user : $user->id,
            'to_user'   => $request->to_user,
            'jobs_id'   => $request->jobs_id,
            'bid_id'    => $request->bid_id,
            'body'      => $request->body,
            'comity'    => $request->comity,
            'quality'   => $request->quality,
            'cost'      => $request->cost,
            'status'    => ($request->status) ? $request->status : 1,
        ], $id);
        event(new ChangeEvent($review));
        return $review;
    }
    /* public function calcRating($review)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function calcRating($review)
    {
        $reviews = $this->model->where('to_user', $review->to_user)->where('status', 1)->get();
        // К-во оценок
        $comity_count   = $reviews->pluck('comity')->count();
        $quality_count  = $reviews->pluck('quality')->count();
        $cost_count     = $reviews->pluck('cost')->count();
        // Сума оценок
        $comity_sum     = $reviews->pluck('comity')->sum();
        $quality_sum    = $reviews->pluck('quality')->sum();
        $cost_sum       = $reviews->pluck('cost')->sum();
        // Среднее значение
        $comity     = $comity_sum / $comity_count;
        $quality    = $quality_sum / $quality_count;
        $cost       = $cost_sum / $cost_count;
        // Общий рейтинг
        $rating = ($comity + $quality + $cost) / 3;
        // Обновляей рейтинг пользователя
        $user = \User::find($review->to_user);
        $user->rating()->detach();
        $user->rating()->attach($user->id, [
            'rating'    => $rating,
            'comity'    => $comity,
            'quality'   => $quality,
            'cost'      => $cost,
        ]);
        
        
    }
    /* public function sendMail($review)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function sendMail($review)
    {
        $review->load('fromUser', 'toUser', 'job', 'bid');
        $user = $review->toUser;
        $data = $review->toArray();
        \Mail::later(5, 'site::reviews.mail-reviews', $data, function($message) use ($user)
        {
            $message->to($user->email)->subject('У вас новый отзыв на сайте "'.config('app.name').'"');
        });
    }
    /* public function user($user)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user($user)
    {
        $reviews = $this->whereUser($user->id);
        $reviews->load('fromUser', 'toUser', 'job', 'bid');
        //dd($reviews);
        return view('site::reviews.block-user-reviews', compact('reviews', 'user'));
    }
    /* public function whereUser($user_id)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function whereUser($user_id)
    {
        $reviews = $this->model->where('to_user', $user_id)->orderBy('created_at', 'DESC')->paginate();
        $reviews->setPath(route('reviews.more', $user_id));
        return $reviews;
    }
    /* public function fromUserArr
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function fromUserArr()
    {
        $reviews = $this->model->all();
        $reviews->load('fromUser');
        $users = $reviews->pluck('fromUser')->unique();
        $data = [];
        foreach($users as $user)
        {
            $data[$user->id] = $user->fullname;
        }
        return $data;
    }
    /* public function toUserArr
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function toUserArr()
    {
        $reviews = $this->model->all();
        $reviews->load('toUser');
        $users = $reviews->pluck('toUser')->unique();
        $data = [];
        foreach($users as $user)
        {
            $data[$user->id] = $user->fullname;
        }
        return $data;
    }
    /* public function jobArr
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function jobArr()
    {
        $reviews = $this->model->all();
        $reviews->load('job');
        $jobs = $reviews->pluck('job')->unique();
        $data = [];
        foreach($jobs as $job)
        {
            $data[$job->id] = $job->name;
        }
        return $data;
    }
    /* public function host($host_id)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function host($host_id, $type)
    {
        $q = $this->model->orderBy('created_at', 'DESC');
        $q->with('fromUser.avatar', 'helpfull');
        $q->where('host_id', $host_id);
        $q->where('status', 1);
        //
        if($type == 'highest')
        {
            $q->whereIn('rating', [4,5]);
            $q->orderBy('rating', 'ASC');
        }
        //
        if($type == 'lowest')
        {
            $q->whereIn('rating', [1,2,3]);
            $q->orderBy('rating', 'ASC');
        }
        if(\Request::has('query'))
        {
            $q->where('body', 'like', '%'.\Request::get('query').'%');
        }
        //
        $reviews = $q->paginate();
        
        return $reviews;
    }
}
