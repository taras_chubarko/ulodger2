<?php

namespace Modules\Reviews\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReviewsRepository
 * @package namespace App\Repositories;
 */
interface ReviewsRepository extends RepositoryInterface
{
    /*
     * Создание отзыва
     */
    public function make($request);
    /*
     * Редактироапние отзыва
     */
    public function change($request, $id);
    /*
     * Калькуляция рейтинга
     */
    public function calcRating($review);
    /*
     * Отправка почты после создания отзыва.
     */
    public function sendMail($review);
    /*
     * Отзывы о пользователе
     */
    public function user($user);
    /*
     *
     */
    public function whereUser($user_id);
    /*
     *
     */
    public function fromUserArr();
    /*
     *
     */
    public function toUserArr();
    /*
     *
     */
    public function jobArr();
    /*
     *
     */
    public function host($host_id, $type);
}
