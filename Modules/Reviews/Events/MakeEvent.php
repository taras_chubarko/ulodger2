<?php

namespace Modules\Reviews\Events;

use Illuminate\Queue\SerializesModels;

class MakeEvent
{
    use SerializesModels;
    public $review;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($review)
    {
        $this->review = $review;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
