<?php namespace Modules\Reviews\Facades;

use Illuminate\Support\Facades\Facade;

class Reviews extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Modules\Reviews\Repositories\ReviewsRepository';
    }
}