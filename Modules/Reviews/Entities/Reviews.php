<?php

namespace Modules\Reviews\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Reviews extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'reviews';

    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    /* public function fromUser
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function fromUser()
    {
        return $this->hasOne('Modules\User\Entities\User', 'id', 'from_user');
    }
    /* public function toUser
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function toUser()
    {
        return $this->hasOne('Modules\User\Entities\User', 'id', 'to_user');
    }
    /* public function job
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function host()
    {
        return $this->hasOne('Modules\Host\Entities\Host', 'id', 'host_id')->withTrashed();
    }
    /* public function bid
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function book()
    {
        return $this->hasOne('Modules\Host\Entities\HostBook', 'id', 'book_id');
    }
    /* public function helpfull
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function helpfull()
    {
        return $this->hasMany('Modules\Reviews\Entities\ReviewsHelpfull', 'reviews_id', 'id');
    }
    /* public function getFieldCreatedAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldCreatedAttribute()
    {
        $field = new \stdClass;
        $field->created_at = $this->created_at;
        $field->text = rusishData($this->created_at);
        return $field;
    }
    /* public function getFieldStatusAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldStatusAttribute()
    {
        $field = new \stdClass;
        $field->status = config('reviews.status.'.$this->status);
        $field->label = null;
        $label[0] = 'danger';
        $label[1] = 'success';
        
        $field->label = '<span class="label label-'.$label[$this->status].'">'.$field->status.'</span>';
        
        return $field;
    }

}
