<?php

namespace Modules\Reviews\Entities;

use Illuminate\Database\Eloquent\Model;

class ReviewsHelpfull extends Model
{
    protected $table = 'reviews_helpfull';

    protected $fillable = [];
    
    protected $guarded = ['_token'];
}
