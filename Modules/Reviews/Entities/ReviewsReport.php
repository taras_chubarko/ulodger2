<?php

namespace Modules\Reviews\Entities;

use Illuminate\Database\Eloquent\Model;

class ReviewsReport extends Model
{
    protected $table = 'reviews_report';

    protected $fillable = [];
    
    protected $guarded = ['_token'];
}
