<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::layouts.index');
    }
    /* public function test
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function test()
    {
        //$mail = \Mail::raw('Text to e-mail', function ($message) {
        //    $message->to('tchubarko@gmail.com')->subject('Test1!');
        //});
        //
        //dd($mail);
        $sms = \Nexmo::message()->send([
            'to'   => 14079218209,
            'from' => 12014643496,
            'text' => 'Test2.'
        ]);
        
        dd($sms);
    }
}
