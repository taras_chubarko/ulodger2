<?php

Route::group(['middleware' => ['web', 'Modules\Admin\Http\Middleware\AdminMDW'], 'prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
    Route::get('/', 'AdminController@index');
    Route::get('/test', 'AdminController@test');
});

Route::group(['middleware' => ['web', 'Modules\Admin\Http\Middleware\AdminMDW'], 'namespace' => 'App\Http\Controllers'], function()
{
    Route::get('filemanager/show', 'FilemanagerLaravelController@getShow');
    Route::get('filemanager/connectors', 'FilemanagerLaravelController@getConnectors');
    Route::post('filemanager/connectors', 'FilemanagerLaravelController@postConnectors');
});