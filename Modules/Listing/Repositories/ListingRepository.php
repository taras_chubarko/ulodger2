<?php

namespace Modules\Listing\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ListingRepository
 * @package namespace App\Repositories;
 */
interface ListingRepository extends RepositoryInterface
{
    //
}
