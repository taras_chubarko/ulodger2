<?php

namespace Modules\Listing\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Listing\Entities\Listing;
use Modules\Listing\Repositories\ListingRepository;
use Modules\Listing\Entities\BookListing;

class ListingController extends Controller
{
    protected $model;
    protected $repository;
    protected $book;

    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(Listing $model, ListingRepository $repository, BookListing $book)
    {
        $this->model = $model;
        $this->repository = $repository;
        $this->book = $book;
    }
    /* public function get_rentout
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_rentout()
    {
        $items = \Modules\Taxonomy\Entities\TaxonomyTerm::where('taxonomy_id', 1)->get();
        return response()->json($items, 200);
    }

    /* public function get_property_type
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_property_type()
    {
        $items = \Modules\Taxonomy\Entities\TaxonomyTerm::where('taxonomy_id', 2)->get();
        return response()->json($items, 200);
    }

    /* public function get_guests
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_guests()
    {
        $data = config('host.guests');
        $items = [];
        foreach ($data as $k => $guests)
        {
            $items[] = [
                'id' => $k,
                'name' => $guests,
            ];
        }
        return $items;
    }

    /* public function get_bedrooms
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_bedrooms()
    {
        $data = config('host.bedrooms');
        $items = [];
        foreach ($data as $k => $bedrooms)
        {
            $items[] = [
                'id' => $k,
                'name' => $bedrooms,
            ];
        }
        return $items;
    }

    /* public function get_beds
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_beds()
    {
        $data = config('host.beds');
        $items = [];
        foreach ($data as $k => $beds)
        {
            $items[] = [
                'id' => $k,
                'name' => $beds,
            ];
        }
        return $items;
    }

    /* public function get_bathrooms
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_bathrooms()
    {
        $data = config('host.bathrooms');
        $items = [];
        foreach ($data as $k => $bathrooms)
        {
            $items[] = [
                'id' => $k,
                'name' => $bathrooms,
            ];
        }
        return $items;
    }

    /* public function get_amenities
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_amenities()
    {
        $items = \Modules\Taxonomy\Entities\TaxonomyTerm::where('taxonomy_id', 4)->get();
        return response()->json($items, 200);
    }

    /* public function get_safety
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_safety()
    {
        $items = \Modules\Taxonomy\Entities\TaxonomyTerm::where('taxonomy_id', 5)->get();
        return response()->json($items, 200);
    }

    /* public function get_spaces
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_spaces()
    {
        $items = \Modules\Taxonomy\Entities\TaxonomyTerm::where('taxonomy_id', 6)->get();
        return response()->json($items, 200);
    }

    /* public function get_lodgers
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_lodgers()
    {
        $data = config('host.guests');
        $items = [];
        foreach ($data as $k => $lodgers)
        {
            $items[] = [
                'id' => $k,
                'name' => $lodgers,
            ];
        }
        return $items;
    }

    /* public function create
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function create(Request $request)
    {
        $add = $this->model->create([
            'user_id'   => \Sentinel::getUser()->id,
            'name'      => $request->name,
            'status'    => 1,
            'progress'  => 100,
            'step'      => 7,
        ]);
        //
        $data = $request->all();
        //
        foreach ($data as $key => $datum) {
            if($request->has($key))
            {
                $add->$key = $datum;
            }
        }
        //
        return response()->json($add, 200);
    }

    /* public function find
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function find(Request $request)
    {
        $this->repository->pushCriteria(\Modules\Listing\Criteria\ListingCriteria::class);
        $listing = $this->repository->paginate(20);
        
        $listing->getCollection()->transform(function ($value) {
            $data = [
                'id'        => $value->id,
                'name'      => $value->name,
                'slug'      => $value->slug,
                'price'     => $value->price,
                'rating'    => $value->rating,
                'photo'     => $value->photo,
                'location'  => $value->location,
            ];
            return $data;
        });
        return response()->json($listing, 200);
    }

    /* public function minmaxprice
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function minmaxprice()
    {
        $min = \DB::table('listing')->join('listing_price', 'listing.id', '=', 'listing_price.listing_id')->min('price');
        $max = \DB::table('listing')->join('listing_price', 'listing.id', '=', 'listing_price.listing_id')->max('price');
        $min = number_format($min,  0, '.', '');
        $max = number_format($max,  0, '.', '');
        return response()->json([$min, $max], 200);
    }

    /* public function edit
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function edit($slug)
    {
        $listing = $this->model->where('slug', $slug)->first();

        $edit['step1'] = [
            'rent_out'      => $listing->rent_out->value,
            'property_type' => $listing->property_type->value,
            'name'          => $listing->name,
            'description'   => $listing->description->value,
        ];
        //
        $edit['step2'] = [
            'smoking'   => $listing->smoking->value,
            'guests'    => $listing->guests->value,
            'bedrooms'  => $listing->bedrooms->value,
            'beds'      => $listing->beds->value,
            'bath_type' => $listing->bath_type->value,
            'bathrooms' => $listing->bathrooms->value,
        ];
        //
        $edit['step3'] = [
            'location' => [
                'address'   => $listing->location->address,
                'region'    => $listing->location->region,
                'locality'  => $listing->location->locality,
                'lat'       => $listing->location->lat,
                'lng'       => $listing->location->lng,
            ],
            'point' => [
                'lat'       => $listing->location->lat,
                'lng'       => $listing->location->lng,
            ],
            'schoolslocaton' => $listing->schoolslocaton->values,
        ];
        //
        $edit['step4'] = [
            'amenities' => $listing->amenities->arr,
        ];
        //spaces
        $edit['step5'] = [
            'spaces' => $listing->spaces->arr,
        ];
        //photo
        $edit['step6'] = [
            'photo' => $listing->photo->values,
        ];
        //
        $edit['step7'] = [
            'available' => $listing->available->value,
            'price' => $listing->price->data,
        ];

        return response()->json($edit, 200);
    }

    /* public function update
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function update(Request $request, $slug)
    {
        $listing = $this->model->where('slug', $slug)->first();
        $listing->name = $request->name;
        $listing->progress = 100;
        $listing->step = 7;
        $listing->save();

        $data = $request->all();

        foreach ($data as $key => $datum) {
            if($request->has($key))
            {
                $listing->$key = $datum;
            }
        }
        return response()->json($listing, 200);
    }

    /* public function slug
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function slug($slug)
    {
        $listing = $this->model->where('slug', $slug)
            ->whereHas('userListing', function($query){
                $query->whereNull('deleted_at');
            })
            ->first();
        if($listing)
        {
            $listing->getMyAttributes('all');
            $listing = $listing->toArray();
            return response()->json($listing, 200);
        }
        else
        {
            return response()->json('error', 404);
        }
    }

    /* public function my_listing_data
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function my_listing_data()
    {
        $user = \Sentinel::getUser();
        $listing = $this->model->where('user_id', $user->id)->orderBy('created_at', 'DESC')->paginate(10);
        $listing->getCollection()->transform(function ($value) {
            $data = [
                'id'        => $value->id,
                'name'      => $value->name,
                'photo'     => ($value->photo->values) ? $value->photo->values[0]->filename : 'noim-min.png',
                'slug'      => $value->slug,
                'progress'  => $value->progress,
                'description' => $value->description,
                'updated_at' => $value->updated_at->format('m/d/Y'),
            ];
            return $data;
        });
        return $listing;
    }
    /* public function my_listing
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function my_listing()
    {
        $listing = $this->my_listing_data();
        return response()->json($listing, 200);
    }

    /* public function my_requests
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function my_requests()
    {
        $user = \Sentinel::getUser();
        $book = $this->book->where('user_id', $user->id)->orderBy('created_at', 'DESC')->paginate(10);
        $book->getCollection()->transform(function ($value) {
            $data = [
                'id'        => $value->listing->id,
                'name'      => $value->listing->name,
                'photo'     => ($value->listing->photo->values) ? $value->listing->photo->values[0]->filename : 'noim-min.png',
                'slug'      => $value->listing->slug,
                'req' => [
                    'id'            => $value->id,
                    'check_in'      => $value->check_in,
                    'check_out'     => $value->check_out,
                    'lodgers'       => $value->lodgers,
                    'remaining'     => $value->remaining,
                    'status'        => $value->status,
                    'created_at'    => $value->created_at->format('m/d/Y'),
                ]
            ];
            return $data;
        });
        return response()->json($book, 200);
    }

    /* public function my_reservations
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function my_reservations()
    {
        $user = \Sentinel::getUser();
        $book = $this->book->where('listing_user_id', $user->id)->orderBy('created_at', 'DESC')->paginate(10);
        $book->getCollection()->transform(function ($value) {
            $data = [
                'id'        => $value->listing->id,
                'name'      => $value->listing->name,
                'photo'     => ($value->listing->photo->values) ? $value->listing->photo->values[0]->filename : 'noim-min.png',
                'slug'      => $value->listing->slug,
                'user' => [
                    'id'            => $value->user->id,
                    'first_name'    => $value->user->first_name,
                    'slug'          => $value->user->slug,
                ],
                'req' => [
                    'id'            => $value->id,
                    'check_in'      => $value->check_in,
                    'check_out'     => $value->check_out,
                    'lodgers'       => $value->lodgers,
                    'remaining'     => $value->remaining,
                    'status'        => $value->status,
                    'created_at'    => $value->created_at->format('m/d/Y'),
                ]
            ];
            return $data;
        });
        return response()->json($book, 200);
    }

    /* public function destroy
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function destroy($id)
    {
        $this->model->where('id', $id)->delete();
        $listing = $this->my_listing_data();
        return response()->json($listing, 200);
    }
}
