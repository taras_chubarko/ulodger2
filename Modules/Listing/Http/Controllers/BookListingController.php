<?php

namespace Modules\Listing\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Listing\Entities\BookListing;
use Modules\Inbox\Entities\Inbox;
use Modules\Listing\Entities\Listing;
use Modules\Invoice\Repositories\InvoiceRepository;

class BookListingController extends Controller
{
    protected $model;
    protected $inbox;
    protected $listing;
    protected $invoiceRepository;

    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(BookListing $model, Inbox $inbox, Listing $listing, InvoiceRepository $invoiceRepository)
    {
        $this->model = $model;
        $this->inbox = $inbox;
        $this->listing = $listing;
        $this->invoiceRepository = $invoiceRepository;
    }

    /* public function book
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function book(Request $request)
    {
        $user = \Sentinel::getUser();
        $book = $this->model->where('user_id', $user->id)->where('listing_id', $request->listing_id)->whereIn('status', [0,1])->first();
        if(!$book)
        {
            $book = $this->model->create([
                'listing_id'        => $request->listing_id,
                'user_id'           => $user->id,
                'listing_user_id'   => $request->listing_user_id,
                'check_in'          => $request->check_in,
                'check_out'         => $request->check_out,
                'lodgers'           => $request->lodgers,
                'status'            => 0,
            ]);
            //
            $listing = $this->listing->find($request->listing_id);
            //
            $this->inbox->create([
                'user_id_from'  => $user->id,
                'user_id_to'    => $request->listing_user_id,
                'type'          => 'requests',
                'subject'       => '<span>New Reservation</span> - '.$listing->name,
                'message'       => 'Hi, I just sent you a lodging request on one of your listings. Please see your “My Requests” page within “My Listings” to respond to the request. Thank you, Lodger.',
                'status'        => 1,
                'parent_id'     => 0,
                'read'          => 0,
                'request_id'    => $book->id,
            ]);

            $result['success'] = 'You have booked this listing. Wait for confirmation from the owner.';
            return response()->json($result, 200);
        }
        else
        {
            if($book->status == 0)
            {
                $result['error'] = 'You have already applied for a booking.';
            }
            //
            if($book->status == 1)
            {
                $result['error'] = 'You have already booked this listing.';
            }
            return response()->json($result, 442);
        }
    }

    /* public function cancel
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function cancel(Request $request)
    {
        $user = \Sentinel::getUser();
        $book = $this->model->where('id', $request->id)->update(['status' => 10]);
        //
        $listing = $this->listing->find($request->listing_id);
        //
        $this->inbox->create([
            'user_id_from'  => $user->id,
            'user_id_to'    => $request->user_id_to,
            'type'          => 'message',
            'subject'       => '<span>Cancel Reservation</span> - '.$listing->name,
            'message'       => $request->message,
            'status'        => 1,
            'parent_id'     => 0,
            'read'          => 0,
            'request_id'    => null,
        ]);
        //
        return response()->json('ok', 200);
    }

    /* public function apply
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function apply(Request $request)
    {
        $user = \Sentinel::getUser();
        $this->model->where('id', $request->id)->update([
            'check_in' => \Carbon::parse($request->check_in),
            'check_out' => \Carbon::parse($request->check_out),
            'lodgers' => $request->lodgers,
            'status' => 100
        ]);
        //
        $book = $this->model->find($request->id);
        //
        $listing = $this->listing->find($request->listing_id);
        $listing->status = 2;
        $listing->save();
        // create invoice
        $this->invoiceRepository->add($book, $listing);
        //
        $this->inbox->create([
            'user_id_from'  => $user->id,
            'user_id_to'    => $book->user_id,
            'type'          => 'message',
            'subject'       => '<span>Apply Reservation</span> - '.$listing->name,
            'message'       => 'Congratulations!!! Your lodging request has been approved; now it is time to make a payment. Please navigate to “Payments” page. Thank you, Ulodger team.',
            'status'        => 1,
            'parent_id'     => 0,
            'read'          => 0,
            'request_id'    => null,
        ]);
        //
        return response()->json('ok', 200);
    }
}
