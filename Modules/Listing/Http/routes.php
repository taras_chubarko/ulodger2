<?php

Route::group(['middleware' => 'web', 'prefix' => 'api/v1/listing', 'namespace' => 'Modules\Listing\Http\Controllers'], function()
{
    Route::get('/', 'ListingController@index');
    Route::get('/get/rentout', 'ListingController@get_rentout');
    Route::get('/get/propertytype', 'ListingController@get_property_type');
    Route::get('/get/guests', 'ListingController@get_guests');
    Route::get('/get/bedrooms', 'ListingController@get_bedrooms');
    Route::get('/get/beds', 'ListingController@get_beds');
    Route::get('/get/bathrooms', 'ListingController@get_bathrooms');
    Route::get('/get/amenities', 'ListingController@get_amenities');
    Route::get('/get/safety', 'ListingController@get_safety');
    Route::get('/get/spaces', 'ListingController@get_spaces');
    Route::get('/get/lodgers', 'ListingController@get_lodgers');
    Route::post('/create', 'ListingController@create');
    Route::get('/find', 'ListingController@find');
    Route::get('/minmaxprice', 'ListingController@minmaxprice');
    Route::get('/edit/{slug}', 'ListingController@edit');
    Route::put('/update/{slug}', 'ListingController@update');
    Route::get('/{slug}', 'ListingController@slug');
    Route::get('/my-listing/get', 'ListingController@my_listing');
    Route::get('/my-listing/requests', 'ListingController@my_requests');
    Route::get('/my-listing/reservations', 'ListingController@my_reservations');
    Route::delete('/{id}', 'ListingController@destroy');

});

Route::group(['middleware' => 'web', 'prefix' => 'api/v1/listing/book', 'namespace' => 'Modules\Listing\Http\Controllers'], function()
{
    Route::post('/', 'BookListingController@book');
    Route::post('/cancel', 'BookListingController@cancel');
    Route::post('/apply', 'BookListingController@apply');
});
