<?php

namespace Modules\Listing\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ListingCriteria
 * @package namespace App\Criteria;
 */
class ListingCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('status', 1)->orderBy('created_at', 'DESC');


        $model = $model->whereHas('userListing', function($query){
            $query->whereNull('deleted_at');
        });

        //rent_out
        if(\Request::has('rent_out') && !empty(\Request::get('rent_out')) )
        {
            $model = $model->whereHas('listing_rent_out_table', function($query){
                $query->whereIn('rent_out', \Request::get('rent_out'));
            });
        }
        //price
        if(\Request::has('price'))
        {
            $model = $model->whereHas('listing_price_table', function($query){
                $query->whereBetween('price', \Request::get('price'));
            });
        }
        //bath_type
        if(\Request::has('bath_type') && !empty(\Request::get('bath_type')))
        {
            $model = $model->whereHas('listing_bath_type_table', function($query){
                $query->whereIn('bath_type', \Request::get('bath_type'));
            });
        }
        //bedrooms
        if(\Request::has('bedrooms') && !empty(\Request::get('bedrooms')))
        {
            $model = $model->whereHas('listing_bedrooms_table', function($query){
                $query->where('bedrooms', \Request::get('bedrooms'));
            });
        }
        //available
        if(\Request::has('move_in') && \Request::has('move_out'))
        {
            $model = $model->whereHas('listing_available_table', function($query){
                $query->whereDate('from', '<=', \Carbon::parse(\Request::get('move_in')));
                $query->whereDate('to', '>=', \Carbon::parse(\Request::get('move_out')));
            });
        }
        //schoolslocaton
        if(\Request::has('schoolslocaton'))
        {
            $ids = \DB::select('SELECT listing_id, ( 3959 * acos( cos( radians('.\Request::input('schoolslocaton.lat').') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.\Request::input('schoolslocaton.lng').') ) + sin( radians('.\Request::input('schoolslocaton.lat').') ) * sin( radians( lat ) ) ) ) AS distance FROM listing_location HAVING distance < '.\Request::input('distance').' ORDER BY distance;');
            $ids = collect($ids)->pluck('listing_id')->toArray();
            $model =  $model->whereIn('listing.id', $ids);
        }

        return $model;
    }
}
