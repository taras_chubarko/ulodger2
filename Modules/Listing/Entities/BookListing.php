<?php

namespace Modules\Listing\Entities;

use Illuminate\Database\Eloquent\Model;

class BookListing extends Model
{
    protected $table = 'book_listing';

    protected $fillable = [];

    protected $guarded = ['_token'];


    /* public function set
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setCheckInAttribute($value)
    {
        if($value)
        {
            $this->attributes['check_in'] = \Carbon::parse($value);
        }
    }

    /* public function getCheckInAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getCheckInAttribute($value)
    {
        return \Carbon::parse($value)->format('m/d/Y');
    }

    /* public function setCheckOutAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setCheckOutAttribute($value)
    {
        if($value)
        {
            $this->attributes['check_out'] = \Carbon::parse($value);
        }
    }

    /* public function getCheckOutAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getCheckOutAttribute($value)
    {
        return \Carbon::parse($value)->format('m/d/Y');
    }

    /* public function listing
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing()
    {
        return $this->hasOne(\Modules\Listing\Entities\Listing::class, 'id', 'listing_id');
    }

    /* public function user
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function user()
    {
        return $this->hasOne(\Modules\User\Entities\User::class, 'id', 'user_id');
    }

    /* public function getRemainingAttribute
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getRemainingAttribute()
    {
        $s_dt=substr($this->check_out,0,10);
        $num_days=ceil((strtotime($s_dt)-time())/86400);
        return $num_days;
    }
    
}
