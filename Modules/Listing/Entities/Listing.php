<?php

namespace Modules\Listing\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Listing extends Model implements Transformable
{
    use SoftDeletes;
    use TransformableTrait;
    use Sluggable;

    protected $table = 'listing';

    protected $fillable = [];

    protected $guarded = ['_token'];

    protected $dates = ['deleted_at'];

    protected $appends = [];

    protected $myAttributes = [
        'location',
        'rating',
        'photo',
        'description',
        'user',
        'rent_out',
        'bath_type',
        'guests',
        'bedrooms',
        'beds',
        'smoking',
        'bathrooms',
        'amenities',
        'spaces',
        'price',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slughost'
            ]
        ];
    }
    /* public function getSlugjobAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getSlughostAttribute()
    {
        $gen = 9 - strlen($this->id);
        $numer = str_pad(mt_rand(0, 999999), $gen, '0', STR_PAD_LEFT).$this->id;
        return 'id-'.$numer;
    }

    /* public function getMyAttributes
    * @param
    *-----------------------------------
    *| Привязка атрибутов
    *-----------------------------------
    */
    public function getMyAttributes($attributes)
    {
        if(is_array($attributes))
        {
            $this->appends = $attributes;
        }
        else
        {
            if($attributes == 'all')
            {
                $this->appends = $this->myAttributes;
            }
        }
    }

    /* public function userListing
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function userListing()
    {
        return $this->hasOne(\Modules\User\Entities\User::class, 'id', 'user_id');
    }
    /* public function listing_amenities_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_amenities_table()
    {
        return $this->belongsToMany(\Modules\Taxonomy\Entities\TaxonomyTerm::class, 'listing_amenities', 'listing_id', 'amenities')->withPivot('amenities', 'sort');
    }
    /* public function listing_available_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_available_table()
    {
        return $this->belongsToMany(\Modules\Listing\Entities\Listing::class, 'listing_available')->withPivot('from', 'to');
    }
    /* public function listing_bath_type_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_bath_type_table()
    {
        return $this->belongsToMany(\Modules\Listing\Entities\Listing::class, 'listing_bath_type')->withPivot('bath_type');
    }
    /* public function listing_bathrooms_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_bathrooms_table()
    {
        return $this->belongsToMany(\Modules\Listing\Entities\Listing::class, 'listing_bathrooms')->withPivot('bathrooms');
    }
    /* public function listing_bedrooms_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_bedrooms_table()
    {
        return $this->belongsToMany(\Modules\Listing\Entities\Listing::class, 'listing_bedrooms')->withPivot('bedrooms');
    }
    /* public function listing_beds_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_beds_table()
    {
        return $this->belongsToMany(\Modules\Listing\Entities\Listing::class, 'listing_beds')->withPivot('beds');
    }
    /* public function listing_description_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_description_table()
    {
        return $this->belongsToMany(\Modules\Listing\Entities\Listing::class, 'listing_description')->withPivot('description');
    }
    /* public function listing_guests_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_guests_table()
    {
        return $this->belongsToMany(\Modules\Listing\Entities\Listing::class, 'listing_guests')->withPivot('guests');
    }
    /* public function listing_location_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_location_table()
    {
        return $this->belongsToMany(\Modules\Listing\Entities\Listing::class, 'listing_location')->withPivot('address', 'locality', 'region', 'lat', 'lng');
    }
    /* public function listing_photo_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_photo_table()
    {
        return $this->belongsToMany(\Modules\Filem\Entities\Filem::class, 'listing_photo', 'listing_id', 'fid', 'sort')->withPivot('fid', 'sort');
    }
    /* public function listing_price_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_price_table()
    {
        return $this->belongsToMany(\Modules\Listing\Entities\Listing::class, 'listing_price')->withPivot('price');
    }
    /* public function listing_property_type_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_property_type_table()
    {
        return $this->belongsToMany(\Modules\Taxonomy\Entities\TaxonomyTerm::class, 'listing_property_type', 'listing_id', 'property_type')->withPivot('property_type');
    }
    /* public function listing_rating_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_rating_table()
    {
        return $this->belongsToMany(\Modules\Listing\Entities\Listing::class, 'listing_rating')->withPivot('rating');
    }
    /* public function listing_rent_out_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_rent_out_table()
    {
        return $this->belongsToMany(\Modules\Taxonomy\Entities\TaxonomyTerm::class, 'listing_rent_out', 'listing_id', 'rent_out')->withPivot('rent_out');
    }
    /* public function listing_schoolslocaton_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_schoolslocaton_table()
    {
        return $this->belongsToMany(\Modules\Listing\Entities\Listing::class, 'listing_schoolslocaton')->withPivot('name', 'address', 'locality', 'region', 'lat', 'lng', 'sort');
    }
    /* public function listing_smoking_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_smoking_table()
    {
        return $this->belongsToMany(\Modules\Listing\Entities\Listing::class, 'listing_smoking')->withPivot('smoking');
    }
    /* public function listing_spaces_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing_spaces_table()
    {
        return $this->belongsToMany(\Modules\Taxonomy\Entities\TaxonomyTerm::class, 'listing_spaces', 'listing_id', 'spaces', 'sort')->withPivot('spaces', 'sort');
    }
    /* public function setAmenitiesAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setAmenitiesAttribute($value)
    {
        $this->listing_amenities_table()->detach();
        if($value)
        {
            $i = 0;
            foreach ($value as $amenities)
            {
                $this->listing_amenities_table()->attach($this->id, [
                    'amenities' => $amenities,
                    'sort' => $i,
                ]);
                $i++;
            }

        }
    }

    /* public function getAmenitiesAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getAmenitiesAttribute()
    {
        $field = new \stdClass;
        $field->title = 'amenities';
        $field->values = [];
        $field->arr = [];
        if($this->listing_amenities_table->count())
        {
            foreach ($this->listing_amenities_table as $item) {
                $field->values[] = [
                    'id'    => $item->id,
                    'name'  => $item->name,
                ];
                $field->arr[] = $item->id;
            }
        }
        unset($this->listing_amenities_table);
        return $field;
    }

    /* public function setAvailableAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setAvailableAttribute($value)
    {
        $this->listing_available_table()->detach();
        if($value)
        {
            $this->listing_available_table()->attach($this->id, [
                'from' => \Carbon::parse($value['from']),
                'to' => \Carbon::parse($value['to']),
            ]);
        }
    }

    /* public function getAvailableAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getAvailableAttribute()
    {
        $field = new \stdClass;
        $field->title = 'available';
        $available = data_get($this, 'listing_available_table.0.pivot');
        $field->value = [
            'from' => \Carbon::parse($available->from)->format('m/d/Y'),
            'to' => \Carbon::parse($available->to)->format('m/d/Y'),
        ];

        unset($this->listing_available_table);
        return $field;
    }

    /* public function setBathTypeAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setBathTypeAttribute($value)
    {
        $this->listing_bath_type_table()->detach();
        if($value)
        {
            $this->listing_bath_type_table()->attach($this->id, [
                'bath_type' => $value,
            ]);
        }
    }

    /* public function getBathTypeAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getBathTypeAttribute()
    {
        $field = new \stdClass;
        $field->title = 'bath_type';
        $field->value = data_get($this, 'listing_bath_type_table.0.pivot.bath_type');
        $field->name = ($field->value) ? config('host.bath_type.'.$field->value) : null;
        unset($this->listing_bath_type_table);
        return $field;
    }

    /* public function setBathroomsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setBathroomsAttribute($value)
    {
        $this->listing_bathrooms_table()->detach();
        if($value)
        {
            $this->listing_bathrooms_table()->attach($this->id, [
                'bathrooms' => $value,
            ]);
        }
    }

    /* public function getBathroomsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getBathroomsAttribute()
    {
        $field = new \stdClass;
        $field->title = 'bathrooms';
        $field->value = data_get($this, 'listing_bathrooms_table.0.pivot.bathrooms');
        $field->name = ($field->value) ? config('host.bathrooms.'.$field->value) : null;
        unset($this->listing_bathrooms_table);
        return $field;
    }

    /* public function setBedroomsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setBedroomsAttribute($value)
    {
        $this->listing_bedrooms_table()->detach();
        if($value)
        {
            $this->listing_bedrooms_table()->attach($this->id, [
                'bedrooms' => $value,
            ]);
        }
    }

    /* public function getBedroomsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getBedroomsAttribute()
    {
        $field = new \stdClass;
        $field->title = 'bedrooms';
        $field->value = data_get($this, 'listing_bedrooms_table.0.pivot.bedrooms');
        $field->name = ($field->value) ? config('host.bedrooms.'.$field->value) : null;
        unset($this->listing_bedrooms_table);
        return $field;
    }

    /* public function setBedsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setBedsAttribute($value)
    {
        $this->listing_beds_table()->detach();
        if($value)
        {
            $this->listing_beds_table()->attach($this->id, [
                'beds' => $value,
            ]);
        }
    }

    /* public function getBedsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getBedsAttribute()
    {
        $field = new \stdClass;
        $field->title = 'beds';
        $field->value = data_get($this, 'listing_beds_table.0.pivot.beds');
        $field->name = ($field->value) ? config('host.beds.'.$field->value) : null;
        unset($this->listing_beds_table);
        return $field;
    }

    /* public function setDescriptionAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setDescriptionAttribute($value)
    {
        $this->listing_description_table()->detach();
        if($value)
        {
            $this->listing_description_table()->attach($this->id, [
                'description' => $value,
            ]);
        }
    }

    /* public function getDescriptionAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getDescriptionAttribute()
    {
        $field = new \stdClass;
        $field->title = 'description';
        $field->value = data_get($this, 'listing_description_table.0.pivot.description');
        unset($this->listing_description_table);
        return $field;
    }

    /* public function setGuestsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setGuestsAttribute($value)
    {
        $this->listing_guests_table()->detach();
        if($value)
        {
            $this->listing_guests_table()->attach($this->id, [
                'guests' => $value,
            ]);
        }
    }

    /* public function getGuestsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getGuestsAttribute()
    {
        $field = new \stdClass;
        $field->title = 'guests';
        $field->value = data_get($this, 'listing_guests_table.0.pivot.guests');
        unset($this->listing_guests_table);
        return $field;
    }

    /* public function setLocationAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setLocationAttribute($value)
    {
        $this->listing_location_table()->detach();
        if($value)
        {
            $this->listing_location_table()->attach($this->id, [
                'address'   => $value['address'],
                'locality'  => $value['locality'],
                'region'    => $value['region'],
                'lat'       => $value['lat'],
                'lng'       => $value['lng'],
            ]);
        }
    }

    /* public function getLocationAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getLocationAttribute()
    {
        $field = new \stdClass;
        $field->title = 'location';

        $field->address     = data_get($this, 'listing_location_table.0.pivot.address');
        $field->region      = data_get($this, 'listing_location_table.0.pivot.region');
        $field->locality    = data_get($this, 'listing_location_table.0.pivot.locality');
        $field->lat         = (float) data_get($this, 'listing_location_table.0.pivot.lat');
        $field->lng         = (float) data_get($this, 'listing_location_table.0.pivot.lng');

        $addr[] = $field->locality;
        $addr[] = $field->region;
        $field->short_address = implode(', ', $addr);

        unset($this->listing_location_table);
        return $field;
    }

    /* public function setPhotoAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setPhotoAttribute($value)
    {
        $this->listing_photo_table()->detach();
        if($value)
        {
            $i = 0;
            foreach ($value as $k => $item) {
                if ($item) {
                    $this->listing_photo_table()->attach($this->id, [
                        'fid' => $item['id'],
                        'sort' => $i,
                    ]);
                }
                $i++;
            }
        }
    }

    /* public function getPhotoAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getPhotoAttribute()
    {
        $field = new \stdClass;
        $field->title = 'photo';
        $field->values = [];
        if($this->listing_photo_table->count())
        {
            foreach ($this->listing_photo_table as $item) {
                $field->values[] = $item;
            }
        }
        unset($this->listing_photo_table);
        return $field;
    }

    /* public function setPriceAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setPriceAttribute($value)
    {
        $this->listing_price_table()->detach();
        if($value)
        {
            $this->listing_price_table()->attach($this->id, [
                'price' => $value,
            ]);
        }
    }

    /* public function getPriceAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getPriceAttribute()
    {
        $field = new \stdClass;
        $field->title = 'price';
        $field->value = data_get($this, 'listing_price_table.0.pivot.price');
        $field->data = ($field->value) ? number_format($field->value, 0, '.', '') : 0;
        unset($this->listing_price_table);
        return $field;
    }

    /* public function setPropertyTypeAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setPropertyTypeAttribute($value)
    {
        $this->listing_property_type_table()->detach();
        if($value)
        {
            $this->listing_property_type_table()->attach($this->id, [
                'property_type' => $value,
            ]);
        }
    }

    /* public function getPropertyTypeAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getPropertyTypeAttribute()
    {
        $field = new \stdClass;
        $field->title = 'property_type';
        $field->value = data_get($this, 'listing_property_type_table.0.pivot.property_type');
        unset($this->listing_property_type_table);
        return $field;
    }

    /* public function setRatingAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setRatingAttribute($value)
    {
        $this->listing_rating_table()->detach();
        if($value)
        {
            $this->listing_rating_table()->attach($this->id, [
                'reviews'       => $value['reviews'],
                'rating'        => $value['rating'],
                'accuracy'      => $value['accuracy'],
                'communication' => $value['communication'],
                'cleanliness'   => $value['cleanliness'],
                'location'      => $value['location'],
                'check_in'      => $value['check_in'],
                'value'         => $value['value'],
            ]);
        }
    }

    /* public function getRatingAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getRatingAttribute()
    {
        $field = new \stdClass;
        $field->title = 'rating';
        $field->reviews         = data_get($this, 'listing_rating_table.0.pivot.reviews', 0);
        $field->rating          = data_get($this, 'listing_rating_table.0.pivot.rating', 0);
        $field->accuracy        = data_get($this, 'listing_rating_table.0.pivot.accuracy', 0);
        $field->communication   = data_get($this, 'listing_rating_table.0.pivot.communication', 0);
        $field->cleanliness     = data_get($this, 'listing_rating_table.0.pivot.cleanliness', 0);
        $field->location        = data_get($this, 'listing_rating_table.0.pivot.location', 0);
        $field->check_in        = data_get($this, 'listing_rating_table.0.pivot.check_in', 0);
        $field->value           = data_get($this, 'listing_rating_table.0.pivot.value', 0);
        unset($this->listing_rating_table);

        return $field;
    }

    /* public function setRentOutAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setRentOutAttribute($value)
    {
        $this->listing_rent_out_table()->detach();
        if($value)
        {
            $this->listing_rent_out_table()->attach($this->id, [
                'rent_out' => $value,
            ]);
        }
    }

    /* public function getRentOutAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getRentOutAttribute()
    {
        $field = new \stdClass;
        $field->title = 'rent_out';
        $field->value = data_get($this, 'listing_rent_out_table.0.pivot.rent_out');
        $field->name = data_get($this, 'listing_rent_out_table.0.name');

        $ico[1] = 'hotels-ico hotels-uniF105';
        $ico[2] = 'hotels-ico hotels-uniF10D2';
        $ico[3] = 'hotels-ico hotels-uniF1232';

        $field->ico = ($field->value) ? $ico[$field->value] : null;

        unset($this->listing_rent_out_table);
        return $field;
    }

    /* public function setSchoolslocatonAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setSchoolslocatonAttribute($value)
    {
        $this->listing_schoolslocaton_table()->detach();
        if($value) {
            $i = 0;
            foreach ($value as $item) {
                if ($item) {
                    $this->listing_schoolslocaton_table()->attach($this->id, [
                        'name' => $item['name'],
                        'address' => $item['address'],
                        'locality' => $item['locality'],
                        'region' => $item['region'],
                        'lat' => $item['lat'],
                        'lng' => $item['lng'],
                        'sort' => $i,
                    ]);
                }
                $i++;
            }
        }
    }

    /* public function getSchoolslocatonAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getSchoolslocatonAttribute()
    {
        $field = new \stdClass;
        $field->title = 'schoolslocaton';
        $field->values = [];

        $data[0] = 'one';
        $data[1] = 'two';
        $data[2] = 'tree';

        if($this->listing_schoolslocaton_table->count())
        {
            foreach ($this->listing_schoolslocaton_table as $item) {
                $field->values[$data[$item->pivot->sort]] = [
                    'name'      => $item->pivot->name,
                    'address'   => $item->pivot->address,
                    'locality'  => $item->pivot->locality,
                    'region'    => $item->pivot->region,
                    'lat'       => (float) $item->pivot->lat,
                    'lng'       => (float) $item->pivot->lng,
                ];
            }
        }
        unset($this->listing_schoolslocaton_table);
        return $field;
    }

    /* public function setSmokingAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setSmokingAttribute($value)
    {
        $this->listing_smoking_table()->detach();
        if($value)
        {
            $this->listing_smoking_table()->attach($this->id, [
                'smoking' => $value,
            ]);
        }
    }

    /* public function getSmokingAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getSmokingAttribute()
    {
        $field = new \stdClass;
        $field->title = 'smoking';
        $field->value = data_get($this, 'listing_smoking_table.0.pivot.smoking');
        $field->name = ($field->value) ? config('host.smoking.'.$field->value) : null;
        unset($this->listing_smoking_table);
        return $field;
    }

    /* public function setSpacesAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setSpacesAttribute($value)
    {
        $this->listing_spaces_table()->detach();
        if($value)
        {
            $i = 0;
            foreach ($value as $item) {
                $this->listing_spaces_table()->attach($this->id, [
                    'spaces' => $item,
                    'sort' => $i,
                ]);
                $i++;
            }
        }
    }

    /* public function getSpacesAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getSpacesAttribute()
    {
        $field = new \stdClass;
        $field->title = 'spaces';
        $field->values = [];
        $field->arr = [];
        if($this->listing_spaces_table->count())
        {
            foreach ($this->listing_spaces_table as $item)
            {
                $field->values[] = [
                    'id'    => $item->id,
                    'name'  => $item->name,
                ];
                $field->arr[] = $item->id;
            }
        }
        unset($this->listing_spaces_table);
        return $field;
    }

    /* public function getUserAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getUserAttribute()
    {
        $field = new \stdClass;
        $field->id = $this->userListing->id;
        $field->first_name = $this->userListing->first_name;
        $field->slug = $this->userListing->slug;
        $field->reviews = 0;
        $field->verified = true;
        $field->rate = 100;
        $field->time = 'within an hour';

        unset($this->userListing);
        return $field;
    }
}
