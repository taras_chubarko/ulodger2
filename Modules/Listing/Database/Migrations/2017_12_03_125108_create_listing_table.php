<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->decimal('progress', 10,2)->nullable();
            $table->integer('step')->unsigned()->nullable();
            $table->integer('status')->nullable();
            $table->string('slug');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('listing_rent_out', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->integer('rent_out')->nullable();
        });

        Schema::create('listing_property_type', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->integer('property_type')->nullable();
        });

        Schema::create('listing_description', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->text('description')->nullable();
        });

        Schema::create('listing_smoking', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->integer('smoking')->nullable();
        });

        Schema::create('listing_guests', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->integer('guests')->nullable();
        });

        Schema::create('listing_bedrooms', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->integer('bedrooms')->nullable();
        });

        Schema::create('listing_beds', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->integer('beds')->nullable();
        });

        Schema::create('listing_bath_type', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->integer('bath_type')->nullable();
        });

        Schema::create('listing_bathrooms', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->decimal('bathrooms', 10, 2)->nullable();
        });

        Schema::create('listing_location', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->string('address')->nullable();
            $table->string('locality')->nullable();
            $table->string('region')->nullable();
            $table->decimal('lat', 10,6)->nullable();
            $table->decimal('lng', 10,6)->nullable();
        });

        Schema::create('listing_schoolslocaton', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('locality')->nullable();
            $table->string('region')->nullable();
            $table->decimal('lat', 10,6)->nullable();
            $table->decimal('lng', 10,6)->nullable();
            $table->integer('sort')->nullable();
        });

        Schema::create('listing_amenities', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->integer('amenities')->nullable();
            $table->integer('sort')->nullable();
        });

        Schema::create('listing_spaces', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->integer('spaces')->nullable();
            $table->integer('sort')->nullable();
        });

        Schema::create('listing_photo', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->integer('fid')->nullable();
            $table->integer('sort')->nullable();
        });

        Schema::create('listing_available', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->date('from')->nullable();
            $table->date('to')->nullable();
        });

        Schema::create('listing_price', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->decimal('price', 10,2)->nullable();
        });

        Schema::create('listing_rating', function (Blueprint $table) {
            $table->integer('listing_id')->unsigned()->nullable();
            $table->foreign('listing_id')->references('id')->on('listing')->onDelete('cascade');
            $table->integer('reviews')->nullable();
            $table->integer('rating')->nullable();
            $table->integer('accuracy')->nullable();
            $table->integer('communication')->nullable();
            $table->integer('cleanliness')->nullable();
            $table->integer('location')->nullable();
            $table->integer('check_in')->nullable();
            $table->integer('value')->nullable();
        });





    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing');
    }
}
