<?php

namespace Modules\Listing\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;
use Modules\Listing\Entities\Listing;

class ListingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('en_US');

        foreach(range(1, 200) as $i)
        {
            $lat = $faker->latitude($min = 45.41194838064268, $max = 44.61784415342067);
            $lng = $faker->longitude($min = -93.76556396484375, $max = -92.548828125);
            $adderss = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lng.'&language=en&result_type=street_address&key=AIzaSyBBziS3HCZ6We5DhI3VdlGfQ4mHF_QvIcs');
            $adderss = json_decode($adderss);

            if($adderss->results)
            {
                $add = new Listing();
                $add->user_id = $faker->numberBetween(1, 10);
                $add->name = $faker->catchPhrase;
                $add->status = 1;
                $add->progress = 100;
                $add->step = 7;
                $add->save();

                $add->rent_out = $faker->numberBetween(1, 3);
                $add->property_type = $faker->numberBetween(4, 8);
                $add->description = $faker->text($maxNbChars = 500);
                $add->smoking = $faker->numberBetween(1, 3);
                $add->guests = $faker->numberBetween(1, 10);
                $add->bedrooms = $faker->numberBetween(0, 10);
                $add->beds = $faker->numberBetween(1, 10);
                $add->bath_type = $faker->numberBetween(1, 2);
                $add->bathrooms = $faker->numberBetween(1, 11);

                $addr = $adderss->results[0];
                $add->location = [
                    'address' => $addr->formatted_address,
                    'locality' => $this->get_addr($addr->address_components, 'locality'),
                    'country' => $this->get_addr($addr->address_components, 'country'),
                    'region' => $this->get_addr($addr->address_components, 'administrative_area_level_1'),
                    'lat' => $addr->geometry->location->lat,
                    'lng' => $addr->geometry->location->lng,
                ];

                $lat = $faker->latitude($min = 45.41194838064268, $max = 44.61784415342067);
                $lng = $faker->longitude($min = -93.76556396484375, $max = -92.548828125);
                $establishment = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lng.'&language=en&result_type=street_address&&key=AIzaSyBBziS3HCZ6We5DhI3VdlGfQ4mHF_QvIcs');
                $establishment = json_decode($establishment);

                if($establishment->results)
                {
                    $est = $establishment->results[0];
                    $add->schoolslocaton = [
                        'one' => [
                            'name' => $this->get_addr($est->address_components, 'locality'),
                            'address' => $est->formatted_address,
                            'locality' => $this->get_addr($est->address_components, 'locality'),
                            'country' => $this->get_addr($est->address_components, 'country'),
                            'region' => $this->get_addr($est->address_components, 'administrative_area_level_1'),
                            'lat' => $est->geometry->location->lat,
                            'lng' => $est->geometry->location->lng,
                        ]
                    ];
                }

                $add->amenities = [$faker->numberBetween(19, 24)];
                $add->spaces = [$faker->numberBetween(31, 38)];

                $in = $faker->numberBetween(1, 1000);

                if($this->checkNum($in))
                {
                    $arrIm = [];
                    foreach(range(1, 4) as $y)
                    {
                        $imagesPathID = $faker->numberBetween(1, 20);
                        $imagesPath = 'http://css.progim.net/img/kv/'.$imagesPathID.'.jpg';
                        $images = \Image::make($imagesPath);
                        $imagesName = str_random(10);
                        $images->save('public/uploads/photo/'.$imagesName.'.jpg');

                        $imagesId = \Filem::create([
                            'user_id'   => 0,
                            'original'  => $imagesPathID.'.jpg',
                            'filename'  => $imagesName.'.jpg',
                            'uri'       => 'photo',
                            'ext'       => 'jpg',
                            'filemime'  => 'image/jpeg',
                            'file_id'   => 1,
                            'filesize'  => 1,
                            'status'    => 1,
                        ]);
                        $arrIm[] = [
                            'id' => $imagesId->id,
                        ];
                    }
                    $add->photo = $arrIm;
                }
                else
                {
                    $add->photo = null;
                }


                $add->available = [
                    'from' => \Carbon::now()->format('d.m.Y'),
                    'to' => \Carbon::now()->addMonths($faker->numberBetween(1, 6))->format('d.m.Y'),
                ];

                $add->price = $faker->numberBetween(50, 1000);
            }
        }
    }
    //
    public function checkNum($num){
        return ($num%2) ? TRUE : FALSE;
    }
    //
    public function get_addr($arr, $fileld)
    {
        $data = null;
        foreach ($arr as $item) {
            if($item->types[0] == $fileld)
            {
                $data = $item->long_name;
            }
        }
        return $data;
    }
}
