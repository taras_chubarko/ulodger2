<?php

namespace Modules\Listing\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Listing extends Model implements Transformable
{
    use SoftDeletes;
    use TransformableTrait;
    use Sluggable;

    protected $table = 'host';

    protected $fillable = [];

    protected $guarded = ['_token', 'meta'];

    protected $dates = ['deleted_at'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slughost'
            ]
        ];
    }
    /* public function getSlugjobAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getSlughostAttribute()
    {
        $gen = 9 - strlen($this->id);
        $numer = str_pad(mt_rand(0, 999999), $gen, '0', STR_PAD_LEFT).$this->id;
        return 'id-'.$numer;
    }
    /* public function host_amenities_table
* @param
*-----------------------------------
*|
*-----------------------------------
*/
    public function host_amenities_table()
    {
        return $this->belongsToMany('Modules\Taxonomy\Entities\TaxonomyTerm', 'host_amenities', 'host_id', 'amenities')->withPivot('amenities', 'sort');
    }
    /* public function host_available_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_available_table()
    {
        return $this->belongsToMany('Modules\Listing\Entities\Listing', 'host_available', 'host_id', 'from', 'to')->withPivot('from', 'to');
    }
    /* public function host_bath_type_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_bath_type_table()
    {
        return $this->belongsToMany('Modules\Listing\Entities\Listing', 'host_bath_type', 'host_id', 'bath_type')->withPivot('bath_type');
    }
    /* public function host_bathrooms_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_bathrooms_table()
    {
        return $this->belongsToMany('Modules\Listing\Entities\Listing', 'host_bathrooms', 'host_id', 'bathrooms')->withPivot('bathrooms');
    }
    /* public function host_bedrooms_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_bedrooms_table()
    {
        return $this->belongsToMany('Modules\Listing\Entities\Listing', 'host_bedrooms', 'host_id', 'bedrooms')->withPivot('bedrooms');
    }
    /* public function host_beds_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_beds_table()
    {
        return $this->belongsToMany('Modules\Listing\Entities\Listing', 'host_beds', 'host_id', 'beds')->withPivot('beds');
    }
    /* public function host_description_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_description_table()
    {
        return $this->belongsToMany('Modules\Listing\Entities\Listing', 'host_description', 'host_id', 'description')->withPivot('description');
    }
    /* public function host_guests_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_guests_table()
    {
        return $this->belongsToMany('Modules\Listing\Entities\Listing', 'host_guests', 'host_id', 'guests')->withPivot('guests');
    }
    /* public function host_location_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_location_table()
    {
        return $this->belongsToMany('Modules\Listing\Entities\Listing', 'host_location', 'host_id', 'address', 'locality', 'region', 'lat', 'lng')->withPivot('address', 'locality', 'region', 'lat', 'lng');
    }
    /* public function host_photo_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_photo_table()
    {
        return $this->belongsToMany('Modules\Filem\Entities\Filem', 'host_photo', 'host_id', 'fid')->withPivot('fid', 'nameid', 'sort');
    }
    /* public function host_price_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_price_table()
    {
        return $this->belongsToMany('Modules\Listing\Entities\Listing', 'host_price', 'host_id', 'price')->withPivot('price');
    }
    /* public function host_property_type_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_property_type_table()
    {
        return $this->belongsToMany('Modules\Taxonomy\Entities\TaxonomyTerm', 'host_property_type', 'host_id', 'property_type')->withPivot('property_type');
    }
    /* public function host_rating_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_rating_table()
    {
        return $this->belongsToMany('Modules\Listing\Entities\Listing', 'host_rating', 'host_id')->withPivot('reviews', 'rating', 'accuracy', 'communication', 'cleanliness', 'location', 'check_in', 'value');
    }
    /* public function host_rent_out_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_rent_out_table()
    {
        return $this->belongsToMany('Modules\Taxonomy\Entities\TaxonomyTerm', 'host_rent_out', 'host_id', 'rent_out')->withPivot('rent_out');
    }
    /* public function host_schoolslocaton_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_schoolslocaton_table()
    {
        return $this->belongsToMany('Modules\Listing\Entities\Listing', 'host_schoolslocaton', 'host_id', 'name', 'address', 'locality', 'region', 'lat', 'lng', 'sort')->withPivot('name', 'address', 'locality', 'region', 'lat', 'lng', 'sort');
    }
    /* public function host_smoking_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_smoking_table()
    {
        return $this->belongsToMany('Modules\Listing\Entities\Listing', 'host_smoking', 'host_id', 'smoking')->withPivot('smoking');
    }
    /* public function host_spaces_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function host_spaces_table()
    {
        return $this->belongsToMany('Modules\Taxonomy\Entities\TaxonomyTerm', 'host_spaces', 'host_id', 'spaces', 'sort')->withPivot('spaces', 'sort');
    }
    /* public function setAmenitiesAttribute
* @param
*-----------------------------------
*|
*-----------------------------------
*/
    public function setAmenitiesAttribute($value)
    {
        $this->host_amenities_table()->detach();
        if($value && is_array($value))
        {
            $i = 0;
            foreach ($value as $item) {
                $this->host_amenities_table()->attach($this->id, [
                    'amenities' => $item,
                    'sort' => $i,
                ]);
                $i++;
            }

        }
    }

    /* public function getAmenitiesAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getAmenitiesAttribute()
    {
        $field = new \stdClass;
        $field->title = 'amenities';
        $field->value = data_get($this, 'host_amenities_table');
        unset($this->host_amenities_table);
        return $field;
    }

    /* public function setAvailableAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setAvailableAttribute($value)
    {
        $this->host_available_table()->detach();
        if($value)
        {
            $this->host_available_table()->attach($this->id, [
                'from' => \Carbon::parse($value['from']),
                'to' => \Carbon::parse($value['to']),
            ]);
        }
    }

    /* public function getAvailableAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getAvailableAttribute()
    {
        $field = new \stdClass;
        $field->title = 'available';
        $field->value = data_get($this, 'host_available_table.0.pivot.available');
        unset($this->host_available_table);
        return $field;
    }

    /* public function setBathTypeAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setBathTypeAttribute($value)
    {
        $this->host_bath_type_table()->detach();
        if($value)
        {
            $this->host_bath_type_table()->attach($this->id, [
                'bath_type' => $value,
            ]);
        }
    }

    /* public function getBathTypeAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getBathTypeAttribute()
    {
        $field = new \stdClass;
        $field->title = 'bath_type';
        $field->value = data_get($this, 'host_bath_type_table.0.pivot.bath_type');
        unset($this->host_bath_type_table);
        return $field;
    }

    /* public function setBathroomsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setBathroomsAttribute($value)
    {
        $this->host_bathrooms_table()->detach();
        if($value)
        {
            $this->host_bathrooms_table()->attach($this->id, [
                'bathrooms' => $value,
            ]);
        }
    }

    /* public function getBathroomsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getBathroomsAttribute()
    {
        $field = new \stdClass;
        $field->title = 'bathrooms';
        $field->value = data_get($this, 'host_bathrooms_table.0.pivot.bathrooms');
        unset($this->host_bathrooms_table);
        return $field;
    }

    /* public function setBedroomsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setBedroomsAttribute($value)
    {
        $this->host_bedrooms_table()->detach();
        if($value)
        {
            $this->host_bedrooms_table()->attach($this->id, [
                'bedrooms' => $value,
            ]);
        }
    }

    /* public function getBedroomsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getBedroomsAttribute()
    {
        $field = new \stdClass;
        $field->title = 'bedrooms';
        $field->value = data_get($this, 'host_bedrooms_table.0.pivot.bedrooms');
        unset($this->host_bedrooms_table);
        return $field;
    }

    /* public function setBedsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setBedsAttribute($value)
    {
        $this->host_beds_table()->detach();
        if($value)
        {
            $this->host_beds_table()->attach($this->id, [
                'beds' => $value,
            ]);
        }
    }

    /* public function getBedsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getBedsAttribute()
    {
        $field = new \stdClass;
        $field->title = 'beds';
        $field->value = data_get($this, 'host_beds_table.0.pivot.beds');
        unset($this->host_beds_table);
        return $field;
    }

    /* public function setDescriptionAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setDescriptionAttribute($value)
    {
        $this->host_description_table()->detach();
        if($value)
        {
            $this->host_description_table()->attach($this->id, [
                'description' => $value,
            ]);
        }
    }

    /* public function getDescriptionAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getDescriptionAttribute()
    {
        $field = new \stdClass;
        $field->title = 'description';
        $field->value = data_get($this, 'host_description_table.0.pivot.description');
        unset($this->host_description_table);
        return $field;
    }

    /* public function setGuestsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setGuestsAttribute($value)
    {
        $this->host_guests_table()->detach();
        if($value)
        {
            $this->host_guests_table()->attach($this->id, [
                'guests' => $value,
            ]);
        }
    }

    /* public function getGuestsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getGuestsAttribute()
    {
        $field = new \stdClass;
        $field->title = 'guests';
        $field->value = data_get($this, 'host_guests_table.0.pivot.guests');
        unset($this->host_guests_table);
        return $field;
    }

    /* public function setLocationAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setLocationAttribute($value)
    {
        $this->host_location_table()->detach();
        if($value)
        {
            $this->host_location_table()->attach($this->id, [
                'address' => $value['address'],
                'locality' => $value['city'],
                'region' => $value['region'],
                'lat' => $value['lat'],
                'lng' => $value['lng'],
            ]);
        }
    }

    /* public function getLocationAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getLocationAttribute()
    {
        $field = new \stdClass;
        $field->title = 'location';
        $field->address = data_get($this, 'host_location_table.0.pivot.address');
        $field->region = data_get($this, 'host_location_table.0.pivot.region');
        $field->city = data_get($this, 'host_location_table.0.pivot.locality');
        $field->lat = data_get($this, 'host_location_table.0.pivot.lat');
        $field->lng = data_get($this, 'host_location_table.0.pivot.lng');
        unset($this->host_location_table);
        return $field;
    }

    /* public function setPhotoAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setPhotoAttribute($value)
    {
        $this->host_photo_table()->detach();
        if($value)
        {
            $i = 0;
            foreach ($value as $k => $item) {
                if($item)
                {
                    $this->host_photo_table()->attach($this->id, [
                        'fid' => $item['id'],
                        'nameid' => $item['file_id'],
                        'sort' => $i,
                    ]);
                }
                $i++;
            }
        }
    }

    /* public function getPhotoAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getPhotoAttribute()
    {
        $field = new \stdClass;
        $field->title = 'photo';
        $field->value = data_get($this, 'host_photo_table.0.pivot.photo');
        unset($this->host_photo_table);
        return $field;
    }

    /* public function setPriceAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setPriceAttribute($value)
    {
        $this->host_price_table()->detach();
        if($value)
        {
            $this->host_price_table()->attach($this->id, [
                'price' => $value,
            ]);
        }
    }

    /* public function getPriceAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getPriceAttribute()
    {
        $field = new \stdClass;
        $field->title = 'price';
        $field->value = data_get($this, 'host_price_table.0.pivot.price');
        unset($this->host_price_table);
        return $field;
    }

    /* public function setPropertyTypeAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setPropertyTypeAttribute($value)
    {
        $this->host_property_type_table()->detach();
        if($value)
        {
            $this->host_property_type_table()->attach($this->id, [
                'property_type' => $value,
            ]);
        }
    }

    /* public function getPropertyTypeAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getPropertyTypeAttribute()
    {
        $field = new \stdClass;
        $field->title = 'property_type';
        $field->value = data_get($this, 'host_property_type_table.0.pivot.property_type');
        unset($this->host_property_type_table);
        return $field;
    }

    /* public function setRatingAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setRatingAttribute($value)
    {
        $this->host_rating_table()->detach();
        if($value)
        {
            $this->host_rating_table()->attach($this->id, [
                'rating' => $value,
            ]);
        }
    }

    /* public function getRatingAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getRatingAttribute()
    {
        $field = new \stdClass;
        $field->title = 'rating';
        $field->value = data_get($this, 'host_rating_table.0.pivot.rating');
        unset($this->host_rating_table);
        return $field;
    }

    /* public function setRentOutAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setRentOutAttribute($value)
    {
        $this->host_rent_out_table()->detach();
        if($value)
        {
            $this->host_rent_out_table()->attach($this->id, [
                'rent_out' => $value,
            ]);
        }
    }

    /* public function getRentOutAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getRentOutAttribute()
    {
        $field = new \stdClass;
        $field->title = 'rent_out';
        $field->value = data_get($this, 'host_rent_out_table.0.pivot.rent_out');
        unset($this->host_rent_out_table);
        return $field;
    }

    /* public function setSchoolslocatonAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setSchoolslocatonAttribute($value)
    {
        $this->host_schoolslocaton_table()->detach();
        if($value)
        {
            $i = 0;
            foreach ($value as $item) {
                if($item)
                {
                    $this->host_schoolslocaton_table()->attach($this->id, [
                        'name' => $item['name'],
                        'address' => $item['address'],
                        'locality' => $item['city'],
                        'region' => $item['region'],
                        'lat' => $item['lat'],
                        'lng' => $item['lng'],
                        'sort' => $i,
                    ]);
                }
                $i++;
            }
        }
    }

    /* public function getSchoolslocatonAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getSchoolslocatonAttribute()
    {
        $field = new \stdClass;
        $field->title = 'schoolslocaton';
        $field->value = data_get($this, 'host_schoolslocaton_table.0.pivot.schoolslocaton');
        unset($this->host_schoolslocaton_table);
        return $field;
    }

    /* public function setSmokingAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setSmokingAttribute($value)
    {
        $this->host_smoking_table()->detach();
        if($value)
        {
            $this->host_smoking_table()->attach($this->id, [
                'smoking' => $value,
            ]);
        }
    }

    /* public function getSmokingAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getSmokingAttribute()
    {
        $field = new \stdClass;
        $field->title = 'smoking';
        $field->value = data_get($this, 'host_smoking_table.0.pivot.smoking');
        unset($this->host_smoking_table);
        return $field;
    }

    /* public function setSpacesAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setSpacesAttribute($value)
    {
        $this->host_spaces_table()->detach();
        if($value)
        {
            $i = 0;
            foreach ($value as $item) {
                $this->host_spaces_table()->attach($this->id, [
                    'spaces' => $item,
                    'sort' => $i,
                ]);
                $i++;
            }
        }
    }

    /* public function getSpacesAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getSpacesAttribute()
    {
        $field = new \stdClass;
        $field->title = 'spaces';
        $field->value = data_get($this, 'host_spaces_table.0.pivot.spaces');
        unset($this->host_spaces_table);
        return $field;
    }





}
