<?php namespace Modules\Listing\Facades;

use Illuminate\Support\Facades\Facade;

class Listing extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Modules\Listing\Repositories\ListingRepository';
    }
}