<?php

namespace Modules\Invoice\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $table = 'invoice';

    protected $fillable = [];

    protected $guarded = ['_token'];

    protected $dates = ['deleted_at'];


    /* public function toUser
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function toUser()
    {
        return $this->hasOne(\Modules\User\Entities\User::class, 'id', 'to_user_id');
    }

    /* public function book
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function book()
    {
        return $this->hasOne(\Modules\Listing\Entities\BookListing::class, 'id', 'book_id');
    }

    /* public function licting
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listing()
    {
        return $this->hasOne(\Modules\Listing\Entities\Listing::class, 'id', 'listing_id');
    }
    /* public function set
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setPaymentForMonthAttribute($value)
    {
        if($value)
        {
            $this->attributes['payment_for_month'] = \Carbon::parse($value);
        }
    }
    /* public function setOrderIdAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setOrderIdAttribute($value)
    {
        $this->attributes['order_id'] = $value.time().$this->id;
    }

    /* public function invoice_items_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function invoice_items_table()
    {
        return $this->belongsToMany(\Modules\Invoice\Entities\Invoice::class, 'invoice_items')->withPivot('item', 'description', 'quantity', 'unit_cost', 'sort');
    }
    /* public function setItemsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setItemsAttribute($value)
    {
        $this->invoice_items_table()->detach();
        if($value && is_array($value))
        {
            $i = 0;
            foreach ($value as $item)
            {
                $this->invoice_items_table()->attach($this->id, [
                    'item'          => $item['item'],
                    'description'   => $item['description'],
                    'quantity'      => $item['quantity'],
                    'unit_cost'     => $item['unit_cost'],
                    'sort'          => $i,
                ]);
                $i++;
            }

        }
    }

    /* public function getItemsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getItemsAttribute()
    {
        $field = new \stdClass;
        $items = data_get($this, 'invoice_items_table');
        $field->values = [];
        if($items)
        {
            foreach ($items as $item)
            {
                $field->values[$item->pivot->sort] = collect([
                    'item'          => $item->pivot->item,
                    'description'   => $item->pivot->description,
                    'quantity'      => $item->pivot->quantity,
                    'unit_cost'     => $item->pivot->unit_cost,
                    'total'         => $item->pivot->quantity * $item->pivot->unit_cost,
                ]);
            }
        }
        unset($this->invoice_items_table);
        return $field;
    }

    /* public function getTotalAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getTotalAttribute()
    {
        $total = $this->subtotal + $this->commission;
        return $total;
    }

    /* public function getCalcTotalAttribute
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getCalcTotalAttribute()
    {
        $sum = [];
        if(count($this->items->values) > 0)
        {
            foreach ($this->items->values as $item)
            {
                $sum[] = $item['quantity'] * $item['unit_cost'];
            }
        }
        $total = array_sum($sum);
        return $total;
    }

    /* public function calc
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function calc()
    {
        $commission = 0;
        //
        $onOff = config('invoice.commission');
        $commission_persent = config('invoice.commission_persent');
        $commission_type = config('invoice.commission_type');
        //
        if($onOff)
        {
            switch ($commission_type)
            {
                case 'per_month':
                    $commission = $this->calcTotal * $commission_persent;
                    break;
                case 'per_period':

                    $date1 = new \DateTime($this->book->check_in);
                    $date2 = new \DateTime($this->book->check_out);
                    $interval = date_diff($date1, $date2);
                    $qtyMonth = $interval->m + ($interval->y * 12);
                    $commission = $this->calcTotal * $qtyMonth * $commission_persent;
                    break;
            }
        }

        self::where('id', $this->id)->update([
            'subtotal' => $this->calcTotal,
            'commission' => $commission,
        ]);
    }

}
