<?php

namespace Modules\Invoice\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Invoice\Entities\Invoice;
use Vsmoraes\Pdf\Pdf;

class BillingController extends Controller
{
    protected $invoice;
    private $pdf;
    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(Invoice $invoice, Pdf $pdf)
    {
        $this->invoice = $invoice;
        $this->pdf = $pdf;
    }
    /* public function invoice_pdf
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function invoice_pdf($id)
    {
        $invoice = $this->invoice->find($id);

        $item = (object)[
            'id'            => $invoice->id,
            'order_id'      => $invoice->order_id,
            'month'         => \Carbon::parse($invoice->payment_for_month)->format('m/Y'),
            'subtotal'      => $invoice->subtotal,
            'commission'    => $invoice->commission,
            'status'        => [
                'id'    => $invoice->status,
                'name'  => config('invoice.status.'.$invoice->status),
            ],
            'created_at'    => $invoice->created_at->format('m/d/Y H:i'),
            'items'         => $invoice->items->values,
            'address'       => ($invoice->status == 1) ? $invoice->listing->location->address : null,
            'total'         => $invoice->total,
        ];

        $html = view('site::invoice.invoice', compact('item'))->render();

        $defaultOptions = $this->pdf->getOptions();
        $defaultOptions->setDefaultFont('Courier');

        return $this->pdf->setOptions($defaultOptions)->load($html)->filename($invoice->order_id.'.pdf')->show();
    }
}
