<?php

namespace Modules\Invoice\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Invoice\Entities\Invoice;
use Modules\Listing\Entities\BookListing;

class InvoiceController extends Controller
{
    protected $model;
    protected $bookListing;
    /* public function __constuct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(Invoice $model, BookListing $bookListing)
    {
        $this->model = $model;
        $this->bookListing = $bookListing;
    }
    /* public function bills_me
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function bills_me()
    {
        $user = \Sentinel::getUser();

        $items = $this->model->where('to_user_id', $user->id)->orderBy('created_at', 'DESC')->paginate();

        $items->getCollection()->transform(function ($value) {
            $data = [
                'id'        => $value->id,
                'order_id'  => $value->order_id,
                'month'     => \Carbon::parse($value->payment_for_month)->format('m/Y'),
                'user'      => [
                    'id'    => $value->toUser->id,
                    'name'  => $value->toUser->fullName,
                ],
                'status'    => [
                    'id'    => $value->status,
                    'name'  => config('invoice.status.'.$value->status),
                ],
                'total'     => $value->total,
                'created_at' => $value->created_at->format('m/d/Y H:i'),
            ];
            return $data;
        });

        return response()->json($items, 200);
    }

    /* public function bills_my
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function bills_my()
    {
        $user = \Sentinel::getUser();

        $items = $this->model->where('from_user_id', $user->id)->orderBy('created_at', 'DESC')->paginate();

        $items->getCollection()->transform(function ($value) {
            $data = [
                'id'        => $value->id,
                'order_id'  => $value->order_id,
                'month'     => \Carbon::parse($value->payment_for_month)->format('m/Y'),
                'user'      => [
                    'id'    => $value->toUser->id,
                    'name'  => $value->toUser->fullName,
                ],
                'status'    => [
                    'id'    => $value->status,
                    'name'  => config('invoice.status.'.$value->status),
                ],
                'total'     => $value->total,
                'created_at' => $value->created_at->format('m/d/Y H:i'),
            ];
            return $data;
        });

        return response()->json($items, 200);
    }

    /* public function update_status
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function update_success($id)
    {
        $this->model->where('id', $id)->update(['status' => 1]);
        $invoice = $this->model->find($id);
        $book = $this->bookListing->find($invoice->book_id);
        $book->status = 1;
        $book->save();

        return response()->json('success', 200);
    }

    /* public function invoice
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function invoice($order_id)
    {
        $invoice = $this->model->where('order_id', $order_id)->first();
        //
        $item = [
            'id'            => $invoice->id,
            'order_id'      => $invoice->order_id,
            'month'         => \Carbon::parse($invoice->payment_for_month)->format('m/Y'),
            'subtotal'      => $invoice->subtotal,
            'commission'    => $invoice->commission,
            'status'        => [
                'id'    => $invoice->status,
                'name'  => config('invoice.status.'.$invoice->status),
            ],
            'created_at'    => $invoice->created_at->format('m/d/Y H:i'),
            'items'         => $invoice->items->values,
            'address'       => ($invoice->status == 1) ? $invoice->listing->location->address : null,
            'total'         => $invoice->total,
        ];
        //
        return response()->json($item, 200);
    }

    /* public function pay
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function pay(Request $request)
    {
        $account = [
            'account_id'    => '1950646076',
            'short_description' => $request->order_id,
            'type'          => 'service',
            'amount'        => $request->total,
            'currency'      => 'USD',
            //'unique_id'     => $request->id,
            'callback_uri'  => 'https://ulodger2.progim.net/api/v1/invoice/pay/1',
        ];

        $checkout = \WePayLaravel::checkout()->create($account);

        $link = $checkout->hosted_checkout;

        return response()->json($link, 200);
    }

    /* public function pay1
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function pay1(Request $request)
    {
        \Log::debug($request->all());
    }
}
