<?php

Route::group(['middleware' => 'web', 'prefix' => 'api/v1/invoice', 'namespace' => 'Modules\Invoice\Http\Controllers'], function()
{
    Route::get('/get/bills/me', 'InvoiceController@bills_me');
    Route::get('/get/bills/my', 'InvoiceController@bills_my');
    Route::put('/{id}/success', 'InvoiceController@update_success');
    Route::get('/{order_id}', 'InvoiceController@invoice');
    Route::post('/pay', 'InvoiceController@pay');
    Route::get('/pay/1', 'InvoiceController@pay1');
});

Route::group(['middleware' => 'web', 'prefix' => 'billing', 'namespace' => 'Modules\Invoice\Http\Controllers'], function()
{
    Route::get('/invoice/{id}/pdf', [
        'as' 	=> 'invoice.pdf',
        'uses' 	=> 'BillingController@invoice_pdf'
    ]);
});
