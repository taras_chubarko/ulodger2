<?php

namespace Modules\Invoice\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InvoiceRepository
 * @package namespace App\Repositories;
 */
interface InvoiceRepository extends RepositoryInterface
{
    public function add($book, $listing);
}
