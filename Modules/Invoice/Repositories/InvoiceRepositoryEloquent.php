<?php

namespace Modules\Invoice\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Invoice\Repositories\InvoiceRepository;
use Modules\Invoice\Entities\Invoice;

/**
 * Class InvoiceRepositoryEloquent
 * @package namespace App\Repositories;
 */
class InvoiceRepositoryEloquent extends BaseRepository implements InvoiceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Invoice::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /* public function add($book, $listing)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function add($book, $listing)
    {
        $onOff = config('invoice.commission');
        $commission_persent = config('invoice.commission_persent');
        $commission_type = config('invoice.commission_type');

        $commission = 0;

        if($onOff)
        {
            switch ($commission_type)
            {
                case 'per_month':
                    $commission = $listing->price->value * $commission_persent;
                    break;
                case 'per_period':
                    $firstInvoice = $this->model->where('book_id', $book->id)->orderBy('created_at', 'ASC')->first();

                    if(!$firstInvoice)
                    {
                        $date1 = new \DateTime($book->check_in);
                        $date2 = new \DateTime($book->check_out);
                        $interval = date_diff($date1, $date2);
                        $qtyMonth = $interval->m + ($interval->y * 12);
                        $commission = $listing->price->value * $qtyMonth * $commission_persent;
                    }
                    break;
            }
        }



        $invoice = $this->model->create([
            'order_id'          => 'UL-',
            'payment_for_month' => $book->check_in,
            'from_user_id'      => $book->listing_user_id,
            'to_user_id'        => $book->user_id,
            'listing_id'        => $listing->id,
            'book_id'           => $book->id,
            'subtotal'          => $listing->price->value,
            'commission'        => $commission,
            'commission_type'   => $commission_type,
            'commission_persent'=> $commission_persent,
            'status'            => 0,
        ]);

        $items[] = [
            'item'          => $listing->name,
            'description'   => 'Rent for the period '. $book->check_in .'-'. $book->check_out. ' with a monthly payment',
            'quantity'      => 1,
            'unit_cost'     => $listing->price->value,
        ];

        $invoice->items = $items;
        //$invoice->calc();

        return $invoice;
    }
}
