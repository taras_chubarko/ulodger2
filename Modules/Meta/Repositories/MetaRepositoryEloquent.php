<?php

namespace Modules\Meta\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Meta\Repositories\MetaRepository;
use Modules\Meta\Entities\Meta;

/**
 * Class MetaRepositoryEloquent
 * @package namespace App\Repositories;
 */
class MetaRepositoryEloquent extends BaseRepository implements MetaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Meta::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /* public function make($entity, $entity_id, $request)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function make($entity, $entity_id, $request)
    {
        $meta = \Meta::create([
            'entity'        => $entity,
            'entity_id'     => $entity_id,
            'title'         => $request->meta['title'],
            'keywords'      => $request->meta['keywords'],
            'robots'        => $request->meta['robots'],
            'description'   => $request->meta['description'],
        ]);
        return $meta;
    }
    /* public function change($entity, $entity_id)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function change($entity, $entity_id, $request)
    {
        $meta = $this->model->where('entity', $entity)->where('entity_id', $entity_id)->first();
        if($meta)
        {
            $meta->title        = $request->meta['title'];
            $meta->keywords     = $request->meta['keywords'];
            $meta->robots       = $request->meta['robots'];
            $meta->description  = $request->meta['description'];
            $meta->save();
            return $meta;
        }
        else
        {
            $meta = \Meta::create([
                'entity'        => $entity,
                'entity_id'     => $entity_id,
                'title'         => $request->meta['title'],
                'keywords'      => $request->meta['keywords'],
                'robots'        => $request->meta['robots'],
                'description'   => $request->meta['description'],
            ]);
            return $meta;
        }
    }
    /* public function destroy($entity, $entity_id)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function destroy($entity, $entity_id)
    {
        $meta = $this->model->where('entity', $entity)->where('entity_id', $entity_id)->first();
        $meta->delete();
        return $meta;
    }
    /* public function makeNew($request)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function makeTag($request)
    {
        $meta = \Meta::create([
            'entity'        => $request->entity,
            'entity_id'     => $request->entity_id,
            'title'         => $request->title,
            'keywords'      => $request->keywords,
            'robots'        => $request->robots,
            'description'   => $request->description,
        ]);
        return $meta;
    }
    /* public function changeTag($request, $id)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function changeTag($request, $id)
    {
        $meta = $this->model->find($id);
        $meta->entity       = $request->entity;
        $meta->entity_id    = $request->entity_id;
        $meta->title        = $request->title;
        $meta->keywords     = $request->keywords;
        $meta->robots       = $request->robots;
        $meta->description  = $request->description;
        $meta->save();
        return $meta;
    }
    /* public function setTags($entity, $entity_id)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setTags($entity, $entity_id, $vars = null)
    {
        $meta = $this->model->where('entity', $entity)->where('entity_id', $entity_id)->first();
        if($meta)
        {
            \MetaTag::set('title',  $meta->title);
            \MetaTag::set('robots', $meta->robots);
            \MetaTag::set('description', $meta->description);
            \MetaTag::set('keywords', $meta->keywords);
        }
        else
        {
            if($vars)
            {
                \MetaTag::set('title',          !empty($vars['title']) ? $vars['title'] : '');
                \MetaTag::set('robots',         !empty($vars['robots']) ? $vars['robots'] : '');
                \MetaTag::set('description',    !empty($vars['description']) ? $vars['description'] : '');
                \MetaTag::set('keywords',       !empty($vars['keywords']) ? $vars['keywords'] : '');
            }
            else
            {
                \MetaTag::set('title', \Setting::get('site.name'));
            }
        }
    }
    
    
    
    
    
    
    
}
