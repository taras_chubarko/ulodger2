<?php

namespace Modules\Meta\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MetaRepository
 * @package namespace App\Repositories;
 */
interface MetaRepository extends RepositoryInterface
{
    /*
     *
     */
    public function make($entity, $entity_id, $request);
    /*
     *
     */
    public function change($entity, $entity_id, $request);
    /*
     *
     */
    public function destroy($entity, $entity_id);
    /*
     *
     */
    public function makeTag($request);
    /*
     *
     */
    public function changeTag($request, $id);
    /*
     *
     */
    public function setTags($entity, $entity_id, $vars);
}
