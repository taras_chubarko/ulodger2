<?php

namespace Modules\Meta\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Meta\Repositories\MetaRepository;

class MetaController extends Controller
{
        /*
         *
         */
        protected $meta;
        /* public function __construct
         * @param $id
         *-----------------------------------
         *|
         *-----------------------------------
         */
        public function __construct(MetaRepository $meta)
        {
                $this->meta = $meta;
        }
        /* public function index
         * @param $id
         *-----------------------------------
         *|
         *-----------------------------------
         */
        public function index(Request $request)
        {
                $meta = $this->meta->paginate();
                return view('admin::meta.index', compact('meta'));
        }
        /* public function create
         * @param $id
         *-----------------------------------
         *|
         *-----------------------------------
         */
        public function create()
        {
                return view('admin::meta.create');
        }
        /* public function store
         * @param $id
         *-----------------------------------
         *|
         *-----------------------------------
         */
        public function store(\Modules\Meta\Http\Requests\CreateMetaReq $request)
        {
                $meta = $this->meta->makeTag($request);
                return redirect()->route('admin.meta.index')->with('success', 'Мета-тег создан.');
        }
        /* public function edit
         * @param $id
         *-----------------------------------
         *|
         *-----------------------------------
         */
        public function edit($id)
        {
                $meta = $this->meta->find($id);
                return view('admin::meta.edit', compact('meta'));
        }
        /* public function update
         * @param $id
         *-----------------------------------
         *|
         *-----------------------------------
         */
        public function update(\Modules\Meta\Http\Requests\CreateMetaReq $request, $id)
        {
                $meta = $this->meta->changeTag($request, $id);
                return redirect()->route('admin.meta.index')->with('success', 'Мета-тег обновлен.');
        }
        /* public function destroy
         * @param $id
         *-----------------------------------
         *|
         *-----------------------------------
         */
        public function destroy($id)
        {
                $meta = $this->meta->delete($id);
        }
}
