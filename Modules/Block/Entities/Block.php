<?php

namespace Modules\Block\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Block extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $table = 'block';
    
    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    /* public function getFieldStatusAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldStatusAttribute()
    {
        $field = new \stdClass;
        $label[0] = 'danger';
        $label[1] = 'success';
        $field->label = '<span class="label label-'.$label[$this->status].'">'.config('block.status.'.$this->status).'</span>';
        return $field;
    }
}
