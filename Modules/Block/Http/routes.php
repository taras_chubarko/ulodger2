<?php

Route::group(['middleware' => 'web', 'prefix' => 'block', 'namespace' => 'Modules\Block\Http\Controllers'], function()
{
    Route::get('/', 'BlockController@index');
});
/*
 * Админка
 */
Route::group(['middleware' => ['web', 'Modules\Admin\Http\Middleware\AdminMDW'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\Block\Http\Controllers'], function()
{
    Route::resource('block', 'AdminBlockController');
});
