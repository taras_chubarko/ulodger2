<?php

namespace Modules\Block\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateREQ extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        //
        $rules['name'] = 'required';
        $rules['body'] = 'required';
        //
        return $rules;
    }
    /*
     *
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        //
        $rules['name'] = '"Название"';
        $rules['body'] = '"Содержимое"';
        //
        $validator->setAttributeNames($rules);
        //
        return $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
