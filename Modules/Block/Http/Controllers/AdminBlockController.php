<?php

namespace Modules\Block\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Block\Repositories\BlockRepository;

class AdminBlockController extends Controller
{
    /*
     *
     */
    protected $block;
    /*
     *
     */
    public function __construct(BlockRepository $block)
    {
        $this->block = $block;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $blocks = $this->block->paginate();
        return view('admin::block.index', compact('blocks'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::block.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(\Modules\Block\Http\Requests\CreateREQ $request)
    {
        $block = $this->block->make($request);
        return redirect()->route('admin.block.index')->with('success', trans('site.block.8eOyp9OmAN5tbDFZKbVd'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('block::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $block = $this->block->find($id);
        return view('admin::block.edit', compact('block'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(\Modules\Block\Http\Requests\CreateREQ $request, $id)
    {
        $block = $this->block->change($request, $id);
        return redirect()->route('admin.block.index')->with('success', trans('site.block.x6tpsMgACyob6WplAAx6'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $this->block->delete($id);
    }
}
