<?php

namespace Modules\Block\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BlockRepository
 * @package namespace App\Repositories;
 */
interface BlockRepository extends RepositoryInterface
{
    /*
     *
     */
    public function make($request);
    /*
     *
     */
    public function change($request, $id);
    /*
     *
     */
    public function getBody($id);
}
