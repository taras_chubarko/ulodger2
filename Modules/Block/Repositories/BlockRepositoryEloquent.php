<?php

namespace Modules\Block\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Block\Repositories\BlockRepository;
use Modules\Block\Entities\Block;
/**
 * Class BlockRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BlockRepositoryEloquent extends BaseRepository implements BlockRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Block::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /* public function make($request)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function make($request)
    {
        $block = $this->model->create([
            'name'      => $request->name,
            'title'     => $request->title,
            'body'      => $request->body,
            'vid'       => $request->vid,
            'status'    => $request->status,
        ]);
        //
        return $block;
    }
    /* public function change($request, $id)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function change($request, $id)
    {
        $block = self::update([
            'name'      => $request->name,
            'title'     => $request->title,
            'body'      => $request->body,
            'vid'       => $request->vid,
            'status'    => $request->status,
        ], $id);
        //
        return $block;
    }
    /* public function getBody($id)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getBody($id)
    {
        $block = $this->model->find($id);
        return $block;
    }
    
    
    
    
}
