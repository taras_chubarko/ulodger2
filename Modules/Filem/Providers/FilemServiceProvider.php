<?php

namespace Modules\Filem\Providers;

use Illuminate\Support\ServiceProvider;

class FilemServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->field();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Modules\Filem\Repositories\FilemRepository',
	    'Modules\Filem\Repositories\FilemRepositoryEloquent'
	);
		
	$this->app->booting(function()
	{
	    $loader = \Illuminate\Foundation\AliasLoader::getInstance();
	    $loader->alias('Filem', 'Modules\Filem\Facades\Filem');
	});
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('filem.php'),
        ]);
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'filem'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/filem');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/filem';
        }, \Config::get('view.paths')), [$sourcePath]), 'filem');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/filem');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'filem');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'filem');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
    /* public function field
    * @param $id
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function field()
    {
        \Form::macro('imagefield', function($name, $uri, $actions, $image = null)
        {
            return view('admin::filem.image-field', compact('name', 'uri', 'actions', 'image'));
        });
	
	\Form::macro('images', function($name, $uri, $actions, $images = null)
        {
            return view('admin::filem.images', compact('name', 'uri', 'actions', 'images'));
        });
    }
    
}
