<?php

namespace Modules\Filem\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Widen implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->widen(\Request::get('w'));
    }
}