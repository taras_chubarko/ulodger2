<?php

namespace Modules\Filem\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Filem\Repositories\FilemRepository;

class FilemController extends Controller
{
    /*
    *
    */
    protected $filem;
    /* public function __construct
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct(FilemRepository $filem)
    {
        $this->filem = $filem;
    }
    /* public function upload_image
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function upload_image(Request $request)
    {
        $user = \Sentinel::getUser();
        $user_id = ($user) ? $user->id : 0;

        if($request->has('file_id'))
        {
            $file = \Filem::make($request->image, $request->folder, $user_id, $status = 1, null, $request->file_id);
        }
        else
        {
            $file = \Filem::make($request->image, $request->folder, $user_id, $status = 1);
        }
        return response()->json($file, 200);
    }
    /* public function upload_images
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function upload_images(Request $request)
    {
        if($request->hasFile('file_data'))
        {
            $uri = $request->uri;
            $actions = $request->actions;
            $name = $request->name;
            $file_id = $request->file_id;
            $image = $this->filem->make($request->file_data, $uri, null, 0);
            $view = view('admin::filem.im', compact('name', 'image', 'actions', 'uri', 'file_id'))->render();
            return response()->json(['result' => $view], 200);
        }
    }
    /* public function get_image
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_image($uri = null, $size = null, $name = null)
    {
        //dd($name);
        if(!is_null($size) && !is_null($name)){
            $size = explode('x', $size);
            $cache_image = \Image::cache(function($image) use($uri, $size, $name){
                return $image->make(url('/uploads/'.$uri.'/'.$name))->fit($size[0], $size[1]);
            }, 1440); // cache for 10 minutes
            return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
        } else {
            abort(403);
        }
    }
    /* public function get_filem
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_filem($uri = null, $size = null, $name = null)
    {
        if(!is_null($size) && !is_null($name)){
            $sizeArr = explode('x', $size);
            $patentFolder = public_path().'/uploads/'.$uri;

            try{
                $list = \File::directories($patentFolder);
                $newFolder = $patentFolder.'/'.$size;

                if(!in_array($newFolder, $list))
                {
                    \File::makeDirectory($newFolder, $mode = 0777, true, true);
                }

                $file = $patentFolder.'/'.$size.'/'.$name;

                if(\File::exists($file) == false)
                {
                    $img = \Image::make($patentFolder.'/'.$name)->fit($sizeArr[0], $sizeArr[1]);
                    $img->save($file, 80);
                }

                $cache_image = \Image::cache(function($image) use($file){
                    $im = $image->make($file);
                    return $im;
                }, 1440); // cache for 10 minutes
                return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
            }
            catch (\Exception $e)
            {
                $img = \Image::canvas($sizeArr[0], $sizeArr[1], '#CCCCCC');
                return $img->response();
            }
        }
        else {
            $img = \Image::canvas($sizeArr[0], $sizeArr[1], '#CCCCCC');
            return $img->response();
        }

    }
    /* public function get_avatar
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_avatar($size = null, $user_id = null)
    {
        $defaultImage = public_path().'/uploads/avatar/nouser.png';
        if(!is_null($size) && !is_null($user_id)){

            $sizeArr = explode('x', $size);
            $patentFolder = public_path().'/uploads/avatar/';
            $user = \Sentinel::findById($user_id);

            if($user && $user->avatar->count())
            {
                $name = data_get($user, 'avatar.0.filename');
                //
                $currentImage = $patentFolder.$name;
                //
                $list = \File::directories($patentFolder);
                $newFolder = $patentFolder.'/'.$size;
                // Создаем папку превю
                if(!in_array($newFolder, $list))
                {
                    \File::makeDirectory($newFolder, $mode = 0777, true, true);
                }
                //
                $file = $patentFolder.'/'.$size.'/'.$name;
                //
                if(\File::exists($file) == false)
                {
                    $img = \Image::make($currentImage)->fit($sizeArr[0], $sizeArr[1]);
                    $img->save($file, 80);
                }
                //
                $cache_image = \Image::cache(function($image) use($file){
                    $im = $image->make($file);
                    return $im;
                }, 1440); // cache for 10 minutes
                //
                return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
            }
            else
            {
                if($user)
                {
                    $name = str_random(10).'.jpg';

                    $soc = $user->social;

                    if($soc->count())
                    {
                        $socFoto = $soc->first();
                        $img = \Image::make($socFoto->photo_big);
                        $img->save(public_path('uploads/avatar/'.$name));
                    }
                    else
                    {
                        $ava = \Avatar::create($user->first_name)->save(public_path('/uploads/avatar/'.$name), 100);
                    }


                    $fid = \Filem::create([
                        'user_id'   => $user->id,
                        'original'  => $name,
                        'filename'  => $name,
                        'uri'       => 'avatar',
                        'ext'       => 'jpg',
                        'filemime'  => 'image/jpeg',
                        'filesize'  => 1,
                        'status'    => 1,
                    ]);

                    $user->avatar()->attach($user->id,['fid' => $fid->id]);

                    //$name = data_get($user, 'avatar.0.filename');
                    //
                    $currentImage = $patentFolder.$name;
                    //
                    $list = \File::directories($patentFolder);
                    $newFolder = $patentFolder.'/'.$size;
                    // Создаем папку превю
                    if(!in_array($newFolder, $list))
                    {
                        \File::makeDirectory($newFolder, $mode = 0777, true, true);
                    }
                    //
                    $file = $patentFolder.'/'.$size.'/'.$name;
                    //
                    if(\File::exists($file) == false)
                    {
                        $img = \Image::make($currentImage)->fit($sizeArr[0], $sizeArr[1]);
                        $img->save($file, 80);
                    }
                    //
                    $cache_image = \Image::cache(function($image) use($file){
                        $im = $image->make($file);
                        return $im;
                    }, 1440); // cache for 10 minutes
                    //
                    return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
                }
                else
                {
                    $cache_image = \Image::cache(function($image) use($defaultImage){
                        $im = $image->make($defaultImage);
                        return $im;
                    }, 1440); // cache for 10 minutes
                    return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
                }

            }

        }
        else
        {
            $cache_image = \Image::cache(function($image) use($defaultImage){
                $im = $image->make($defaultImage);
                return $im;
            }, 1440); // cache for 10 minutes
            return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
        }
    }
    /* public function destroy
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function destroy($id)
    {
        $this->filem->destroy($id);
        //return $filem;
    }
}
