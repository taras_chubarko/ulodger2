<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Repositories\UserRepository;
use Modules\User\Events\UserRegisterEvent;
use Modules\User\Events\ReminderEvent;
use Modules\Host\Entities\HostBook;
use Modules\User\Entities\UserSocial;
use Modules\User\Entities\UserProvide;
use Modules\User\Emails\ProvideToAdmin;
use Modules\User\Emails\VerifyMail;
use Modules\User\Entities\User as UserQ;
use Modules\Host\Emails\MailBookReqApply;

class UserController extends Controller
{
    /*
     *
     */
    protected $user;
    /*
     *
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }
    /* public function login_submit
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function login_submit(Request $request)
    {
        try
        {
            $credentials = [
                'email'    => $request->email,
                'password' => $request->password,
            ];
            $user = \Sentinel::findByCredentials($credentials);
            //dd($user);
            if($user){
                //if($user->ban->count())
                //{
                //    return response()->json(['error' => [0 => 'User is blocked']], 442);
                //}
                //else
                //{
                $user = \Sentinel::authenticate($request->all());

                if($user)
                {
                    $user->load('roles');
                    //return redirect()->back()->with('error', 'Вы успешно вошли на сайт.');
                    if($user->inRole('admin'))
                    {
                        $data['redirect'] = '/dashboard';
                        $data['cartalyst_sentinel'] = session('cartalyst_sentinel');
                        $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
                        $data['user'] = $user;
                        return response()->json($data, 200);
                    }
                    else
                    {
                        $data['redirect'] = '/dashboard';
                        $data['cartalyst_sentinel'] = session('cartalyst_sentinel');
                        $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
                        $data['user'] = $user;
                        \Session::flash('success', 'You have successfully entered the site.');
                        return response()->json($data, 200);
                    }
                }
                else
                {
                    //return redirect()->back()->with('error', 'Не верный логин или пароль');
                    return response()->json(['error' => [0 => 'Wrong login or password.']], 442);
                }
                //}
            }
            else{
                //\Session::flash('info', 'Пользователь не существует.');
                return response()->json(['error' => [0 => 'This user does not exist.']], 442);
            }
        }
        catch(\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e){
            //return redirect()->back()->with('error', 'Пользователь не активирован.');
            return response()->json(['error' => [0 => 'The user is not activated.']], 442);
        }
        catch(\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e){
            //return redirect()->back()->with('error', 'Пользователь заблокирован.');
            return response()->json(['error' => [0 => 'The user is blocked.']], 442);
        }
    }
    /* public function logout
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function logout(Request $request)
    {
        $user = \Sentinel::getUser();
        //\Cache::forget('user-is-online-' . $user->id);
        \Sentinel::logout($user, true);

        if($request->ajax())
        {
            return response()->json('OK', 200);
        }
        else
        {
            return redirect('/');
        }
    }
    /* public function api_logout
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function api_logout(Request $request)
    {
        $user = \Sentinel::getUser();
        //\Cache::forget('user-is-online-' . $user->id);
        \Sentinel::logout($user, true);
    }
    /* public function reminder_submit
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function reminder_submit(Request $request)
    {
        $credentials = [
            'email'    => $request->email,
        ];
        $user = \Sentinel::findByCredentials($credentials);
        //
        if($user)
        {
            $reminder = \Reminder::create($user);
            $link = config('app.url').'/reminder/'.$reminder->code;//route('reminder.code', $reminder->code);
            event(new ReminderEvent($user, $credentials, $reminder, $link));
        }
        else
        {
            return response()->json(['error' => [0 => 'Пользователь не существует.']], 442);
        }
    }
    /* public function reminder_code
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function reminder_code($code)
    {
        $reminder = \DB::table('reminders')->where('code', $code)->first();

        return view('site::user.page-reminder', compact('reminder'));
    }
    /* public function reminder_set_new_password
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function reminder_set_new_password(Request $request)
    {
        $user = UserQ::where('email', $request->email)->first();
        if($user)
        {
            $password = str_random(6);
            $credentials = [
                'password' => $password,
            ];

            $user = \Sentinel::update($user, $credentials);
            \Mail::send(new \Modules\User\Emails\NewPassMail($user, $password));

            return response()->json('ok', 200);
        }
        else
        {
            return response()->json(['error' => [0 => 'The user with this mail is not registered!']], 442);
        }
    }
    /* public function register
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function register(Request $request)
    {
        $credentials = [
            'email'    => $request->email,
        ];
        $user = \Sentinel::findByCredentials($credentials);

        if($user)
        {
            return response()->json(['error' => [0 => 'The user with this mail is already registered!']], 442);
        }
        else
        {
            $credentials = [
                'email'    	=> $request->email,
                'first_name'    => $request->first_name,
                'last_name'    	=> $request->last_name,
                'password' 	=> $request->password,
            ];

            $user = \Sentinel::registerAndActivate($credentials);
            $rore_user = \Sentinel::findRoleById(3);
            $rore_user->users()->attach($user);

            $user->phone()->attach($user->id,[
                'phone' 	=> $request->phone,
                'verify' 	=> 0,
            ]);

            \Sentinel::login($user);

            event(new UserRegisterEvent($user));

            $data['redirect'] = '/dashboard';
            $data['cartalyst_sentinel'] = session('cartalyst_sentinel');
            $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
            $data['user'] = $user;
            return response()->json($data, 200);
        }
    }
    /* public function user_switch
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_switch($id)
    {
        if (\Sentinel::check())
        {
            \Sentinel::logout();
        }

        $user = \Sentinel::findById($id);

        \Sentinel::login($user);

        $data['redirect'] = '/';
        $data['cartalyst_sentinel'] = session('cartalyst_sentinel');
        $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
        $data['user'] = $user;
        return response()->json($data, 200);
    }
    /* public function settings
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function settings(Request $request, $id)
    {
        $user = \Sentinel::findById($id);
        return response()->json($user, 200);
    }
    /* public function settings_save
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function settings_save(Request $request, $id)
    {
        $user = \Sentinel::findById($id);

        $credentials['first_name'] = $request->first_name;
        $credentials['last_name'] = $request->last_name;
        $credentials['telefon'] = $request->telefon;
        if($request->has('password_conf'))
        {
            $credentials['password'] = $request->password_conf;
        }
        $user = \Sentinel::update($user, $credentials);

        return response()->json($user, 200);
    }
    /* public function orders
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function orders(Request $request, $id)
    {
        $orders = \Shop::orderBy('created_at', 'DESC')->findWhere(['user_id' => $id]);
        $orders->load('items');
        return response()->json($orders, 200);
    }
    /* public function topics
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function topics(Request $request, $id)
    {
        $topics = Forum::where('user_id', $id)->orderBy('created_at', 'DESC')->get();
        $topics->load('user', 'posts.user', 'forum');
        //
        $result['topics'] = $topics;
        //
        return response()->json($result, 200);
    }
    /* public function check_user
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function check_user()
    {
        if ($user = \Sentinel::check())
        {
            $result['logined'] = true;
            $result['data'] = $user;
            return response()->json($result, 200);
        }
        else
        {
            $result['logined'] = false;
            $result['data'] = [];
            return response()->json($result, 200);
        }
    }
    /* public function get_user_data
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_user_data(Request $request, $data)
    {
        $user = \Sentinel::getUser();
        //
        switch($data)
        {
            case 'notifications':
                $user->load('notification');
                return response()->json($user, 200);
                break;

            case 'privacy':
                $user->load('privacy');
                return response()->json($user, 200);
                break;
        }
    }
    /* public function set_user_data
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function set_user_data(Request $request, $data)
    {
        $user = \Sentinel::getUser();
        //
        switch($data)
        {
            case 'notifications':

                $user->notification()->detach($user->id);
                if($request->notifi)
                {
                    foreach($request->notifi as $notifi)
                    {
                        $user->notification()->attach($user->id, [
                            'notifi' => $notifi,
                        ]);
                    }
                }

                $user->load('notification');

                return response()->json($user, 200);
                break;

            case 'privacy':

                $user->privacy()->detach($user->id);
                $user->privacy()->attach($user->id, [
                    'include_in_search_engines' => $request->include_in_search_engines,
                ]);

                $user->load('privacy');
                return response()->json($user, 200);
                break;

            case 'security':
                $credentials = [
                    'password' => $request->password_confirmation,
                ];

                $user = \Sentinel::update($user, $credentials);
                break;

            case 'country':

                $user->residence()->detach($user->id);
                $user->residence()->attach($user->id, [
                    'country' => $request->country,
                ]);

                $user->load('residence');
                return response()->json($user, 200);
                break;

            case 'remove':

                $user->remove()->detach($user->id);
                $user->remove()->attach($user->id, [
                    'reason' 	=> $request->reason,
                    'details' 	=> $request->details,
                    'contact_ok'=> $request->contact_ok,
                ]);

                \Sentinel::logout($user, true);

                $user->delete();
                break;

        }
    }
    /* public function user_profile_data
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile()
    {
        $data['gender'] = [
            1 => 'Male',
            2 => 'Female',
            3 => 'Other',
        ];
        //
        $data['month'] = month_array();
        //
        $days = array_combine(range(1, 31), range(1, 31));
        $data['days'] = $days;
        //
        $years = array_combine(range(date('Y'), 1950), range(date('Y'), 1950));
        krsort($years, SORT_NUMERIC);
        $data['years'] = $years;
        //
        $data['timezone'] = array_flip(config('timezone'));
        $data['langs'] = config('lang');
        //
        $result['settings'] = $data;

        $user = \Sentinel::getUser();

        $birthdate = data_get($user, 'birthdate.0.pivot.birthdate');
        $birthdate = ($birthdate) ? \Carbon::parse($birthdate) : null;

        $profile = [
            'id' 	=> $user->id,
            'first_name'=> $user->first_name,
            'last_name' => $user->last_name,
            'email' 	=> $user->email,
            'gender'	=> data_get($user, 'gender.0.pivot.gender_id'),
            'birthdate' => [
                'month' => ($birthdate) ? $birthdate->format('n') : null,
                'day' 	=> ($birthdate) ? $birthdate->format('j') : null,
                'year' 	=> ($birthdate) ? $birthdate->format('Y') : null,
            ],
            'location' => [
                'address' 	=> data_get($user, 'location.0.pivot.address'),
                'location' 	=> data_get($user, 'location.0.pivot.location'),
                'region' 	=> data_get($user, 'location.0.pivot.region'),
                'lat' 		=> data_get($user, 'location.0.pivot.lat'),
                'lng' 		=> data_get($user, 'location.0.pivot.lng'),
            ],
            'about' 	=> data_get($user, 'about.0.pivot.about'),
            'school' 	=> data_get($user, 'school.0.pivot.school'),
            'work' 	=> data_get($user, 'work.0.pivot.work'),
            'timezone' 	=> data_get($user, 'timezone.0.pivot.timezone'),
            'phone'	=> data_get($user, 'phone'),
            'lang'	=> data_get($user, 'lang'),
            'emergency' => [
                'name' 		=> data_get($user, 'emergency.0.pivot.name'),
                'phone' 	=> data_get($user, 'emergency.0.pivot.phone'),
                'email' 	=> data_get($user, 'emergency.0.pivot.email'),
                'relationship' 	=> data_get($user, 'emergency.0.pivot.relationship'),
            ],
            'avatar' => data_get($user, 'avatar'),

        ];
        $result['user'] = $profile;

        return response()->json($result, 200);
    }
    /* public function user_profile_save
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_save(Request $request)
    {
        $user = \Sentinel::getUser();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->save();
        //
        $user->gender()->detach();
        if($request->has('gender'))
        {
            $user->gender()->attach($user->id, [
                'gender_id' => $request->gender,
            ]);
        }
        //
        $user->birthdate()->detach();
        if($request->has('birthdate'))
        {
            $birthdate = $request->birthdate['year'].'-'.$request->birthdate['month'].'-'.$request->birthdate['day'];
            $user->birthdate()->attach($user->id, [
                'birthdate' => \Carbon::parse($birthdate),
            ]);
        }
        //
        $user->location()->detach();
        if($request->has('location'))
        {
            $user->location()->attach($user->id, [
                'address' => $request->location['address'],
                'location' => $request->location['location'],
                'region' => $request->location['region'],
                'lat' => $request->location['lat'],
                'lng' => $request->location['lng'],
            ]);
        }
        //
        $user->about()->detach();
        if($request->has('about'))
        {
            $user->about()->attach($user->id, [
                'about' => $request->about,
            ]);
        }
        //
        $user->school()->detach();
        if($request->has('school'))
        {
            $user->school()->attach($user->id, [
                'school' => $request->school,
            ]);
        }
        //
        $user->work()->detach();
        if($request->has('work'))
        {
            $user->work()->attach($user->id, [
                'work' => $request->work,
            ]);
        }
        $user->timezone()->detach();
        if($request->has('timezone'))
        {
            $user->timezone()->attach($user->id, [
                'timezone' => $request->timezone,
            ]);
        }
        $user->emergency()->detach();
        if($request->has('emergency'))
        {
            $user->emergency()->attach($user->id, [
                'name' => $request->emergency['name'],
                'phone' => $request->emergency['phone'],
                'email' => $request->emergency['email'],
                'relationship' => $request->emergency['relationship'],
            ]);
        }

    }
    /* public function user_profile_verify_phone
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_verify_phone(Request $request)
    {
        $user = \Sentinel::getUser();

        if($user->phone->count())
        {
            if($request->verify == 1)
            {
                return $this->addPhone($user, $request);
            }
            else
            {
                $phones = $user->phone->pluck('pivot.phone');
                if($phones->contains($request->phone))
                {
                    return response()->json(['error' => 'This phone is already on your list, enter another one.'], 442);
                }
                else
                {
                    return $this->addPhone($user, $request);
                }
            }
        }
        else
        {
            return $this->addPhone($user, $request);
        }
    }
    /* public function addPhone
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function addPhone($user, $request)
    {
        //dd($request->all());
        $phone = str_replace("(", "", $request->phone);
        $phone = str_replace(")", "", $phone);
        $phone = str_replace("-", "", $phone);

        if($request->verify == 0)
        {
            $user->phone()->attach($user->id, [
                'phone'  => $request->phone,
                'verify' => 0,
            ]);
        }

        $verifyPhone = [
            'phone' => $request->phone,
            'code'  => random_number(),
        ];

        session(['verifyPhone' => $verifyPhone]);

        $result['verifyPhone'] = $verifyPhone;

        $user->load('phone');
        $result['user'] = $user;


        $sms = \Nexmo::message()->send([
            'to'   => $phone, //14079218209
            'from' => 12014643496,
            'text' => 'Ulodger: Your verification code is:'.$verifyPhone['code'],
        ]);
        $result['sms'] = $sms;


        return response()->json($result, 200);
    }
    /* public function user_profile_remove_phone
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_remove_phone(Request $request)
    {
        $user = \Sentinel::getUser();
        if($user->phone->count())
        {
            $phones = $user->phone->pluck('pivot.phone');
            if($phones->contains($request->phone))
            {
                \DB::table('users_phone')
                    ->where('user_id', $user->id)
                    ->where('phone', $request->phone)
                    ->delete();
            }
        }
    }
    /* public function user_profile_complete_phone
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_complete_phone(Request $request)
    {
        $user = \Sentinel::getUser();
        \DB::table('users_phone')
            ->where('user_id', $user->id)
            ->where('phone', $request->phone)
            ->update(['verify' => 1]);
        $request->session()->forget('verifyPhone');

        $user->load('phone');
        $result['user'] = $user;

        return response()->json($result, 200);
    }
    /* public function user_profile_lang_add
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_lang_add(Request $request)
    {
        $user = \Sentinel::getUser();
        $user->lang()->detach();
        foreach($request->lang as $lang)
        {
            $user->lang()->attach($user->id, [
                'lang_id' => $lang,
            ]);
        }
        $user->load('lang');
        $result['user'] = $user;

        return response()->json($result, 200);
    }
    /* public function user_profile_lang_remove
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_lang_remove(Request $request)
    {
        $user = \Sentinel::getUser();
        \DB::table('users_lang')
            ->where('user_id', $user->id)
            ->where('lang_id', $request->lang_id)
            ->delete();
        $user->load('lang');
        $result['user'] = $user;
        return response()->json($result, 200);
    }
    /* public function user_profile_photo
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_photo()
    {
        $user = \Sentinel::getUser();
        $user->load('avatar');
        $result['user'] = $user;
        return response()->json($result, 200);
    }
    /* public function user_profile_photo_save
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_photo_save(Request $request)
    {
        $user = \Sentinel::getUser();
        $fid = data_get($user, 'avatar.0.pivot.fid');
        if($fid)
        {
            \Filem::destroy($fid);
        }
        $file = \Filem::make($request->webcam, 'avatar', $user->id, $status = 1);
        $user->avatar()->detach();
        $user->avatar()->attach($user->id, [
            'fid' =>  $file->id,
        ]);
        $user->load('avatar');
        $result['user'] = $user;
        return response()->json($result, 200);
    }
    /* public function user_profile_photo_upload
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_photo_upload(Request $request)
    {
        $user = \Sentinel::getUser();
        $fid = data_get($user, 'avatar.0.pivot.fid');
        if($fid)
        {
            \Filem::destroy($fid);
        }
        $file = \Filem::make($request->image, 'avatar', $user->id, $status = 1);
        $user->avatar()->detach();
        $user->avatar()->attach($user->id, [
            'fid' =>  $file->id,
        ]);
        $user->load('avatar');
        $result['user'] = $user;
        return response()->json($result, 200);
    }
    /* public function user_account_settings
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_account_settings()
    {
        $user = \Sentinel::getUser();
        $settings['country'] = \Geo::getCountryNameArr();

        $user->load('residence');
        $result['user'] = $user;
        $result['settings'] = $settings;
        return response()->json($result, 200);
    }
    /* public function user_dashboard
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_dashboard()
    {
        $user = \Sentinel::getUser();

        $user2 = collect($user->toArray());

        $user2 = $user2->merge([
            'verifyPhone' => $user->fieldVerifyPhone,
            'connectedAccounts' => ($user->social->count()) ? $user->social : null,

        ]);

        $result['user'] = $user2;

        return response()->json($result, 200);
    }
    /* public function user_yuor_listing
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_yuor_listing()
    {
        $user = \Sentinel::getUser();
        $hostInProgress = \Host::with(['photo', 'description'])->orderBy('created_at', 'DESC')->findWhere(['user_id' => $user->id, 'status' => 0]);
        $hostPublished = \Host::with(['photo', 'description'])
            ->orderBy('created_at', 'DESC')
            ->findWhere(['user_id' => $user->id, 'status' => 1]);


        $result['hostInProgress'] = $hostInProgress;
        $result['hostPublished'] = $hostPublished;
        return response()->json($result, 200);
    }
    /* public function social_login
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function social_login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
        ];

        $user = \Sentinel::findByCredentials($credentials);

        if($user)
        {
            $this->user->socialLogin($request, $user);
            \Sentinel::login($user);
            $data['redirect'] = '/dashboard';
            $data['cartalyst_sentinel'] = session('cartalyst_sentinel');
            $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
            $data['user'] = $user;
            return response()->json($data, 200);
        }
        else
        {
            $checkRemove = \DB::table('users')
                ->where('email', $request->email)
                ->whereNotNull('deleted_at')->first();
            if($checkRemove)
            {
                $result['error'] = 'User was removed. Please contact with adminisntator.';
                return response()->json($result, 200);
            }
            else
            {
                $user = $this->user->socialRegister($request);
                \Sentinel::login($user);
                $data['redirect'] = '/dashboard';
                $data['cartalyst_sentinel'] = session('cartalyst_sentinel');
                $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
                $data['user'] = $user;
                return response()->json($data, 200);
            }
        }
    }
    /* public function slug
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function slug($slug)
    {
        $user = $this->user->findWhere(['slug' => $slug])->first();
        $result = [
            'id'            => $user->id,
            'first_name'    => $user->first_name,
            'address'       => $user->address,
            'about'         => $user->field_about,
            'countReviews'  => 0,
            'verified'      => $user->field_verified->value,
            'verifieds'     => $user->field_verifieds->values,
            'social'        => $user->field_social->values,
        ];

        return response()->json($result, 200);
    }
    /* public function user_yuor_reservations
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_yuor_reservations()
    {
        $user = \Sentinel::getUser();
        $requests = HostBook::orderBy('created_at', 'DESC')->with('host.photo', 'host.description', 'user', 'review')
            ->where('user_id', $user->id)
            ->orWhere('host_user_id', $user->id)
            ->whereIn('status', [1,2])
            ->paginate();

        return response()->json($requests, 200);
    }
    /* public function user_yuor_requests
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_yuor_requests()
    {
        $user = \Sentinel::getUser();
        $requests = HostBook::orderBy('created_at', 'DESC')->with('host.photo', 'host.description', 'user', 'review')
            ->where('host_user_id', $user->id)
            //->whereIn('status', [0, 1, 2])
            ->whereIn('status', [0])
            ->paginate();

        return response()->json($requests, 200);
    }
    /* public function user_yuor_requests_reservation
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_yuor_requests_reservation(Request $request, $id)
    {
        $requests = HostBook::find($id);
        $requests->guests = $request->lodgers;
        $requests->check_in = \Carbon::parse($request->check_in);
        $requests->check_out = \Carbon::parse($request->check_out);
        $requests->status = 1;
        $requests->save();

        $requests2 = HostBook::find($id);
        $requests2->load('host.photo', 'host.description', 'user', 'review');

        $host = \Host::find($requests->host_id);
        $host->status = 3;
        $host->save();

        $user = \User::find($requests->user_id);


        \Mail::send(new MailBookReqApply($request, $user, $host));


        return response()->json($requests2, 200);
    }
    /* public function user_profile_verify
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_verify(Request $request)
    {
        $user = \Sentinel::getUser();
        $user->load('social', 'phone');

        return response()->json($user, 200);
    }
    /* public function social_connect
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function social_connect(Request $request)
    {
        $user = \Sentinel::getUser();

        $soc = UserSocial::create([
            'user_id' 		=> $user->id,
            'manual' 		=> $request->manual,
            'email' 		=> $request->email,
            'phone' 		=> $request->phone,
            'city' 		=> $request->city,
            'network' 		=> $request->network,
            'identity' 		=> $request->identity,
            'photo_big' 	=> $request->photo_big,
            'profile' 		=> $request->profile,
            'last_name' 	=> $request->last_name,
            'uid' 		=> $request->uid,
            'photo' 		=> $request->photo,
            'verified_email' 	=> $request->verified_email,
            'first_name' 	=> $request->first_name,
        ]);

        $user->load('social', 'phone');

        return response()->json($user, 200);
    }
    /* public function social_disconnect
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function social_disconnect(Request $request, $id)
    {
        UserSocial::find($id)->delete();

        $user = \Sentinel::getUser();
        $user->load('social', 'phone');
        //
        return response()->json($user, 200);
    }
    /* public function user_profile_photo_verify
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_photo_verify(Request $request)
    {
        $user = \Sentinel::getUser();

        $provide = UserProvide::where('user_id', $user->id)->first();

        if($provide)
        {
            \Filem::destroy($provide->fid);
            $file = \Filem::make($request->webcam, 'provide', $user->id, $status = 1);
            $provide->fid = $file->id;
            $provide->save();
        }
        else
        {
            $file = \Filem::make($request->webcam, 'provide', $user->id, $status = 1);
            $provide = UserProvide::create([
                'user_id' 	=> $user->id,
                'fid' 		=> $file->id,
                'status' 	=> 0,
            ]);
            //
            \Mail::send(new ProvideToAdmin($provide, $user));
        }

        return response()->json($user, 200);
    }
    /* public function user_profile_verify_email
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_verify_email(Request $request)
    {
        $result['code'] = encrypt($request->email);
        \Mail::send(new VerifyMail($request, $result['code']));
        return response()->json($result, 200);
    }
    /* public function user_profile_verify_email_code
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_profile_verify_email_code($code)
    {
        $email = decrypt($code);
        $user = UserQ::where('email', $email)->first();
        $user->verify_email = 1;
        $user->save();

        return redirect('/my-page/verification');
    }
}
