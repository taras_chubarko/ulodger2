<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Repositories\UserRepository;
use Modules\User\Entities\UserProvide;
use Modules\User\Emails\ProvideToUser;

class AdminUserController extends Controller
{
    /*
     *
     */
    protected $user;
    /*
     *
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $this->user->pushCriteria(app('Modules\User\Criteria\UserAdminCriteria'));
        $users = $this->user->paginate();
        return view('admin::user.index', compact('users'));
    }
    /* public function create_admin
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function create()
    {
        return view('admin::user.create');
    }
    /* public function store_admin
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function store(\Modules\User\Http\Requests\CreateAdmin $request)
    {
        $credentials = [
            'first_name'=> $request->first_name,
            'last_name'	=> $request->last_name,
            'telefon'	=> $request->telefon,
            'email'    	=> $request->email,
            'password' 	=> $request->password,
        ];

        $user = \Sentinel::registerAndActivate($credentials);

        foreach($request->role_id as $role_id)
        {
            $role = \Sentinel::findRoleById($role_id);
            $role->users()->attach($user);
        }


        return redirect()->route('admin.users.index')->with('success', 'Пользователь создан.');
    }
    /* public function edit_admin
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function edit($id)
    {
        $user = $this->user->find($id);
        return view('admin::user.edit', compact('user'));
    }
    /* public function update_admin
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function update(\Modules\User\Http\Requests\UpdateAdmin $request, $id)
    {
        $user = \Sentinel::findById($id);
        $credentials['first_name'] = $request->first_name;
        $credentials['last_name'] = $request->last_name;
        $credentials['telefon'] = $request->telefon;
        $credentials['email'] = $request->email;
        if($request->password)
        {
            $credentials['password'] = $request->password;
        }

        $user = \Sentinel::update($user, $credentials);

        foreach($user->rolesArr as $def)
        {
            $roleDel = \Sentinel::findRoleById($def);
            $roleDel->users()->detach($user);
        }

        foreach($request->role_id as $role)
        {
            $role = \Sentinel::findRoleById($role);
            $role->users()->attach($user);
        }

        return redirect()->route('admin.users.index')->with('success', 'Пользователь обновлен.');
    }
    /* public function activate
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function activate($id)
    {
        $user = \Sentinel::findById($id);
        $activation = \Activation::create($user);
        \Activation::complete($user, $activation->code);
    }
    /* public function ban
    * @param $id use Illuminate\Http\Request;
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function ban($id)
    {
        $user = \Sentinel::findById($id);
        $user->ban()->attach($user->id);
    }
    /* public function unban
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function unban($id)
    {
        $user = \Sentinel::findById($id);
        $user->ban()->detach();
    }
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->user->find($id);
        $user->delete();
    }
    /* public function provide
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function provide()
    {
        $users = UserProvide::orderBy('created_at', 'DESC')->with('user', 'photo')->paginate();
        //dd($users);
        return view('admin::user.provide-index', compact('users'));
    }
    /* public function provide_edit
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function provide_edit($id)
    {
        $provide = UserProvide::find($id);
        $provide->load('user', 'photo');
        return view('admin::user.provide-edit', compact('provide'));
    }
    /* public function provide_edit_save
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function provide_edit_save(Request $request, $id)
    {
        //dd($request->all());
        //
        $provide = UserProvide::find($id);
        $provide->comment = $request->comment;
        $provide->status = $request->status;
        $provide->save();

        $user = \User::find($provide->user_id);
        $user->provide = $request->status;
        $user->save();

        if($request->has('send_user_msg'))
        {
            \Mail::send(new ProvideToUser($provide));
        }

        return redirect()->route('admin.users.provide')->with('success', 'Provide status updated.');
    }
}
