<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Repositories\UserRepository;
use Ixudra\Curl\Facades\Curl;

class SocialiteController extends Controller
{
    protected $userRepository;
    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    /* public function socialite
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function socialite($network)
    {
        $soc = \Socialite::driver($network)->stateless()->redirect()->getTargetUrl();
        //dd($soc);
        //return $soc;

//        $response = \Curl::to($soc)
//            //->withHeader('User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36')
//            //->withHeader('Accept: */*')
//            //->withHeader('Accept-Encoding: gzip, deflate, br')
//            //->withHeader('Accept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6')
//            //->withHeader('Cookie: sb=wBmbV_AH819mE3f1vFgjV0yE; datr=ceLsWQ9tdj_Oh5Sv9iPwR1Fl; c_user=1473870837; xs=63%3ASgRxiBXGza5Qtg%3A2%3A1469782464%3A16433%3A15704; wd=1600x804; presence=EDvF3EtimeF1518959355EuserFA21473870837A2EstateFDutF1518959355282CEchFDp_5f1473870837F2CC; fr=0oZqNtdRZjEdwFId1.AWUdb_4L39mz2VFZb-1TWqsjTEE.BXmxnA.iU.FqJ.0.0.BaiXwo.AWWM5Noz; act=1518959678608%2F0')
//            ->withResponseHeaders()
//            ->returnResponseObject()
//            ->get();
//        $headers = $response->headers;
//        //dd($response);
//
//        $response2 = \Curl::to($headers['Location'])
//            //->withHeader('Access-Control-Allow-Origin: *')
//            //->withHeader('User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36')
//            ->withResponseHeaders()
//            ->returnResponseObject()
//            ->get();
//        $content = $response2->content;
//        dd($response2);

        return response()->json($soc, 200);
    }

    /* public function socialite_callback
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function socialite_callback($network)
    {
        $user = \Socialite::driver($network)->stateless()->user();
        $js = json_encode($user);
        //dd($user);
        //return response()->json($user, 200);
        return view('site::layouts.soc', compact('js'));
    }
}
