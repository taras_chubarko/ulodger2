<?php

//Route::group(['middleware' => 'web', 'prefix' => 'user', 'namespace' => 'Modules\User\Http\Controllers'], function()
//{
//    Route::get('/', 'UserController@index');
//});
Route::get('api/us/{id}', [
    'middleware' 	=> 'web',
    'uses' 		=> 'Modules\User\Http\Controllers\UserController@user_switch'
]);

Route::post('login', [
    'middleware' 	=> ['web', 'guest'],
    'as' 		=> 'login',
    'uses' 		=> 'Modules\User\Http\Controllers\UserController@login_submit'
]);

Route::post('/user/account/logout', [
    'middleware' 	=> ['web'],
    'as' 		=> 'logout',
    'uses' 		=> 'Modules\User\Http\Controllers\UserController@logout'
]);

Route::post('api-v1/logout', [
    'middleware' 	=> ['web'],
    'as' 		=> 'api.logout',
    'uses' 		=> 'Modules\User\Http\Controllers\UserController@api_logout'
]);

Route::post('reminder', [
    'middleware' 	=> ['web', 'guest'],
    'as' 		=> 'reminder',
    'uses' 		=> 'Modules\User\Http\Controllers\UserController@reminder_submit'
]);

Route::get('socialite/{network}', [
    'middleware' 	=> ['web'],
    'as' 		=> 'socialite.network',
    'uses' 		=> 'Modules\User\Http\Controllers\SocialiteController@socialite'
]);

Route::get('socialite/{network}/callback', [
    'middleware' 	=> ['web'],
    'as' 		=> 'socialite.network.callback',
    'uses' 		=> 'Modules\User\Http\Controllers\SocialiteController@socialite_callback'
]);

//Route::get('reminder/{code}', [
//    'middleware' 	=> ['web', 'guest'],
//    'as' 		=> 'reminder.code',
//    'uses' 		=> 'Modules\User\Http\Controllers\UserController@reminder_code'
//]);

Route::post('set/new/password', [
    'middleware' 	=> ['web', 'guest'],
    'as' 		=> 'reminder.set.new.password',
    'uses' 		=> 'Modules\User\Http\Controllers\UserController@reminder_set_new_password'
]);

Route::post('register', [
    'middleware' 	=> ['web', 'guest'],
    'as' 		=> 'reminder',
    'uses' 		=> 'Modules\User\Http\Controllers\UserController@register'
]);

Route::post('check/user', [
    'middleware' 	=> ['web'],
    'as' 		=> 'check.user',
    'uses' 		=> 'Modules\User\Http\Controllers\UserController@check_user'
]);

Route::post('social/login', [
    'middleware' 	=> ['web'],
    'as' 		=> 'social.login',
    'uses' 		=> 'Modules\User\Http\Controllers\UserController@social_login'
]);

Route::post('social/connect', [
    'middleware' 	=> ['web'],
    'as'    		=> 'user.social.connect',
    'uses' 		=> 'Modules\User\Http\Controllers\UserController@social_connect'
]);

Route::post('social/disconnect/{id}', [
    'middleware' 	=> ['web'],
    'as'    		=> 'user.social.disconnect',
    'uses' 		=> 'Modules\User\Http\Controllers\UserController@social_disconnect'
]);

Route::post('user/{slug}', [
    'middleware' 	=> ['web'],
    'as' 		=> 'user.slug',
    'uses' 		=> 'Modules\User\Http\Controllers\UserController@slug'
]);

Route::get('/verify/email/{code}', [
    'as'   => 'verify.email',
    'uses' => 'Modules\User\Http\Controllers\UserController@user_profile_verify_email_code'
]);



/*
 *
 */
Route::group(['middleware' => 'web', 'prefix' => 'user', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
    
    Route::post('/get/{data}', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.get.data',
	'uses' 	        => 'UserController@get_user_data'
    ]);
    
    Route::post('/set/{data}', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.set.data',
	'uses' 	        => 'UserController@set_user_data'
    ]);
    
    
    
    Route::post('/profile/data', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile',
	'uses' 	        => 'UserController@user_profile'
    ]);
    
    Route::post('/profile/save', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile.save',
	'uses' 	        => 'UserController@user_profile_save'
    ]);
    
    Route::post('/profile/verify/phone', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile.verify.phone',
	'uses' 	        => 'UserController@user_profile_verify_phone'
    ]);
    
    Route::post('/profile/remove/phone', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile.remove.phone',
	'uses' 	        => 'UserController@user_profile_remove_phone'
    ]);
    
    Route::post('/profile/complete/phone', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile.complete.phone',
	'uses' 	        => 'UserController@user_profile_complete_phone'
    ]);
    
    Route::post('/profile/verify/email', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile.verify.email',
	'uses' 	        => 'UserController@user_profile_verify_email'
    ]);
    
    Route::post('/profile/lang/add', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile.lang.add',
	'uses' 	        => 'UserController@user_profile_lang_add'
    ]);
    
    Route::post('/profile/lang/remove', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile.lang.add',
	'uses' 	        => 'UserController@user_profile_lang_remove'
    ]);
    
    Route::post('/profile/photo', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile.photo',
	'uses' 	        => 'UserController@user_profile_photo'
    ]);
    
    Route::post('/profile/photo/save', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile.photo.save',
	'uses' 	        => 'UserController@user_profile_photo_save'
    ]);
    
    Route::post('/profile/photo/upload', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile.photo.upload',
	'uses' 	        => 'UserController@user_profile_photo_upload'
    ]);
    
    Route::post('/account/settings', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.account.settings',
	'uses' 	        => 'UserController@user_account_settings'
    ]);
	
    Route::post('/account/dashboard', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.dashboard',
	'uses' 	        => 'UserController@user_dashboard'
    ]);
    
    Route::post('/account/yuor-listing', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.yuor.listing',
	'uses' 	        => 'UserController@user_yuor_listing'
    ]);
    
    Route::post('/yuor-listing/reservations', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.yuor.reservations',
	'uses' 	        => 'UserController@user_yuor_reservations'
    ]);
    
    Route::post('/yuor-listing/requests', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.yuor.requests',
	'uses' 	        => 'UserController@user_yuor_requests'
    ]);
    
    Route::post('/reservation-requests/{id}', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.yuor.requests',
	'uses' 	        => 'UserController@user_yuor_requests_reservation'
    ]);
    
    Route::post('/profile/verify', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile.verify',
	'uses' 	        => 'UserController@user_profile_verify'
    ]);
    
    Route::post('/profile/photo/verify', [
        //'middleware'    => 'Modules\User\Http\Middleware\MDWeditUser',
	'as'            => 'user.profile.photo.verify',
	'uses' 	        => 'UserController@user_profile_photo_verify'
    ]);
    
});
/*
 * Админка
 */
Route::group(['middleware' => ['web', 'Modules\Admin\Http\Middleware\AdminMDW'], 'prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
    Route::resource('users', 'AdminUserController');
    
    Route::post('users/{id}/activate', [
        'as' => 'users.activate',
        'uses' => 'AdminUserController@activate'
    ]);
    
    Route::post('users/{id}/ban', [
        'as' => 'users.ban',
        'uses' => 'AdminUserController@ban'
    ]);
    
    Route::post('users/{id}/unban', [
        'as' => 'users.unban',
        'uses' => 'AdminUserController@unban'
    ]);
    
    Route::get('users/provide/list', [
        'as' => 'users.provide',
        'uses' => 'AdminUserController@provide'
    ]);
    
    Route::get('users/provide/{id}/edit', [
        'as' => 'users.provide.edit',
        'uses' => 'AdminUserController@provide_edit'
    ]);
    
    Route::post('users/provide/{id}/edit', [
        'as' => 'users.provide.edit',
        'uses' => 'AdminUserController@provide_edit_save'
    ]);
   
});
