<?php

namespace Modules\User\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class MDWeditUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($user = \Sentinel::check())
        {
            if($user->id == $request->segment(2) || $user->inRole('admin'))
            {
                return $next($request);
            }
            else
            {
                if($request->ajax())
                {
                    $data['redirect'] = '/';
                    return response()->json($data, 200);
                }
                else
                {
                    return redirect('/');
                }
            }
        }
        else
        {
            if($request->ajax())
            {
                $data['redirect'] = '/';
                return response()->json($data, 200);
            }
            else
            {
                return redirect('/');
            }
        }
    }
}
