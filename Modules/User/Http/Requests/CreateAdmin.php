<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'last_name'                 => 'required',
            'first_name'                => 'required',
            'email' 	                => 'required|email|unique:users',
	    'password'                  => 'required|min:6|confirmed',
	    'password_confirmation'     => 'required|min:6',
        ];
    }
    
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
     
        $validator->setAttributeNames([
            'last_name'             => '"Фамилия"',
            'first_name'            => '"Имя"',
            'email' 	            => '"Почта"',
	    'password'              => '"Пароль"',
	    'password_confirmation' => '"Повтор пароля"',
        ]);
     
        return $validator;
    }
}
