<?php

namespace Modules\User\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace App\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    public function getRolesArray();
    /*
     *
     */
    public function socialLogin($request, $user);
    /*
     *
     */
    public function socialRegister($request);
}
