<?php

namespace Modules\User\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\User\Repositories\UserRepository;
use Modules\User\Entities\User;
use Modules\User\Entities\UserSocial;
use Modules\User\Events\UserSocialRegisterEvent;
//use App\Validators\UserValidator;

/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /* public function setRolesArray
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getRolesArray()
    {
        $inems = array();
        foreach(\Sentinel::getRoleRepository()->all() as $role)
        {
            $inems[$role->id] = $role->name;
        }
        return $inems;
    }
    /* public function socialLogin($request)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function socialLogin($request, $user)
    {
	$soc = UserSocial::where('email', $request->email)
        ->where('network', $request->network)
        ->where('user_id', $user->id)->first();
	if(!$soc)
	{
	    $soc = UserSocial::create([
		'user_id' 		=> $user->id,
		'manual' 		=> $request->manual,
		'email' 		=> $request->email,
		'phone' 		=> $request->phone,
		'city' 			=> $request->city,
		'network' 		=> $request->network,
		'identity' 		=> $request->identity,
		'photo_big' 		=> $request->photo_big,
		'profile' 		=> $request->profile,
		'last_name' 		=> $request->last_name,
		'uid' 			=> $request->uid,
		'photo' 		=> $request->photo,
		'verified_email' 	=> $request->verified_email,
		'first_name' 		=> $request->first_name,
	    ]);
	    // Save avatar
	//    $avatar = data_get($user, 'avatar.0.pivot.fid');
	//    if(is_null($avatar))
	//    {
	//	$filename = str_random(10);
	//	$img = \Image::make($soc->photo_big);
	//	$img->save(public_path('uploads/avatar/'.$filename.'.jpg'));
	//	$ava = \Filem::create([
	//	    'user_id' 	=> $user->id,
	//	    'original' 	=> $filename.'.jpg',
	//	    'filename' 	=> $filename.'.jpg',
	//	    'uri' 	=> 'avatar',
	//	    'ext' 	=> 'jpg',
	//	    'filemime' 	=> $img->mime(),
	//	    'filesize' 	=> $img->filesize(),
	//	    'status' 	=> 1,
	//	]);
	//	$user->avatar()->attach($user->id, ['fid' => $ava->id]);
	//    }
	}
	return $soc;
    }
    /* public function socialRegister($data);
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function socialRegister($request)
    {
	$credentials = [
            'first_name'    => $request->first_name,
            'last_name'     => $request->last_name,
            'email'    	    => $request->email,
            'password' 	    => str_random(6),
        ];
	
	$user = \Sentinel::registerAndActivate($credentials);
	
	$role = \Sentinel::findRoleById(3); 
        $role->users()->attach($user);
        
        $soc = UserSocial::create([
	    'user_id' 		=> $user->id,
            'manual' 		=> $request->manual,
            'email' 		=> $request->email,
            'phone' 		=> $request->phone,
            'city' 		=> $request->city,
            'network' 		=> $request->network,
            'identity' 		=> $request->identity,
            'photo_big' 	=> $request->photo_big,
            'profile' 		=> $request->profile,
            'last_name' 	=> $request->last_name,
            'uid' 		=> $request->uid,
            'photo' 		=> $request->photo,
            'verified_email' 	=> $request->verified_email,
            'first_name' 	=> $request->first_name,
	]);
	// Save avatar
	//$filename = str_random(10);
	//$img = \Image::make($soc->photo_big);
	//$img->save(public_path('uploads/avatar/'.$filename.'.jpg'));
	//$ava = \Filem::create([
	//    'user_id' 	=> $user->id,
	//    'original' 	=> $filename.'.jpg',
	//    'filename' 	=> $filename.'.jpg',
	//    'uri' 	=> 'avatar',
	//    'ext' 	=> 'jpg',
	//    'filemime' 	=> $img->mime(),
	//    'filesize' 	=> $img->filesize(),
	//    'status' 	=> 1,
	//]);
	//$user->avatar()->attach($user->id, ['fid' => $ava->id]);
	
	event(new UserSocialRegisterEvent($user, $credentials));
	
	return $user;
    }
}
