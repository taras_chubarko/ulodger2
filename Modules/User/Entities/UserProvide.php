<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class UserProvide extends Model
{
    protected $fillable = [];
    
    protected $table = 'users_provide';
    
    protected $guarded = ['_token'];
    
    /* public function card
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user()
    {
        return $this->hasOne('Modules\User\Entities\User', 'id', 'user_id');
    }
    /*
     * Photo
     */
    public function photo()
    {
        return $this->hasOne('Modules\Filem\Entities\Filem', 'id', 'fid');
    }
    /* public function getDateAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFlagAttribute()
    {
        if($this->status == 0)
        {
            return '<span class="text-danger"><i class="fa fa-hourglass-start"></i> Not provide</span>';
        }
        if($this->status == 1)
        {
            return '<span class="text-success"><i class="fa fa-check"></i> Provide</span>';
        }
        
    }
    /* public function getFlag2Attribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFlag2Attribute()
    {
        if($this->status == 0)
        {
            return 'Not provide';
        }
        if($this->status == 1)
        {
            return 'Provide';
        }
    }
}
