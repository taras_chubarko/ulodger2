<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Users\EloquentUser as CartalystUser;
use Cviebrock\EloquentSluggable\Sluggable;

class User extends CartalystUser implements Transformable 
{
    use Sluggable;
    use SoftDeletes;
    use TransformableTrait;
    
    protected $fillable = ['email', 'password', 'first_name', 'last_name',  'permissions'];
    
    protected $table = 'users';
    
    protected $dates = ['deleted_at'];
    
    //protected $attributes = array(
    //    'CountReviews' => '',
    //);
    //
    //protected $appends = ['CountReviews'];
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'fullName'
            ]
        ];
    }
    /* public function getFullNameAttribute
     * @param $id
     *-----------------------------------
     *| полное имя
     *-----------------------------------
     */
    public function getFullNameAttribute()
    {
        $name[] = $this->first_name;
        $name[] = $this->last_name;
        return implode(' ', $name);
    }
    /*
     * Аватарка
     */
    public function avatar()
    {
        return $this->belongsToMany('Modules\Filem\Entities\Filem', 'users_avatar', 'user_id', 'fid')
        ->withPivot('fid');
    }
    /*
     * забаненый пользователь
     */
    public function ban()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_ban')
        ->withTimestamps();
    }
    /*
     * Роли
     */
    public function role()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'role_users')
        ->withPivot('role_id')
        ->leftJoin('roles', 'roles.id', '=', 'role_users.role_id')
        ->select(
            "roles.id as pivot_id",
            "roles.name as pivot_name",
            "roles.slug as pivot_slug",
            "roles.permissions as pivot_permissions"
        );
    }
    /*
     *
     */
    public function isOnline()
    {
        return \Cache::has('user-is-online-' . $this->id);
    }
    /* public function phone
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function phone()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_phone')
        ->withPivot('telcode', 'phone', 'verify');
    }
     /* public function phoneVerify
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function phoneVerify()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_phone')
        ->withPivot('telcode', 'phone', 'verify')->where('verify', 1);
    }
    /* public function phoneNotVerify
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function phoneNotVerify()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_phone')
        ->withPivot('telcode', 'phone', 'verify')->where('verify', 0);
    }
    /* public function name
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function notification()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_notification')
        ->withPivot('notifi');
    }
    /* public function card
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function card()
    {
        return $this->hasMany('Modules\User\Entities\UserCard');
    }
    /* public function payout
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function payout()
    {
        return $this->hasMany('Modules\User\Entities\UserPayout')->orderBy('created_at', 'DESC');
    }
    /* public function privacy
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function privacy()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_privacy')
        ->withPivot('include_in_search_engines');
    }
    /* public function residence
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function residence()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_residence')
        ->withPivot('country');
    }
    /* public function remove
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function remove()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_remove')
        ->withPivot('reason', 'details', 'contact_ok');
    }
    /* public function lang
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function lang()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_lang')
        ->withPivot('lang_id');
    }
    /* public function about
     * @param $id
     *-----------------------------------
     *| users_about
     *-----------------------------------
     */
    public function about()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_about')
        ->withPivot('about');
    }
    /* public function birthdate
     * @param $id
     *-----------------------------------
     *| users_birthdate
     *-----------------------------------
     */
    public function birthdate()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_birthdate')
        ->withPivot('birthdate');
    }
    /* public function emergency
     * @param $id
     *-----------------------------------
     *| users_emergency
     *-----------------------------------
     */
    public function emergency()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_emergency')
        ->withPivot('name', 'phone', 'email', 'relationship');
    }
    /* public function gender
     * @param $id
     *-----------------------------------
     *| users_gender
     *-----------------------------------
     */
    public function gender()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_gender')
        ->withPivot('gender_id');
    }
    /* public function location
     * @param $id
     *-----------------------------------
     *| users_location
     *-----------------------------------
     */
    public function location()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_location')
        ->withPivot('location', 'region', 'address', 'lat', 'lng');
    }
    /* public function school
     * @param $id
     *-----------------------------------
     *| users_school
     *-----------------------------------
     */
    public function school()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_school')
        ->withPivot('school');
    }
    /* public function timezone
     * @param $id
     *-----------------------------------
     *| users_timezone
     *-----------------------------------
     */
    public function timezone()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_timezone')
        ->withPivot('timezone');
    }
    /* public function work
     * @param $id
     *-----------------------------------
     *| users_work
     *-----------------------------------
     */
    public function work()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_work')
        ->withPivot('work');
    }
    /* public function social
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function social()
    {
        return $this->hasMany('Modules\User\Entities\UserSocial');
    }
    /* public function video
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function video()
    {
        return $this->belongsToMany('Modules\Filem\Entities\Filem', 'users_video', 'user_id', 'fid')
        ->withPivot('fid');
    }
    /* public function messagesNew
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function messagesNew()
    {
        return $this->hasMany('Modules\Inbox\Entities\Inbox', 'user_id_to', 'id')->where('read', 0);
    }
    /* public function getUserRolesAttribute
     * @param $id
     *-----------------------------------
     *| Массив ролей пользователя
     *-----------------------------------
     */
    public function getUserRolesAttribute()
    {
        $items = array();
        foreach($this->role as $role)
        {
            $items[] = $role->pivot->name;
        }
        return implode(',', $items);
    }
    /* public function getDateAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFlagAttribute()
    {
        if($this->status == 0)
        {
            return '<span class="text-warning"><i class="fa fa-hourglass-start"></i> Not actively</span>';/*'ptEOzaYeoze1LNpG4RZT' => 'Not actively',*/
        }
        if($this->status == 2)
        {
            return '<span class="text-danger"><i class="fa fa-lock"></i> Blocked</span>'; //'c0MdvE2REYDIIADCGCLb' => 'Blocked',
        }
        return '<span class="text-success"><i class="fa fa-check"></i> Active</span>'; //'0kSkBqsOinKQ4FfoLRBH' => 'Active',
    }
    /* public function name
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getRolesArrAttribute()
    {
        $items = array();
        foreach($this->role as $role)
        {
            $items[$role->pivot->id] = $role->pivot->id;
        }
        return $items;
    }
    /* public function getStatusAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getStatusAttribute()
    {
        $user = $this;
        $activation = \Activation::exists($user);
        $status = 0;
        if($activation == false)
        {
            $status = 0;
        }
        if($activation && $activation->completed == 0)
        {
            $status = 0;
        }
        
        if ($activation = \Activation::completed($user))
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }
        
        if($user->ban->count())
        {
            $status = 2;
        }
        
        return $status;
    }
    /*
     *
     */
    public function scopeRol($query, $id)
    {
      return $query->role()->where('role_id', $id);
    }
    /* public function getFieldCreatedAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldCreatedAttribute()
    {
        $field = new \stdClass;
        $field->created = $this->created_at;
        $field->diff = diffDate($field->created);
        return $field;
    }
    /* public function name
     * @param $id
     *-----------------------------------
     *| Аватар
     *-----------------------------------
     */
    public function getAvaAttribute()
    {
        $data = new \stdClass;
        $avatar = data_get($this, 'avatar.0');
        if($avatar)
        {
            $data->image = $avatar->filename;
        }
        else
        {   
            $name = str_random(10).'.jpg';
            $ava = \Avatar::create($this->fullName)->save(public_path('/uploads/avatar/'.$name), 100);
            $fid = \Filem::create([
                'user_id'   => $this->id,
                'original'  => $name,
                'filename'  => $name,
                'uri'       => 'avatar',
                'ext'       => 'jpg',
                'filemime'  => 'image/jpeg',
                'filesize'  => 1,
                'status'    => 1,
            ]);
            
            $this->avatar()->attach($this->id,['fid' => $fid->id]);
            
            $data->image = 'nouser.png';
        }
        return $data;
    }
    /* public function getFieldlangAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldLangAttribute()
    {
        $field = new \stdClass;
        $field->txt = null;
        $langs = [];
        
        if($this->lang->count())
        {
            foreach($this->lang as $lang)
            {
                $langs[] = config('lang.'.$lang->pivot->lang_id.'.name');
            }
            $field->txt = implode(', ', $langs);
        }
        
        return $field;
    }
    /* public function getFieldVerifyPhoneAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldVerifyPhoneAttribute()
    {
        if($this->phoneVerify->count())
        {
            return 1;
        }
        else
        {
            return null;
        }
    }

    /* public function getAddressAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getAddressAttribute()
    {
        $field = new \stdClass;
        $field->location = data_get($this, 'location.0.pivot.location');
        $field->region = data_get($this, 'location.0.pivot.region');
        $field->address = data_get($this, 'location.0.pivot.address');
        $field->lat = (float) data_get($this, 'location.0.pivot.lat');
        $field->lng = (float) data_get($this, 'location.0.pivot.lng');
        unset($this->location);
        return $field;
    }

    /* public function getFieldAboutAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getFieldAboutAttribute()
    {
        $field = new \stdClass;
        $field->value = data_get($this, 'about.0.pivot.about');
        unset($this->about);
        return $field;
    }

    /* public function provides
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function provides()
    {
        return $this->hasOne(\Modules\User\Entities\UserProvide::class, 'user_id', 'id');
    }
    /* public function getFieldVerifiedAttribute
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getFieldVerifiedAttribute()
    {
        $field = new \stdClass;
        $field->value = ($this->provides && $this->provides->status == 1) ? true : false;
        return $field;
    }

    /* public function getFieldVerifiedEmailAttribute
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getFieldVerifiedsAttribute()
    {
        $field = new \stdClass;
        $field->values = [];
        //
        if($this->verify_email == 1)
        {
            $field->values[] = [
                'name'  => 'Email address',
                'data'  => true,
            ];
        }
        //
        if($this->phoneVerify->count())
        {
            $field->values[] = [
                'name'  => 'Phone number',
                'data'  => true,
            ];
        }
        return $field;
    }

    /* public function getFieldSocialAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getFieldSocialAttribute()
    {
        $field = new \stdClass;
        $field->values = [];

        if($this->social->count())
        {
            foreach ($this->social as $item) {
                $field->values[] = [
                    'id'        => $item->id,
                    'network'   => $item->network,
                ];
            }
        }
        unset($this->social);
        return $field;
    }

}
