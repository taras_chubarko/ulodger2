<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class UserPayout extends Model
{
    protected $fillable = [];
    
    protected $table = 'users_payout';
    
    protected $guarded = ['_token'];
    
    /* public function getDetailsAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getDetailsAttribute()
    {
        $minPayout = '($'.number_format($this->min_payout, 0, '.', '') .' '.$this->currency.')';
        $line = $this->first_name.' '.$this->last_name.' '.$this->country.' '.$minPayout;
        return $line;
    }
    /* public function name
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldStatusAttribute()
    {
        $field = new \stdClass;
        $field->status = $this->status;
        $st[0] = 'warning';
        $st[1] = 'success';
        $field->txt = '<b class="text-'.$st[$field->status].'">'.config('user.payout.status.'.$field->status).'</b>';
        return $field;
    }
    
}
