<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class UserSocial extends Model
{
    protected $fillable = [
        'user_id',
        'manual',
        'email',
        'phone',
        'city',
        'network',
        'identity',
        'photo_big',
        'profile',
        'last_name',
        'uid',
        'photo',
        'verified_email',
        'first_name'
    ];
    
    protected $table = 'users_social';
}
