<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    protected $fillable = [];
    
    protected $table = 'users_card';
    
    protected $guarded = ['_token'];
    
    
}
