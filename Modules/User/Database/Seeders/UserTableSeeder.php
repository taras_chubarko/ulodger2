<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rore_admin = \Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Администратор',
            'slug' => 'admin',
            'permissions' => ['admin' => true]
        ]);
        
        //---------------------------------------------
        $credentials_admin = [
            'email'         => 'admin@admin.ua',
            'password'      => '123456',
            'first_name'    => 'Админ',
            'last_name'     => 'Админыч',
        ];
            
        $admin = \Sentinel::create($credentials_admin);
        
        $rore_admin = \Sentinel::findRoleById(1);
        $rore_admin->users()->attach($admin);
        
        $activation = \Activation::create($admin);
        \Activation::complete($admin, $activation->code);
        
        //
        
        $rore_user = \Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Пользователь',
            'slug' => 'user',
            'permissions' => ['admin' => false]
        ]);
        
        //---------------------------------------------
        $credentials_user = [
            'email'         => 'user@user.ua',
            'password'      => '123456',
            'first_name'    => 'Пользователь',
            'last_name'     => 'Пользователь',
        ];
            
        $user = \Sentinel::create($credentials_user);
        
        $rore_user = \Sentinel::findRoleById(2);
        $rore_user->users()->attach($admin);
        
        $activation = \Activation::create($user);
        \Activation::complete($user, $activation->code);
        
    }
}
