<?php

namespace Modules\User\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $code)
    {
        $this->request  = $request;
        $this->code  = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $request = $this->request;
        $code = $this->code;
        $link = route('verify.email', $code);
        
        return $this->view('site::user.mail-verify-mail', compact('link'))
        ->to($request->email)
        ->subject('Verification of mail "'.config('app.name').'"');
    }
}
