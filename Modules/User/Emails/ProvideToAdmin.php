<?php

namespace Modules\User\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProvideToAdmin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($provide, $user)
    {
        $this->provide = $provide;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $provide = $this->provide;
        $user = $this->user;
        
        return $this->view('site::user.mail-provide-to-admin', compact('provide', 'user'))
        ->to(config('app.admin_mail'))
        ->subject('New provide id');
    }
}
