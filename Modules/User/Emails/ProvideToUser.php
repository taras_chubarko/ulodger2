<?php

namespace Modules\User\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProvideToUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($provide)
    {
        $this->provide = $provide;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $provide = $this->provide;
        
        return $this->view('site::user.mail-provide-to-user', compact('provide'))
        ->to($provide->user->email)
        ->subject('ID status updated.');
    }
}
