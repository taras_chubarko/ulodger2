<?php

namespace Modules\User\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Reminder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $credentials, $reminder, $link)
    {
        $this->user         = $user;
        $this->credentials  = $credentials;
        $this->reminder     = $reminder;
        $this->link         = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user         = $this->user;
        $credentials  = $this->credentials;
        $reminder     = $this->reminder;
        $link         = $this->link;
        
        return $this->view('site::user.mail-reminder', compact('user', 'credentials', 'reminder', 'link'))
        ->to($user->email)
        ->subject('Password recovery "'.config('app.name').'"');
    }
}
