<?php namespace Modules\User\Listeners;

use Illuminate\Http\Request;
use Illuminate\Contracts\Mail\Mailer;
use Modules\User\Emails\RegisterToAdmin;
use Modules\User\Emails\RegisterToUser;
use Modules\User\Emails\Reminder;

class UserEventListener
{
    public $request;
    protected $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request, Mailer $mailer)
    {
        $this->request = $request;
        $this->mailer = $mailer;
    }
    /* public function onSocialRegister
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function userRegister($event)
    {
        $user = $event->user;
        $request = $this->request;
        \Mail::send(new RegisterToAdmin($request, $user));
        \Mail::send(new RegisterToUser($request, $user));
    }
    /* public function reminder
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function reminder($event)
    {
        $user = $event->user;
        $credentials = $event->credentials;
        $reminder    = $event->reminder;
        $link        = $event->link;
        \Mail::send(new Reminder($user, $credentials, $reminder, $link));
    }
    /* public function social
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function social($event)
    {
        $user = $event->user;
        $credentials = $event->credentials;
        $request = new \stdClass;
        $request->password = $credentials['password'];
        \Mail::send(new RegisterToAdmin($request, $user));
        \Mail::send(new RegisterToUser($request, $user));
    }
    /**
     * Регистрация слушателей для подписки.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Modules\User\Events\UserRegisterEvent',
            'Modules\User\Listeners\UserEventListener@userRegister'
        );

        $events->listen(
            'Modules\User\Events\ReminderEvent',
            'Modules\User\Listeners\UserEventListener@reminder'
        );

        $events->listen(
            'Modules\User\Events\UserSocialRegisterEvent',
            'Modules\User\Listeners\UserEventListener@social'
        );

    }

}