<?php

namespace Modules\User\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ExecutorCriteria
 * @package namespace App\Criteria;
 */
class ExecutorCriteria implements CriteriaInterface
{
    /*
     *
     */
    protected $request;
    /*
     *
     */
    public $category;
    /* public function __construct
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct($request, $category)
    {
        $this->request = $request;
        $this->category = $category;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->request->has('orderby'))
        {
            if($this->request->get('orderby') == 'rating')
            {
                $model = $model->leftJoin('users_rating as r', 'users.id', '=', 'r.user_id');
                $model = $model->select('users.*', 'r.rating as rt');
                $model = $model->orderBy('rt', $this->request->get('sortby'));
            }
        }
        else
        {
            $model = $model->leftJoin('users_rating as r', 'users.id', '=', 'r.user_id');
            $model = $model->select('users.*', 'r.rating as rt');
            $model = $model->orderBy('rt', 'DESC');
        }
        
        $model = $model->whereHas('roles', function($query){
            $query->where('role_id', 3);
        });
        
        if(!is_null($this->category))
        {
            $category = \TaxonomyTerm::findBySlug($this->category);//->getDescendantsAndSelf()->toHierarchy();
            if($category)
            {
                $items = [];
                $category = $category->getDescendantsAndSelf()->toHierarchy();
                foreach($category as $cat)
                {
                    if($cat->children->count())
                    {
                        foreach($cat->children as $children)
                        {
                            $items[] = $children->id;
                        }
                    }
                    else
                    {
                        $items[] = $cat->id;
                    }
                }
                
                if($items)
                {
                    $model = $model->whereHas('category', function($query) use ($items){
                        $query->whereIn('category_id', $items);
                    });
                }
            }
            else
            {
                abort(404);
            }
        }
        
        if($this->request->has('job'))
        {
            $users = \User::whereHas('roles', function($query){
                $query->where('role_id', 3);
            })->all();
            $users = $users->pluck('id')->toArray();
            
            $users2 = \User::whereHas('employment', function($query){
                $query->where('status', 0);
            })->all();
            $users2 = $users2->pluck('id')->toArray();
            $result = array_diff($users, $users2);
            $model = $model->whereIn('users.id', $result);
        }
        
        if($this->request->has('status'))
        {
            $users = \User::whereHas('roles', function($query){
                $query->where('role_id', 3);
            })->all();
            $arr = [];
            foreach($users as $user)
            {
                if($user->isOnline())
                {
                    $arr[] = $user->id;
                }
            }
            if($arr)
            {
                $model = $model->whereIn('users.id', $arr);
            }
        }
        //
        if($this->request->has('locality'))
        {
            $model = $model->whereHas('location', function($query){
                $query->where('location', $this->request->input('locality'));
            });
        }
        else
        {
            if(\Setting::get('filter.locality') == 1)
            {
                $model = $model->whereHas('location', function($query){
                    $query->where('location', session('myloc.locality'));
                });
            }
        }
        
        return $model;
    }
}
