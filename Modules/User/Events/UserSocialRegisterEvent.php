<?php

namespace Modules\User\Events;

use Illuminate\Queue\SerializesModels;

class UserSocialRegisterEvent
{
    use SerializesModels;
    
    public $user;
    public $credentials;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $credentials)
    {
        $this->user         = $user;
        $this->credentials  = $credentials;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
