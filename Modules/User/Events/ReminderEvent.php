<?php

namespace Modules\User\Events;

use Illuminate\Queue\SerializesModels;

class ReminderEvent
{
    use SerializesModels;
    
    public $user;
    public $credentials;
    public $reminder;
    public $link;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $credentials, $reminder, $link)
    {
        $this->user         = $user;
        $this->credentials  = $credentials;
        $this->reminder     = $reminder;
        $this->link         = $link;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
