<?php

namespace Modules\Inbox\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Inbox extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'inbox';

    protected $fillable = [];
    
    protected $guarded = ['_token', 'meta'];
    
    //protected $dates = ['deleted_at'];
    
    /* public function user
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function userFrom()
    {
        return $this->hasOne('Modules\User\Entities\User', 'id', 'user_id_from');
    }
    /* public function user
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function userTo()
    {
        return $this->hasOne('Modules\User\Entities\User', 'id', 'user_id_to');
    }
    /* public function host
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function requestId()
    {
        return $this->hasOne('Modules\Host\Entities\HostBook', 'id', 'request_id'); 
    }
}
