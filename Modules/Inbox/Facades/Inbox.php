<?php namespace Modules\Inbox\Facades;

use Illuminate\Support\Facades\Facade;

class Inbox extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Modules\Inbox\Repositories\InboxRepository';
    }
}