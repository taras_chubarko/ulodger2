<?php

namespace Modules\Inbox\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InboxRepository
 * @package namespace App\Repositories;
 */
interface InboxRepository extends RepositoryInterface
{
    /*
     *
     */
    public function getInbox($user, $slug);
    /*
     *
     */
    public function buttonSlug();
    /*
     *
     */
    public function getSlug();
    /*
     *
     */
    public function button();
}
