<?php

namespace Modules\Inbox\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Inbox\Repositories\InboxRepository;
use Modules\Inbox\Entities\Inbox;
use Illuminate\Http\Request;

/**
 * Class InboxRepositoryEloquent
 * @package namespace App\Repositories;
 */
class InboxRepositoryEloquent extends BaseRepository implements InboxRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Inbox::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /* public function getInbox($user)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getInbox($user = null, $slug = null)
    {
        if(!$user)
        {
            $user = \Sentinel::getUser();
        }
        //
        $q = $this->model->orderBy('created_at', 'DESC')->where('user_id_to', $user->id);
        
        if($slug)
        {
            switch($slug)
            {
                case 'starred':
                    $q->where('starred', 1);
                    $q->where('status', 1);
                break;
                
                case 'unread':
                    $q->where('read', 0);
                    $q->where('status', 1);
                break;
                
                case 'reservations':
                    $q->where('type', 'reservations');
                    $q->where('status', 1);
                break;
                
                case 'pending-requests':
                    $q->where('type', 'requests');
                    $q->where('status', 1);
                break;
                
                case 'archived':
                    $q->where('status', 0);
                break;
            }
        }
        $messages = $q->paginate(20);
        
        return $messages;
    }
    /* public function buttonSlug
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function buttonSlug()
    {
        $user = \Sentinel::getUser();
        $mess = $this->model->where('user_id_to', $user->id)->get();
        $inbox = [
	    'all' => [
		'id' => null,
		'slug' => 'all',
		'name' => 'All Messages ('.$mess->count().')',
	    ],
	    'starred' => [
		'id' => 3,
		'slug' => 'starred',
		'name' => 'Starred ('.$mess->where('starred', 1)->where('status', 1)->count().')',
	    ],
	    'unread' => [
		'id' => 0,
		'slug' => 'unread',
		'name' => 'Unread ('.$mess->where('read', 0)->where('status', 1)->count().')',
	    ],
	    'reservations' => [
		'id' => 2,
		'slug' => 'reservations',
		'name' => 'Reservations ('.$mess->where('type', 'reservations')->where('status', 1)->count().')',
	    ],
	    'requests' => [
		'id' => 1,
		'slug' => 'requests',
		'name' => 'Pending Requests ('.$mess->where('type', 'requests')->where('status', 1)->count().')',
	    ],
	    'archived' => [
		'id' => 4,
		'slug' => 'archived',
		'name' => 'Archived ('.$mess->where('status', 0)->count().')',
	    ],
	];
        
        $slug = \Request::route('slug');
        if($slug)
        {
            $curreent = $inbox[$slug]['name'];
        }
        else
        {
            $curreent = $inbox['all']['name'];
        }
        
        return view('site::inbox.button-slug', compact('inbox', 'curreent'));
    }
    /* public function geSlug
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getSlug()
    {
        $user = \Sentinel::getUser();
        $mess = $this->model->where('user_id_to', $user->id)->get();
        $inbox = collect([
	    'all' => [
		'id' => null,
		'slug' => 'all',
		'name' => 'All Messages ('.$mess->count().')',
	    ],
	    'starred' => [
		'id' => 3,
		'slug' => 'starred',
		'name' => 'Starred ('.$mess->where('starred', 1)->where('status', 1)->count().')',
	    ],
	    'unread' => [
		'id' => 0,
		'slug' => 'unread',
		'name' => 'Unread ('.$mess->where('read', 0)->where('status', 1)->count().')',
	    ],
	    'reservations' => [
		'id' => 2,
		'slug' => 'reservations',
		'name' => 'Reservations ('.$mess->where('type', 'reservations')->where('status', 1)->count().')',
	    ],
	    'requests' => [
		'id' => 1,
		'slug' => 'requests',
		'name' => 'Pending Requests ('.$mess->where('type', 'requests')->where('status', 1)->count().')',
	    ],
	    'archived' => [
		'id' => 4,
		'slug' => 'archived',
		'name' => 'Archived ('.$mess->where('status', 0)->count().')',
	    ],
	]);
        return $inbox;
    }
    /* public function button
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function button()
    {
	$user = \Sentinel::getUser();
        $mess = $this->model->where('user_id_to', $user->id)->get();
        $inbox = collect([
	    'all' 		=> 'All Messages ('.$mess->count().')',
	    'starred' 		=> 'Starred ('.$mess->where('starred', 1)->where('status', 1)->count().')',
	    'unread' 		=> 'Unread ('.$mess->where('read', 0)->where('status', 1)->count().')',
	    'reservations' 	=> 'Reservations ('.$mess->where('type', 'reservations')->where('status', 1)->count().')',
	    'requests' 		=> 'Pending Requests ('.$mess->where('type', 'requests')->where('status', 1)->count().')',
	    'archived' 		=> 'Archived ('.$mess->where('status', 0)->count().')',
	]);
        return $inbox;
    }
}
