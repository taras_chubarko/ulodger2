<?php

Route::group(['middleware' => ['web'], 'prefix' => 'inbox', 'namespace' => 'Modules\Inbox\Http\Controllers'], function()
{
    Route::post('get/button', [
        'as'    => 'inbox.get.button',
        'uses' 	=> 'InboxController@inbox_get_button'
    ]);

    Route::post('set/starred/{id}/{star}', [
        'as'    => 'inbox.set.read',
        'uses' 	=> 'InboxController@inbox_set_starred'
    ]);

    Route::post('set/removed/{id}', [
        'as'    => 'inbox.set.removed',
        'uses' 	=> 'InboxController@inbox_set_removed'
    ]);

    Route::post('moveto/{id}/{type}', [
        'as'    => 'inbox.moveto',
        'uses' 	=> 'InboxController@inbox_moveto'
    ]);


});

Route::group(['middleware' => ['web'], 'prefix' => 'api/v1/inbox', 'namespace' => 'Modules\Inbox\Http\Controllers'], function()
{
    Route::post('/contact/message', 'InboxController@contact_message');
    Route::post('/count/new', 'InboxController@count_new');
    Route::get('/{slug?}', 'InboxController@index');
    Route::get('/message/{id}', 'InboxController@inbox_message_id');
    Route::post('/set/read/{id}', 'InboxController@inbox_set_read');
    Route::post('block', 'InboxController@inbox_get_inbox_new');

});
