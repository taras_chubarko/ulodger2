<?php

namespace Modules\Inbox\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookReq extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['check_in']      = 'required';
        $rules['check_out']     = 'required|different:check_in|after:check_in';
        $rules['guests']        = 'required';
        
        return $rules;
    }
    /*
     *
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        
        $rules = [];
        $rules['check_in']      = '"Check in"';
        $rules['check_out']     = '"Check out"';
        $rules['guests']        = '"Lodgers"';
        
        $validator->setAttributeNames($rules);
     
        return $validator;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
