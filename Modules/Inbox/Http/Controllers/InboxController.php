<?php

namespace Modules\Inbox\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Inbox\Repositories\InboxRepository;
use Modules\Host\Entities\HostBook;
use Modules\Inbox\Entities\Inbox;

class InboxController extends Controller
{
    /*
     *
     */
    protected $inbox;
    protected $model;
    /*
     *
     */
    public function __construct(InboxRepository $inbox, Inbox $model)
    {
        $this->inbox = $inbox;
        $this->model = $model;
    }

    /* public function getData
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getData($slug = null, $paginate = 20)
    {
        $user = \Sentinel::getUser();
        $q = Inbox::orderBy('created_at', 'DESC');
        $q->with('userFrom');
        $q->where('user_id_to', $user->id);
        $q->whereHas('userFrom', function($r)
        {
            $r->whereNull('deleted_at');
        });
        //
        if(is_null($slug) || $slug == 'all')
        {
            $q->where('status', 1);
        }
        //
        if($slug == 'starred')
        {
            $q->where('status', 1);
            $q->where('starred', 1);
        }
        //
        if($slug == 'unread')
        {
            $q->where('status', 1);
            $q->where('read', 0);
        }
        //
        if($slug == 'reservations')
        {
            $q->where('status', 1);
            $q->where('type', 'reservations');
        }
        //
        if($slug == 'requests')
        {
            $q->where('status', 1);
            $q->where('type', 'requests');
        }
        //
        if($slug == 'archived')
        {
            $q->where('status', 0);
        }
        $inbox = $q->paginate($paginate);

        $inbox->getCollection()->transform(function ($value) {
            $data = [
                'id'        => $value->id,
                'type'      => $value->type,
                'subject'   => $value->subject,
                'status'    => $value->status,
                'read'      => $value->read,
                'starred'   => $value->starred,
                'created_at' => $value->created_at->format('m/d/Y'),
                'user' => [
                    'id'    => $value->userFrom->id,
                    'name'  => $value->userFrom->first_name,
                ],
            ];
            return $data;
        });

        return $inbox;
    }
    /* public function index
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function index($slug = null)
    {
        $inbox = $this->getData($slug);
        return response()->json($inbox, 200);
    }

    /* public function slug
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function slug($slug)
    {
        //return response()->json($inbox, 200);
    }
    /* public function inbox_get_button
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function inbox_get_button()
    {
        $button = $this->inbox->button();
        return response()->json($button, 200);
    }
    /* public function inbox_message_id
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function inbox_message_id($id)
    {
        $message = $this->inbox->find($id);
        $message->load('userFrom');
        $item = [
            'id'            => $message->id,
            'type'          => $message->type,
            'subject'       => $message->subject,
            'message'       => $message->message,
            'status'        => $message->status,
            'read'          => $message->read,
            'starred'       => $message->starred,
            'created_at'    => $message->created_at->format('m/d/Y'),
            'user'          => [
                'id'    => $message->userFrom->id,
                'name'  => $message->userFrom->first_name,
                'slug'  => $message->userFrom->slug,
            ]
        ];
        return response()->json($item, 200);
    }
    /* public function inbox_set_read
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function inbox_set_read($id)
    {
        $message = $this->inbox->find($id);
        $message->read = 1;
        $message->save();
    }
    /* public function inbox_set_starred
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function inbox_set_starred($id, $star)
    {
        $message = $this->inbox->find($id);
        $message->starred = $star;
        $message->save();
    }
    /* public function inbox_set_removed
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function inbox_set_removed($id)
    {
        $message = $this->inbox->find($id);
        $message->status = 0;
        $message->save();
    }
    /* public function inbox_moveto
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function inbox_moveto($id, $type)
    {
        $message = $this->inbox->find($id);
        $message->type = $type;
        $message->save();
    }
    /* public function inbox_get_inbox
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function inbox_get_inbox()
    {
        $user = \Sentinel::getUser();
        $q = Inbox::orderBy('created_at', 'DESC');
        $q->where('user_id_to', $user->id);
        $q->where('status', 1);
        $q->where('read', 0);
        $count = $q->count();
        return response()->json($count, 200);
    }

    /* public function inbox_get_inbox_new
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function inbox_get_inbox_new()
    {
        $inbox = $this->getData();
        return response()->json($inbox, 200);
    }

    /* public function contact_message
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function contact_message(Request $request)
    {
        $user = \Sentinel::getUser();

        $this->model->create([
            'user_id_from'  => $user->id,
            'user_id_to'    => $request->user_id_to,
            'type'          => 'message',//'requests',
            'subject'       => '<span>New Message</span> form listing contact.',
            'message'       => $request->message,
            'status'        => 1,
            'parent_id'     => 0,
            'read'          => 0,
            'request_id'    => null,//$bookRequest->id,
        ]);
        //
        $result['success']     = 'Message sent.';

        return response()->json($result, 200);
    }

    /* public function count_new
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function count_new()
    {
        $user = \Sentinel::getUser();
        $count = $this->model->where('user_id_to', $user->id)->where('read', 0)->count();
        return $count;
    }

}
