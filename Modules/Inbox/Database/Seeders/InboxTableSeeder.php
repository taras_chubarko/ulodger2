<?php

namespace Modules\Inbox\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class InboxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('en_US');
        foreach(range(1, 100) as $i)
        {
            
            $dt = $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = date_default_timezone_get());
            $dt1 = $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = date_default_timezone_get());
            $dt2 = $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = date_default_timezone_get());
            \Inbox::create([
                'type'          => 'requests',
                'user_id_from'  => $faker->numberBetween(1,10),
                'user_id_to'    => 2,
                'subject'       => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'message'       => $faker->text(500),
                //'host_id'       => $faker->numberBetween(40,100),
                //'check_in'      => \Carbon::parse($dt->format('Y-m-d H:i:s')),
                //'check_out'     => \Carbon::parse($dt1->format('Y-m-d H:i:s')),
                //'guests'        => $faker->numberBetween(1,7),
                'status'        => $faker->numberBetween(0,1),
                'parent_id'     => 0,
                'read'          => 0,
                'starred'       => 0,
                'created_at'    => \Carbon::parse($dt2->format('Y-m-d H:i:s')),
            ]);
        }
        
        
    }
}
