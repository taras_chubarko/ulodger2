<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInboxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inbox', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id_from')->unsigned()->nullable();
            $table->foreign('user_id_from')->references('id')->on('users')->onDelete('cascade');
            $table->integer('user_id_to')->unsigned()->nullable();
            $table->foreign('user_id_to')->references('id')->on('users')->onDelete('cascade');
            $table->string('subject')->nullable();
            $table->text('message')->nullable();
            $table->date('reservation_from')->nullable();
            $table->date('reservation_to')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inbox');
    }
}
