<?php

namespace Modules\Geo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface GeoRepository
 * @package namespace App\Repositories;
 */
interface GeoRepository extends RepositoryInterface
{
    /*
     *
     */
    public function getCountryNameArr();
    /*
     *
     */
    public function getTelCodeArr();
    /*
     *
     */
    public function getTelArr();
}
