<?php

namespace Modules\Geo\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Geo\Repositories\GeoRepository;
use Modules\Geo\Entities\Geo;
use Modules\Geo\Entities\GeoCity;
use Modules\Geo\Entities\GeoCountry;
use Modules\Geo\Entities\GeoRegion;
use Modules\Geo\Entities\GeoTel;

/**
 * Class GeoRepositoryEloquent
 * @package namespace App\Repositories;
 */
class GeoRepositoryEloquent extends BaseRepository implements GeoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Geo::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
    /* public function getCountryNameArr();
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCountryNameArr()
    {
        $country = GeoCountry::all();
        $items = [];
        foreach($country as $item)
        {
            $items[$item->name_en] = $item->name_en;
        }
        return $items;
    }
    /* public function getTelCodeArr
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getTelCodeArr()
    {
        $country = GeoTel::all();
        $items = [];
        foreach($country as $item)
        {
            $items[$item->code] = $item->country;
        }
        return $items;
    }
    /* public function getTelArr
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getTelArr()
    {
        $country = GeoTel::all();
        $items = [];
        foreach($country as $item)
        {
            $items[$item->code] = '+'.$item->code;
        }
        return $items;
    }
    
}
