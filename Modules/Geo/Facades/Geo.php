<?php namespace Modules\Geo\Facades;

use Illuminate\Support\Facades\Facade;

class Geo extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Modules\Geo\Repositories\GeoRepository';
    }
}