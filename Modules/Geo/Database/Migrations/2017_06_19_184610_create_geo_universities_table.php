<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_universities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alpha_two_code')->nullable();
            $table->string('name')->nullable();
            $table->string('country')->nullable();
            $table->string('web_page')->nullable();
            $table->string('domain')->nullable();
            $table->string('address')->nullable();
            $table->string('locality')->nullable();
            $table->string('region')->nullable();
            $table->decimal('lat', 10,6)->nullable();
            $table->decimal('lng', 10,6)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_universities');
    }
}
