<?php

namespace Modules\Geo\Entities;

use Illuminate\Database\Eloquent\Model;

class GeoTel extends Model
{
    protected $fillable = [];
    
    protected $table = 'geo_tel';
    
    protected $guarded = ['_token'];
}
