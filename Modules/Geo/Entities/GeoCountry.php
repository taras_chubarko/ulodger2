<?php

namespace Modules\Geo\Entities;

use Illuminate\Database\Eloquent\Model;

class GeoCountry extends Model
{
    protected $fillable = [];
    
    protected $table = 'geo_country';
    
    protected $guarded = ['_token'];
}
