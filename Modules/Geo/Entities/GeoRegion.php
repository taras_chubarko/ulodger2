<?php

namespace Modules\Geo\Entities;

use Illuminate\Database\Eloquent\Model;

class GeoRegion extends Model
{
    protected $fillable = [];
    
    protected $table = 'geo_region';
    
    protected $guarded = ['_token'];
}
