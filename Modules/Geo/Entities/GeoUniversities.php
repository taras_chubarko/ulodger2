<?php

namespace Modules\Geo\Entities;

use Illuminate\Database\Eloquent\Model;

class GeoUniversities extends Model
{
    protected $fillable = [];
    
    protected $table = 'geo_universities';
    
    protected $guarded = ['_token'];
}
