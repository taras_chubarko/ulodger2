<?php

namespace Modules\Geo\Entities;

use Illuminate\Database\Eloquent\Model;

class GeoCity extends Model
{
    protected $fillable = [];
    
    protected $table = 'geo_city';
    
    protected $guarded = ['_token'];
}
