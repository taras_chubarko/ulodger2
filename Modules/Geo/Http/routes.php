<?php

Route::group(['middleware' => 'web', 'prefix' => 'geo', 'namespace' => 'Modules\Geo\Http\Controllers'], function()
{
    Route::get('/', 'GeoController@index');
});
