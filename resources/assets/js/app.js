
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


window.Vue = require('vue');
window.ResponsiveBootstrapToolkit = require('responsive-toolkit');

//import * as VueGoogleMaps from 'vue2-google-maps';
//Vue.use(VueGoogleMaps, {
//    load: {
//        key: 'AIzaSyA-D9ab5aN_5JZGP05u5izNtgvS0c-gezI',
//        libraries: 'places',
//    }
//});

window.selectpicker = require("bootstrap-select");
window.datetimepicker = require("eonasdan-bootstrap-datetimepicker");
window.geocomplete = require("geocomplete");
window.nicescroll = require("nicescroll");


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('login', require('./components/Login.vue'));
//Vue.component('reminer', require('./components/user/Reminder.vue'));
//Vue.component('in-cart', require('./components/inCart.vue'));
Vue.component('headers', require('./components/layouts/Header.vue'));
Vue.component('footers', require('./components/layouts/Footer.vue'));
Vue.component('block-user', require('./components/user/BlockUser.vue'));
Vue.component('menu-dashboard', require('./components/user/MenuDashboard.vue'));
Vue.component('menu-account', require('./components/user/MenuAccount.vue'));
Vue.component('menu-profile', require('./components/user/MenuProfile.vue'));
Vue.component('menu-listing', require('./components/user/MenuListing.vue'));
Vue.component('menu-my-page', require('./components/user/MenuMyPage.vue'));
Vue.component('menu-invoice', require('./components/invoice/MenuInvoice.vue'));
Vue.component('block-photo-profile', require('./components/user/BlockPhotoInProfile.vue'));
Vue.component('loading', require('./components/other/Loading.vue'));
Vue.component('soc', require('./components/other/Soc.vue'));

Vue.component('find-map', {
  template: '<div id="find-map"></div>'
})
   

import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';
import VueSweetAlert from 'vue-sweetalert';
import VueProgressBar from 'vue-progressbar';
import VueScrollTo from 'vue-scrollto';
//import vmodal from 'vue-js-modal';
import Meta from 'vue-meta';
//import VueTruncate from 'vue-truncate-filter';
import VN  from 'vue-notification';

//import VueStrap from 'vue-strap';
import Auth from './packages/auth/Auth.js';
import Msg from './packages/validate/Msg.js';
import VuePaginate from 'vue-paginate';


//import Modal from 'vue-bootstrap-modal';


//Vue.use(VeeValidate);
Vue.use(VueSweetAlert);
Vue.use(VueRouter);
Vue.use(VueScrollTo);
//Vue.use(vmodal);
Vue.use(Meta);
//Vue.use(VueTruncate);
Vue.use(VN);
Vue.use(Auth);
Vue.use(Msg);
//Vue.use(Modal);
import VueSession from 'vue-session';
Vue.use(VueSession);

import Storage from 'vue-web-storage';
Vue.use(Storage);

import ValidForm from './packages/validate/ValidForm';
Vue.use(ValidForm);

//Vue.use(VueStrap);
const moment = require('moment');
//require('moment/locale/ru');
Vue.use(require('vue-moment'), {
    moment
});

//Vue.use(require('vue-moment'));

Vue.use(VeeValidate, {
  inject: false
});


export default {
  provideValidator: true,
  inject: ['$validator']
}

Vue.use(VueProgressBar, {
  color: 'rgb(47,212,85)',
  failedColor: 'red',
  height: '5px'
});

Vue.use(VuePaginate);

import VueSimpleBreakpoints from 'vue-simple-breakpoints';
Vue.use(VueSimpleBreakpoints);


const Front = Vue.extend(require('./components/Front.vue'));
const PageAbout = Vue.extend(require('./components/pages/PageAbout.vue'));
const PageContact = Vue.extend(require('./components/pages/PageContact.vue'));
const PageOperations = Vue.extend(require('./components/pages/PageOperations.vue'));
const PageHosts = Vue.extend(require('./components/pages/PageHost.vue'));
const PageLogin = Vue.extend(require('./components/user/PageLogin.vue'));
const PageDashboard = Vue.extend(require('./components/user/PageDashboard.vue'));
const PageCreateAccount = Vue.extend(require('./components/user/PageCreateAccount.vue'));
const PageForgotPassword = Vue.extend(require('./components/user/PageForgotPassword.vue'));
const PageReminder = Vue.extend(require('./components/user/PageReminder.vue'));
const PageAccount = Vue.extend(require('./components/user/PageAccount.vue'));
const PageProfile = Vue.extend(require('./components/user/PageProfile.vue'));
const PagePhotos = Vue.extend(require('./components/user/PagePhotos.vue'));
const PageVerification = Vue.extend(require('./components/user/PageVerification.vue'));
const PageReviews = Vue.extend(require('./components/user/PageReviews.vue'));
const PageReferences = Vue.extend(require('./components/user/PageReferences.vue'));
const PageSecurity = Vue.extend(require('./components/user/PageSecurity.vue'));
const PageSettings = Vue.extend(require('./components/user/PageSettings.vue'));
const PageUser = Vue.extend(require('./components/user/PageUser.vue'));
const PageYourReservations = Vue.extend(require('./components/user/PageMyReservations.vue'));
const PageReservationRequests = Vue.extend(require('./components/user/PageReservationRequests.vue'));
const Logout = Vue.extend(require('./components/user/Logout.vue'));
// user my page
const PageMy = Vue.extend(require('./components/user/PageMy.vue'));

const PageYourListing = Vue.extend(require('./components/user/PageYourListing.vue'));
const PageAccountPaymentMetods = Vue.extend(require('./components/user/PageAccountPaymentMetods.vue'));
const PageAccountPrivacy = Vue.extend(require('./components/user/PageAccountPrivacy.vue'));
const PageFindHost = Vue.extend(require('./components/host/PageFindHost.vue'));
const PageHost = Vue.extend(require('./components/host/PageHost.vue'));
const PageInbox = Vue.extend(require('./components/inbox/PageInbox.vue'));
const PageInboxMessage = Vue.extend(require('./components/inbox/PageInboxMessage.vue'));

const AddStep1 = Vue.extend(require('./components/host/add/AddStep1.vue'));
const AddStep2 = Vue.extend(require('./components/host/add/AddStep2.vue'));
const AddStep3 = Vue.extend(require('./components/host/add/AddStep3.vue'));
const AddStep4 = Vue.extend(require('./components/host/add/AddStep4.vue'));
const AddStep5 = Vue.extend(require('./components/host/add/AddStep5.vue'));
const AddStep6 = Vue.extend(require('./components/host/add/AddStep6.vue'));
const AddStep7 = Vue.extend(require('./components/host/add/AddStep7.vue'));
const Finish = Vue.extend(require('./components/host/add/Finish.vue'));
const Updated = Vue.extend(require('./components/host/add/Updated.vue'));

const  router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: Front },
        { path: '/about', component: PageAbout },
        { path: '/contact', component: PageContact },
        { path: '/operations', component: PageOperations },
        { path: '/host', component: PageHosts },
        { path: '/user/:slug', name: 'user', component: PageUser },
        {
            path: '/login',
            component: PageLogin,
            meta: {
                forGuests: true
            }
        },
        {
            path: '/logout',
            component: Logout,
        },
        {
            path: '/sign-up',
            component: PageCreateAccount,
            meta: {
                forGuests: true
            }
        },
        {
            path: '/forgot-password',
            component: PageForgotPassword,
            meta: {
                forGuests: true
            }
        },
        {
            path: '/reminder/:code',
            name: 'reminder',
            component: PageReminder,
            meta: {
                forGuests: true
            }
        },
        {
            path: '/dashboard',
            component: PageDashboard,
            meta: {
                forUser: true
            }
        },
        {
            path: '/payments',
            component: Vue.extend(require('./components/invoice/InvoiceMe.vue')),
            meta: {
                forUser: true
            }
        },
        {
            path: '/payments/my',
            component: Vue.extend(require('./components/invoice/InvoiceMy.vue')),
            meta: {
                forUser: true
            }
        },
        {
            path: '/pay',
            name: 'pay',
            component: Vue.extend(require('./components/invoice/PagePay.vue')),
            meta: {
                forUser: true
            }
        },
        {
            path: '/pay/success',
            name: 'pay.success',
            component: Vue.extend(require('./components/invoice/PagePaySuccess.vue')),
            meta: {
                forUser: true
            }
        },
        {
            path: '/pay/fail',
            name: 'pay.fail',
            component: Vue.extend(require('./components/invoice/PagePayFail.vue')),
            meta: {
                forUser: true
            }
        },
        {
            path: '/invoice/:order_id',
            name: 'invoice',
            component: Vue.extend(require('./components/invoice/PageInvioce.vue')),
            meta: {
                forUser: true
            }
        },
        {
            path: '/inbox',
            component: PageInbox,
            meta: {
                forUser: true
            }
        },
        {
            path: '/inbox/:slug',
            component: PageInbox,
            meta: {
                forUser: true
            }
        },
        {
            path: '/inbox/message/:id',
            component: PageInboxMessage,
            name: 'message',
            meta: {
                forUser: true
            }
        },
        {
            path: '/my-listing',
            component: PageYourListing,
            meta: {
                forUser: true
            }
        },
        {
            path: '/my-listing/reservations',
            component: PageYourReservations,
            meta: {
                forUser: true
            }
        },
        {
            path: '/my-listing/requests',
            component: PageReservationRequests,
            meta: {
                forUser: true
            }
        },
        {
            path: '/my-page',
            component: PageMy,
            meta: {
                forUser: true
            }
        },
        {
            path: '/my-page/verification',
            component: PageVerification,
            meta: {
                forUser: true
            }
        },
        
        {
            path: '/my-page/reviews',
            component: PageReviews,
            meta: {
                forUser: true
            }
        },
        
        {
            path: '/my-page/settings',
            component: PageSettings,
            meta: {
                forUser: true
            }
        },
        
        {
            path: '/find/hosts',
            component: PageFindHost,
            name: 'find',
        },
        {
            path: '/host/:slug',
            component: PageHost,
            name: 'host',
        },

        {
            path: '/sw/:id',
            component: Vue.extend(require('./components/user/PageSW.vue')),
            name: 'sw',
        },
        
        { path: '/add/host', component: AddStep1 },
        { path: '/add/host/step-2', component: AddStep2 },
        { path: '/add/host/step-3', component: AddStep3 },
        { path: '/add/host/step-4', component: AddStep4 },
        { path: '/add/host/step-5', component: AddStep5 },
        { path: '/add/host/step-6', component: AddStep6 },
        { path: '/add/host/step-7', component: AddStep7 },
        { path: '/add/host/finish', component: Finish },
        
        { path: '/edit/host/:slug/step-1', name: 'edit_host1', component: AddStep1 },
        { path: '/edit/host/:slug/step-2', name: 'edit_host2', component: AddStep2 },
        { path: '/edit/host/:slug/step-3', name: 'edit_host3', component: AddStep3 },
        { path: '/edit/host/:slug/step-4', name: 'edit_host4', component: AddStep4 },
        { path: '/edit/host/:slug/step-5', name: 'edit_host5', component: AddStep5 },
        { path: '/edit/host/:slug/step-6', name: 'edit_host6', component: AddStep6 },
        { path: '/edit/host/:slug/step-7', name: 'edit_host7', component: AddStep7 },
        { path: '/edit/host/:slug/updated', name: 'edit_host8', component: Updated },

        { path: '/create/listing', component: Vue.extend(require('./components/listing/PageCreateListing.vue')) },
        { path: '/listing/find', name: 'listing.find', component: Vue.extend(require('./components/listing/PageFindListing.vue')) },
        { path: '/edit/listing/:slug', name: 'edit.listing', component: Vue.extend(require('./components/listing/PageEditListing.vue')) },
        { path: '/listing/:slug', name: 'listing', component: Vue.extend(require('./components/listing/PageListingSlug.vue')) },
        { path: '/policy', name: 'policy', component: Vue.extend(require('./components/pages/PagePolicy.vue')) },
        { path: '/terms', name: 'terms', component: Vue.extend(require('./components/pages/PageTerms.vue')) },
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.forGuests)) {
        if (Vue.auth.isAuth()) {
            next({
                path: '/dashboard'
            });
        }
        else
        {
            next();
        }
    }
    else if (to.matched.some(record => record.meta.forUser)) {
        if (!Vue.auth.isAuth()) {
            next({
                path: '/login'
            });
        }
        else
        {
            next();
        }
    }
    else
    {
        next();
    }
});

require('./filters');

Vue.prototype.$slug = {
    set(text)
    {
        const a = 'àáäâèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
        const b = 'aaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
        const p = new RegExp(a.split('').join('|'), 'g')

        return text.toString().toLowerCase()
                .replace(/\s+/g, '-')           // Replace spaces with -
                .replace(p, c => b.charAt(a.indexOf(c)))     // Replace special chars
    .replace(/&/g, '-and-')         // Replace & with 'and'
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '')             // Trim - from end of text
    }
}

import { EventBus } from './components/event-bus';

Vue.prototype.$responsive = {
    init(){
        var item = 'lg';
        if(Vue.breakpoints.isMobile()) {
            item = 'xs';
        }
        if(Vue.breakpoints.isTablet()) {
            item = 'sm';
        }
        if(Vue.breakpoints.isSmallDesktop()) {
            item = 'md';
        }
        if(Vue.breakpoints.isLargeDesktop()) {
            item = 'lg';
        }
        $(window).resize(function () {
            if(Vue.breakpoints.isMobile()) {
                item = 'xs';
            }
            if(Vue.breakpoints.isTablet()) {
                item = 'sm';
            }
            if(Vue.breakpoints.isSmallDesktop()) {
                item = 'md';
            }
            if(Vue.breakpoints.isLargeDesktop()) {
                item = 'lg';
            }
        });
        return item;
    }
}



const app = new Vue({
    el: '#app',
    router:router,
    data: {
        sitename: 'Ulodger',
    },
    methods: {
        //
    },
    mounted: function()
    {

    }
});

