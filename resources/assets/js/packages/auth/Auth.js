export default function (Vue)
{
    Vue.auth = {
        setToken(cartalyst_sentinel, user, expiration) {
            localStorage.setItem('cartalyst_sentinel', cartalyst_sentinel)
            localStorage.setItem('user', user)
            localStorage.setItem('expiration', expiration)
        },
        //
        getToken() {
            var cartalyst_sentinel = localStorage.getItem('cartalyst_sentinel');
            var expiration = localStorage.getItem('expiration');
            //
            if (!cartalyst_sentinel || !expiration ) {
                return null;
            }
            //
            var ts = Math.round((new Date()).getTime() / 1000);
            if (ts > parseInt(expiration)) {
                this.destroyToken();
                return null;
            }
            else
            {
                return cartalyst_sentinel;
            }
            
        },
        //
        destroyToken() {
            localStorage.removeItem('cartalyst_sentinel');
            localStorage.removeItem('user');
            localStorage.removeItem('expiration');
            
            localStorage.removeItem('step1');
            localStorage.removeItem('step2');
            localStorage.removeItem('step3');
            localStorage.removeItem('step4');
            localStorage.removeItem('step5');
            localStorage.removeItem('step6');
            localStorage.removeItem('step7');
            localStorage.removeItem('host');
            localStorage.removeItem('currentTab');
                        
            window.location.href = '/logout';
        },
        //
        isAuth() {
            if (this.getToken()) {
                return true;
            }
            else{
                return false;
            }
        },
        getUser()
        {
            if (this.getToken()) {
                return JSON.parse(localStorage.getItem('user'));
            }
            else{
                return false;
            }
        },
        logout()
        {
            localStorage.removeItem('cartalyst_sentinel');
            localStorage.removeItem('user');
            localStorage.removeItem('expiration');
        }
    }
    
    Object.defineProperties(Vue.prototype, {
        $auth: {
            get() {
                return Vue.auth
            }
        }
    });
}