/*
 *
 */
Vue.filter('reviews', function(rating) {
    var reviews = 0;
    if (rating.length) {
        reviews = rating[0].pivot.reviews;
    }
    return reviews;
});
/*
 *
 */
Vue.filter('rating', function(rating) {
    var ratings = 0;
    if (rating.length) {
        ratings = rating[0].pivot.rating;
    }
    return ratings;
});
/*
 *
 */
Vue.filter('accuracy', function(rating) {
    var accuracy = 0;
    if (rating.length) {
        accuracy = rating[0].pivot.accuracy;
    }
    return accuracy;
});
/*
 *
 */
Vue.filter('communication', function(rating) {
    var communication = 0;
    if (rating.length) {
        communication = rating[0].pivot.communication;
    }
    return communication;
});
/*
 *
 */
Vue.filter('cleanliness', function(rating) {
    var cleanliness = 0;
    if (rating.length) {
        cleanliness = rating[0].pivot.cleanliness;
    }
    return cleanliness;
});
/*
 *
 */
Vue.filter('location', function(rating) {
    var location = 0;
    if (rating.length) {
        location = rating[0].pivot.location;
    }
    return location;
});
/*
 *
 */
Vue.filter('check_in', function(rating) {
    var check_in = 0;
    if (rating.length) {
        check_in = rating[0].pivot.check_in;
    }
    return check_in;
});
/*
 *
 */
Vue.filter('value', function(rating) {
    var value = 0;
    if (rating.length) {
        value = rating[0].pivot.value;
    }
    return value;
});
/*
 *
 */
Vue.filter('bathrooms', function(bathrooms) {
    var item = 0;
    if (bathrooms.length) {
        item = bathrooms[0].pivot.bathrooms.replace(".00", "");
    }
    return item;
});
/*
 *
 */
Vue.filter('bathType', function(bathType) {
    var item = '- none -';
    var arr = {
        1: 'Private bath',
        2: 'Shared bath',
    };
    if (bathType.length) {
        item = arr[bathType[0].pivot.bath_type];
    }
    return item;
});
/*
 *
 */
Vue.filter('smoking', function(smoking) {
    var item = '- none -';
    var arr = {
        1: 'Male',
        2: 'Female',
        3: 'Either is fine',
    };
    if (smoking.length) {
        item = arr[smoking[0].pivot.smoking];
    }
    return item;
});
/*
 *
 */
Vue.filter('price', function(price) { 
    var item = 0;
    if (price.length > 0) {
        item = price[0].pivot.price.replace(".00", "");
    }
    return item;
});
/*
 *
 */
Vue.filter('truncate', function(string, value) {
    return string.substring(0, 200) + '...';
});
/*
 *
 */
Vue.filter('process', function(process) { 
    var item = process.replace(".00", "");
    if (process > 100) {
        item = 100;
    }
    return item;
});
/*
 *
 */
Vue.filter('img', function(image) {
    if (image.length)
    {
        return '/filem/photo/400x320/'+image[0].filename;
    }
    else
    {
        return '/filem/photo/400x320/noim-min.png';
    }
});
/*
 *
 */
Vue.filter('image', function(image) {
    if (image.length)
    {
        return '/filem/photo/300x200/'+image[0].filename;
    }
    else
    {
        return '/filem/photo/300x200/noim-min.png';
    }
});
/*
 *
 */
Vue.filter('username', function(username) {
    if (username.last_name)
    {
        return username.first_name+' '+username.last_name;
    }
    else
    {
        return username.first_name
    }
});

