const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/themes/site/assets/js')
   .sass('resources/assets/sass/app.scss', 'public/themes/site/assets/css').options({processCssUrls: false});
   
   
mix.styles([
    'public/themes/site/assets/css/all/style.css',
    'public/themes/site/assets/css/all/helper.css',
    'public/themes/site/assets/css/all/font-awesome.min.css',
    'public/themes/site/assets/css/all/material-design-iconic-font.min.css',
    'public/themes/site/assets/css/all/ionicons.min.css',
    'public/themes/site/assets/css/all/stylesheet.css',
    'public/themes/site/assets/css/all/flaticon.css',
    'public/themes/site/assets/css/all/hotels.css',
    'node_modules/bootstrap-slider/dist/css/bootstrap-slider.min.css',
    'node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
    'node_modules/bootstrap-select/dist/css/bootstrap-select.min.css'
],  'public/themes/site/assets/css/all.css').options({processCssUrls: false});

mix.scripts([
    'node_modules/gmaps/gmaps.min.js',
    'public/themes/lib/jquery.dump.js'
], 'public/themes/site/assets/js/all.js').options({processCssUrls: false});

mix.js('resources/assets/js/adminApp.js', 'public/themes/admin/assets/js')
   .sass('resources/assets/sass/adminApp.scss', 'public/themes/admin/assets/css').options({processCssUrls: false});


