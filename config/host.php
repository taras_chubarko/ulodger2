<?php

return [
    'name' => 'Host',
    'smoking' => [
        1 => 'Male',
        2 => 'Female',
        3 => 'Either is fine',
    ],
    'guests' => [
        1 => '1 lodger',
        2 => '2 lodgers',
        3 => '3 lodgers',
        4 => '4 lodgers',
        5 => '5 lodgers',
        6 => '6 lodgers',
        7 => '7 lodgers',
        8 => '8 lodgers',
        9 => '9 lodgers',
        10 => '10 lodgers',
    ],
    'bedrooms' => [
        1 => 'Studio',
        2 => '1 bedroom',
        3 => '2 bedrooms',
        4 => '3 bedrooms',
        5 => '4 bedrooms',
        6 => '5 bedrooms',
        7 => '6 bedrooms',
        8 => '7 bedrooms',
        9 => '8 bedrooms',
        10 => '9 bedrooms',
        11 => '10 bedrooms',
    ],
    'beds' => [
        1 => '1 bed',
        2 => '2 beds',
        3 => '3 beds',
        4 => '4 beds',
        5 => '5 beds',
        6 => '6 beds',
        7 => '7 beds',
        8 => '8 beds',
        9 => '9 beds',
        10 => '10 beds',
    ],
    'bathrooms' => [
        1 => '1 bathroom',
        2 => '1.5 bathrooms',
        3 => '2 bathrooms',
        4 => '2.5 bathrooms',
        5 => '3 bathrooms',
        6 => '3.5 bathrooms',
        7 => '4 bathrooms',
        8 => '4.5 bathrooms',
        9 => '5 bathrooms',
        10 => '5.5 bathrooms',
        11 => '6 bathrooms',
    ],
    'bath_type' => [
        1 => 'Private bath',
        2 => 'Shared bath',
    ],
    'relations' => [
        'all' => [
            'rentOut',
            'propertyType',
            'description',
            'smoking',
            'guests',
            'bedrooms',
            'beds',
            'bathType',
            'bathrooms',
            'location',
            'schoolslocaton',
            'amenities',
            'spaces',
            'photo',
            'available',
            'price',
            'user',
            'rating',
        ],
    ],
    'book' => [
        'status' => [
            0 => 'New',
            1 => 'Booked',
            2 => 'Complete',
        ]
    ],
    
];
