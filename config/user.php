<?php

return [
    'name' => 'User',
    'provide' => [
        0 => 'New',
        1 => 'Complete',
        2 => 'Cancel',
    ],
];
