<?php

return [
    'name' => 'Invoice',
    'status' => [
        0 => 'pending', // ожидает оплаты
        1 => 'paid', // оплачен
        2 => 'canceled', // отменен
        3 => 'overdue' // счет просрочен
    ],
    'commission' => true, // true - on, false - off
    'commission_persent' => 0.1, // 10%
    'commission_type' => 'per_period', // per_month, per_period
];
