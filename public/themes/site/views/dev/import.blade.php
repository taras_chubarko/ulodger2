@extends('site::layouts.master')

@section('content')
 <div class="container">
    {!! Form::open(array('route' => 'import', 'role' => 'form', 'class' => 'form')) !!}
        {!! Form::text('id', null, array('class' => 'form-control')) !!}
        {!! Form::textarea('data', null, array('class' => 'form-control', 'rows' => 10)) !!}
        {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
    {!! Form::close() !!}
 </div>
@stop

@section('script')

@stop
