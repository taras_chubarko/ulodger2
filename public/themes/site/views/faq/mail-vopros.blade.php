@extends('site::mail.main')

@section('title')
Новый вопрос #{{ $faq->id }}
@stop


@section('content')
    <p><b>ФИО:</b> {{ $faq->name }}</p>
    <p><b>E-mail:</b> {{ $faq->email }}</p>
    <p><b>Вопрос:</b> {{ $faq->vopros }}</p>
@stop