@extends('site::mail.main')

@section('title')
Ответ на вопрос #{{ $faq->id }}
@stop


@section('content')
    <p><b>Вопрос:</b> {{ $faq->vopros }}</p>
    <p><b>Ответ:</b> {{ $faq->otvet }}</p>
@stop