@extends('site::mail.main')

@section('title')
Заказ № {{ $order->id }}
@stop

@section('content')
    <p><b>ФИО:</b> {{ $request->fio }}</p>
    <p><b>Телефон:</b> {{ $request->telefon }}</p>
    <p><b>E-mail:</b> {{ $request->email }}</p>
    <p><b>Комментарий к заказу:</b> {{ $request->comment }}</p>
    
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
        <thead>
            <tr>
                <th style="text-align: left; border: 1px solid #ccc; padding:5px 10px;">Наименование товара</th>
                <th style="text-align: center; border: 1px solid #ccc; padding:5px 10px;">Цена, шт</th>
                <th style="text-align: center; border: 1px solid #ccc; padding:5px 10px;">Кол-во</th>
                <th style="text-align: center; border: 1px solid #ccc; padding:5px 10px;">Стимость</th>
            </tr>
        </thead>
        <tbody>
            @foreach($order->items as $item)
            <tr>
                <td style="text-align: left; border: 1px solid #ccc; padding:5px 10px;"><a href="{{ config('app.url') }}/part/{{ $item->slug }}">{{ $item->pivot->name }}</a></td>
                <td style="text-align: center; border: 1px solid #ccc; padding:5px 10px;">{{ $item->pivot->price }}</td>
                <td style="text-align: center; border: 1px solid #ccc; padding:5px 10px;">{{ $item->pivot->qty }}</td>
                <td style="text-align: center; border: 1px solid #ccc; padding:5px 10px;">{{ $item->pivot->subtotal }}</td>
            </tr>
            @endforeach
            <tr>
                <td style="text-align: left; border: 1px solid #ccc; padding:5px 10px;" colspan="3"><b>ИТОГО:</b></td>
                <td style="text-align: center; border: 1px solid #ccc; padding:5px 10px;"><b>{{ $order->subtotal }}</b></td>
            </tr>
        </tbody>
    </table>
    
@stop