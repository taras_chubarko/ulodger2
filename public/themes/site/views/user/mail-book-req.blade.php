@extends('site::mail.main')

@section('title')
    Contact
@stop

@section('content')
    <p><b>name:</b> {{ $request->name }}</p>
    <p><b>email:</b> {{ $request->email }}</p>
    <p><b>subject:</b> {{ $request->subject }}</p>
    <p><b>message:</b> {{ $request->message }}</p>
@stop