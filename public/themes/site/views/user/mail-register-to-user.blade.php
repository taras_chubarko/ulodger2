@extends('site::mail.main')

@section('title')
Thank you for registering!
@stop

@section('content')
    <p>Hello!</p>
    <br>
    <p>Congratulations on successful registration on the site {{ config('app.name') }}.</p>
    <br>
    <p>your login: {{ $user->email }}</p>
    <p>your password: {{ $request->password }}</p>
    <br>
@stop