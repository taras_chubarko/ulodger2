@extends('site::mail.main')

@section('title')
Password recovery
@stop

@section('content')
    <p>Hello!</p>
    <br>
    <p>To recover your password on the site {{ config('app.name') }}. Follow the link</p>
    <br>
    <p><a href="{{ $link }}">{{ $link }}</a></p>
    <br>
@stop