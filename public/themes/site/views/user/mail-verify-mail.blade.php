@extends('site::mail.main')

@section('title')
Verification of mail
@stop

@section('content')
    <p>Hello!</p>
    <br>
    <p>To verify mail on the site {{ config('app.name') }}. Follow the link</p>
    <br>
    <p><a href="{{ $link }}">{{ $link }}</a></p>
    <br>
@stop