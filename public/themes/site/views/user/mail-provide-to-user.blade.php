@extends('site::mail.main')

@section('title')
    ID status updated
@stop

@section('content')
    @if($provide->status == 1)
         <p><b>Status:</b> Congratulations! Your ID verification was successful. You can now start requesting lodging. <br><br>Sincerely,<br> Ulodger team.</p>
    @else
        <p><b>Status:</b> Unfortunately your ID verification was not successful. Please check your Profile picture and ID to ensure they match. Once confirmed please resubmit your request. <br><br>Sincerely, <br>Ulodger team.</p>
    @endif

    @if($provide->comment && $provide->comment != '')
        <p><b>Comment:</b> {{ $provide->comment }}</p>
    @endif
@stop