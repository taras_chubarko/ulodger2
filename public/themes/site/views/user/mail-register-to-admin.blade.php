@extends('site::mail.main')

@section('title')
New registered user
@stop

@section('content')
<p><b>E-mail:</b> {{ $user->email }}</p>
@stop