@extends('site::mail.main')

@section('title')
New provide id
@stop

@section('content')
<p><b>Provide ID:</b> {{ $provide->id }}</p>
<p><b>E-mail:</b> {{ $user->email }}</p>
<p>To confirm, go to the Admin panel</p>
@stop