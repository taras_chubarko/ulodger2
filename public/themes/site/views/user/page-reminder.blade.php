@extends('site::layouts.master')

@section('content')
 <div class="container">
   <h1 class="title text-center">{{ MetaTag::set('title', 'Восстановление пароля') }}</h1>
   @if($reminder)
      @if($reminder->completed == 0)
         <reminer></reminer>
      @else
         <div class="alert alert-danger">
            <strong>Ошибка!</strong> Код уже использован.
         </div>
      @endif
   @else
      <div class="alert alert-danger">
         <strong>Ошибка!</strong> Код не существует.
      </div>
 @endif
 </div>
@stop

@section('script')
   @if($reminder)
   <script>
      var codeReminder = '{{ $reminder->code }}';
   </script>
   @endif
@stop
