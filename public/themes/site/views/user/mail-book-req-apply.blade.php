@extends('site::mail.main')

@section('title')
    Your Lodging Request Status.
@stop

@section('content')
    <p><b>Status:</b> Congratulations! Your lodging request has been approved by the landlord. Now it is time to make the payment. Please log in to your account and make the payment to secure your lodging. Sincerely, Ulodger Team.</p>
@stop