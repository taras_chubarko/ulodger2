<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ MetaTag::get('title') }} | {{ config('app.name') }}</title>
    <link href="/favicon.ico" type="image/x-icon" rel="icon" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link href="{{ mix('/themes/site/assets/css/all.css') }}" rel="stylesheet">
    <link href="{{ mix('/themes/site/assets/css/app.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div id="app">
<soc></soc>
</div>


<script src="{{ mix('/themes/site/assets/js/app.js') }}"></script>
<script>
//    function test() {
//        return 'dfgsdfg';
//    }
var RunCallbackFunction = function() { };
    //window.close();
</script>
</body>
</html>
