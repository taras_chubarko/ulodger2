@extends('site::layouts.master')

@section('content')
    @if($user = Sentinel::check())
        <auth :user="{{ $user }}"></auth>
    @else
        <auth></auth>
    @endif
    <router-view></router-view>
    <vue-progress-bar></vue-progress-bar>
    <login></login>
    <notifications classes="cp"/>
    
@stop