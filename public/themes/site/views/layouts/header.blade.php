<nav class="navbar navbar-header-nf" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse-nav-front">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Ulodger</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="collapse-nav-front">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="">Host</a></li>
                <li><a href="">Operations</a></li>
                <li><a href="">About</a></li>
                @if(Sentinel::check())
                    <li>@include('site::user.block-user', ['user' => Sentinel::getUser()])</li>
                @else
                    <li><a href=""><i class="fa fa-user-plus" aria-hidden="true"></i>&ensp;Sign up</a></li>
                    <li><a href=""><i class="fa fa-lock" aria-hidden="true"></i>&ensp;Login</a></li>
                @endif
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>