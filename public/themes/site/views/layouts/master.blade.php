<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ MetaTag::get('title') }} | {{ config('app.name') }}</title>
    <link href="/favicon.ico" type="image/x-icon" rel="icon" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link href="{{ mix('/themes/site/assets/css/all.css') }}" rel="stylesheet">
    <link href="{{ mix('/themes/site/assets/css/app.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div id="app">
    
        <router-view></router-view>
        <vue-progress-bar></vue-progress-bar>
        <notifications classes="cp"/>
        
    </div>
    @if (env('APP_ENV')=='production')
		<script>
			if (location.protocol !== "https:") location.protocol = "https:";
		</script>
	@endif
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-D9ab5aN_5JZGP05u5izNtgvS0c-gezI&libraries=places&language=en"></script>
    <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>
    <script src="https://unpkg.com/esri-leaflet@2.1.1"></script>
    <script src="https://unpkg.com/esri-leaflet-geocoder@2.2.8"></script>
    <script src="//ulogin.ru/js/ulogin.js?lang=en"></script>
    <script src="{{ mix('/themes/site/assets/js/app.js') }}"></script>
    <script src="/themes/lib/infobox.js"></script>
    {{--<script src="/themes/lib/jquery.dump.js"></script>--}}
        
   <!-- <script src="{{ mix('/themes/site/assets/js/all.js') }}"></script>-->
        
    @yield('script')
</body>

</html>
