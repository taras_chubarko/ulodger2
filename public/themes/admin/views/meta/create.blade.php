@extends('admin::layouts.master')

@section('content')
    
<div class="row">
    <div class="col-sm-12">
        <h4 class="pull-left page-title">{{ MetaTag::set('title', 'Создать мета-тег') }}</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="/admin">Админка</a></li>
            <li><a href="#">Настройки</a></li>
            <li><a href="/admin/meta">Мета-теги</a></li>
            <li class="active">Создать мета-тег</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {!! Form::open(array('route' => 'admin.meta.store', 'role' => 'form', 'class' => 'form', 'id' => 'page-form')) !!}
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('entity') ? ' has-error' : '' }}">
                                        <label for="entity">Тип материала*</label>
                                        {!! Form::select('entity', config('meta.entity'), null, array('class' => 'form-control selectpicker', 'title' => '- Выбрать -')) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="entity_id">Ключ материала</label>
                                        {!! Form::text('entity_id', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('robots') ? ' has-error' : '' }}">
                                        <label for="robots">Индексация*</label>
                                        {!! Form::select('robots', config('meta.robots'), null, array('class' => 'form-control selectpicker', 'title' => '- Выбрать -')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title">Мета - title*</label>
                                {!! Form::text('title', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group{{ $errors->has('keywords') ? ' has-error' : '' }}">
                                <label for="keywords">Мета - keywords*</label>
                                {!! Form::text('keywords', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description">Мета - description*</label>
                                {!! Form::textarea('description', null, array('class' => 'form-control', 'rows' => 5)) !!}
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                        </div>
                        <!-- /.box-footer-->
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop