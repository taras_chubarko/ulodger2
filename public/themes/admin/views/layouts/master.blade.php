<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="shortcut icon" href="/favicon.ico">
        <title>{{ MetaTag::get('title') }} | Admin</title>
	
	<!-- Base Css Files -->
        <link href="/themes/admin/assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="/themes/admin/assets/css/adminApp.css" rel="stylesheet" />

        <!-- Font Icons -->
        <link href="/themes/admin/assets/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="/themes/admin/assets/assets/ionicon/css/ionicons.min.css" rel="stylesheet" />
        <link href="/themes/admin/assets/css/material-design-iconic-font.min.css" rel="stylesheet">

        <!-- animate css -->
        <link href="/themes/admin/assets/css/animate.css" rel="stylesheet" />

        <!-- Waves-effect -->
        <link href="/themes/admin/assets/css/waves-effect.css" rel="stylesheet">
        <!-- Custom Files -->
        <link href="/themes/admin/assets/css/helper.css" rel="stylesheet" type="text/css" />
        <link href="/themes/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
	<link href="/themes/admin/assets/assets/sweet-alert/sweet-alert.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    
        <script src="/themes/admin/assets/js/modernizr.min.js"></script>
	<link href="/themes/admin/assets/assets/notifications/notification.css" rel="stylesheet" />
	<link rel="stylesheet" href="/themes/admin/assets/assets/magnific-popup/magnific-popup.css"/>
	
	@yield('style')
	
    </head>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            <!-- Top Bar Start -->
            <div class="topbar">
                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="/" class="logo">
			<i class="fa fa-wrench" aria-hidden="true"></i>
			<span>{{ config('app.name') }} </span></a>
                    </div>
                </div>
                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left">
                                <i class="fa fa-bars"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>
                           
                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li class="hidden-xs">
                                    <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="md md-crop-free"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img src="{{ config('app.url') }}/avatar/40x40/{{ Sentinel::getUser()->id }}" alt="user-img" class="img-circle"> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('user.slug', Sentinel::getUser()->slug) }}"><i class="md md-face-unlock"></i> Profile</a></li>
                                        <li><a href="{{ route('logout') }}"><i class="md md-settings-power"></i> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div class="user-details">
                        <div class="pull-left">
                            <img src="{{ config('app.url') }}/avatar/40x40/{{ Sentinel::getUser()->id }}" alt="" class="thumb-md img-circle">
                        </div>
                        <div class="user-info">
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{ Sentinel::getUser()->fullName }} <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('user.slug', Sentinel::getUser()->slug) }}">
                                            <i class="md md-face-unlock"></i> Profile
                                            <div class="ripple-wrapper"></div>
                                        </a>
                                    </li>
                                    <li><a href="{{ route('logout') }}"><i class="md md-settings-power"></i> Logout</a></li>
                                </ul>
                            </div>
                            <p class="text-muted m-0">{{ Sentinel::getUser()->roles->first()->name }}</p>
                        </div>
                    </div>
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        @include('admin::layouts.menu-left')
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End --> 
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        @yield('content')
                    </div>
                    <!-- container -->
                </div>
                <!-- content -->
                <footer class="footer text-right">
                    {{ date('Y') }} © skype: hummer_t
                </footer>
            </div>
		@include('footer')
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
           
        </div>
	@yield('footer')
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
       <!-- <script src="/themes/admin/assets/js/jquery.min.js"></script>
        <script src="/themes/admin/assets/js/bootstrap.min.js"></script>-->
	<script src="/themes/admin/assets/js/adminApp.js"></script>
        <script src="/themes/admin/assets/js/waves.js"></script>
        <script src="/themes/admin/assets/js/wow.min.js"></script>
        <script src="/themes/admin/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="/themes/admin/assets/js/jquery.scrollTo.min.js"></script>
        <script src="/themes/admin/assets/assets/jquery-detectmobile/detect.js"></script>
        <script src="/themes/admin/assets/assets/fastclick/fastclick.js"></script>
        <script src="/themes/admin/assets/assets/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="/themes/admin/assets/assets/jquery-blockui/jquery.blockUI.js"></script>
	<script src="/themes/admin/assets/js/jquery.app.js"></script>
        <!-- CUSTOM JS -->
	<script src="/themes/admin/assets/assets/notifications/notify.min.js"></script>
        <script src="/themes/admin/assets/assets/notifications/notify-metro.js"></script>
        <script src="/themes/admin/assets/assets/notifications/notifications.js"></script>
	<script src="/themes/admin/assets/assets/sweet-alert/sweet-alert.min.js"></script>
	<script src="/themes/admin/assets/assets/sweet-alert/sweet-alert.init.js"></script>
	<script type="text/javascript" src="/themes/admin/assets/assets/magnific-popup/magnific-popup.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/i18n/defaults-ru_RU.min.js"></script>
        <script type="text/javascript" src="/tinymce/tinymce.min.js"></script>
	<script type="text/javascript" src="/tinymce/tinymce_editor.js"></script>
	<script type="text/javascript">
	    editor_config.selector = ".editor";
	    editor_config.path_absolute = "{{ config('app.url') }}/";
	    tinymce.init(editor_config);
	</script>
	@include('admin::alert.alert')
	@yield('script')
	<script src="/themes/lib/jquery.dump.js"></script>
	<script src="/themes/admin/assets/js/admin.js"></script>
	
    </body>
</html>