<ul class="sidebar-menu">
    <li>
        <!--'CX7v3BUdGZ3RyJO9BLxM' => 'Control panel',-->
        <a class="waves-effect active" href="/admin"><i class="md md-home"></i><span> Admin Panel</span></a>
    </li>
    <li class="has_sub">
        <!--'dcK2nvj8KhbpMRbZBjCZ' => 'Content',-->
        <a class="waves-effect" href="#!"><i class="fa fa-file-text"></i> <span>Content</span><span class="pull-right"><i class="md md-add"></i></span></a>
        <ul class="treeview-menu">
            
        </ul>
    </li>
    <li class="has_sub">
        <!--'yZTG3VvGKNPMLnXKydn5' => 'Structure',-->
        <a class="waves-effect" href="#"><i class="fa fa-cube"></i> <span>Structure</span><span class="pull-right"><i class="md md-add"></i></span></a>
        <ul class="treeview-menu">
            <li><a href="{{ route('admin.taxonomy.index') }}"><span>Taxonomy</span></a></li>
        </ul>
    </li>
    <!--'pD6LrRHBV8MUgnPwg91U' => 'Users',-->
    <li class="has_sub">
        <!--'yZTG3VvGKNPMLnXKydn5' => 'Structure',-->
        <a class="waves-effect" href="#"><i class="fa fa-users"></i> <span>Users</span><span class="pull-right"><i class="md md-add"></i></span></a>
        <ul class="treeview-menu">
            <li><a href="{{ route('admin.users.index') }}"><span>Users List</span></a></li>
            <li><a href="{{ route('admin.users.provide') }}"><span>Users Provide</span></a></li>
        </ul>
    </li>
    
</ul>