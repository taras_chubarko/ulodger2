<script>
    /*
    * Загрузка картинки
    */
   ;
   (function ($) {
       if ($('#images-field').length) {
           $(document).ready(function () {
                
                function upload_images() {
                    $('#images-field').fileinput({
                        maxFileCount: 12,
                        allowedFileExtensions : ['png', 'jpg', 'jpeg', 'gif'],
                        uploadUrl: '/filem/upload/images',
                        browseClass: "btn btn-primary col-md-4",
                        browseLabel: "Добавить фото",
                        browseIcon: '',
                        showCaption: false,
                        showRemove: false,
                        showUpload: false,
                        uploadAsync: true,
                        language: 'ru',
                        showPreview: false,
                        layoutTemplates:{
                            progress: '',
                        },
                        slugCallback: function (filename){
                            return filename;
                        },
                        uploadExtraData: function (previewId, index) {
                            var obj = {};
                            obj['name'] = '{{ $name }}';
                            obj['uri'] = '{{ $uri }}';
                            obj['actions'] = '{{ $actions }}';
                            return obj;
                        },
                    }).on('filebatchselected', function(event, files) {
                        $('body').append('<div id="ajax-load"></div>');
                        var qty = $('.uploader-item').length; 
                        var qty2 = files.length; 
                        var qty3 = qty + qty2;
                        if (qty3 <= 12) {
                            $('#images-field').fileinput("upload"); 
                        }
                        else
                        {
                            $.Notification.autoHideNotify('error','top center', 'Ошибка!', 'Перевышен лимит по фото.');
                        }
                    }).on('fileuploaded', function(event, data, previewId, index) {
                       $('#images-widget').append(data.response.result);
                       delimages();
                       $('#ajax-load').remove();
                    }).on('fileuploaderror', function(event, data, msg) {
                        $.Notification.autoHideNotify('error','top center', 'Ошибка!', msg);
                        $('#ajax-load').remove();
                    });
                    
                } upload_images();
                
                /*
                *
                */
                function delimages() {
                    $('.img-delete').on('click', function(){
                        var self = $(this);
                        $.post(self.attr('href'), function(){
                            self.parent().remove();
                        })
                        return false;
                    });
                } delimages();
                
           });
       }
   })(jQuery);
</script>