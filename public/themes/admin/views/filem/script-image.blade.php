<script>
    /*
    * Загрузка картинки
    */
   ;
   (function ($) {
       if ($('#image-field').length) {
           $(document).ready(function () {
                
                function upload_image() {
                    $('#image-field').fileinput({
                        maxFileCount: 1,
                        allowedFileExtensions : ['png', 'jpg', 'jpeg', 'gif'],
                        uploadUrl: '/filem/upload/image',
                        uploadAsync: true,
                        language: 'ru',
                        showPreview: false,
                        uploadExtraData: function (previewId, index) {
                            var obj = {};
                            obj['name'] = '{{ $name }}';
                            obj['uri'] = '{{ $uri }}';
                            obj['actions'] = '{{ $actions }}';
                            return obj;
                        },
                    }).on('fileuploaded', function(event, data, previewId, index) {
                        $('#image-widget').html(data.response.result);
                        $('.kv-upload-progress').remove();
                        mb();
                        delimage();
                        upload_image();
                    });
                } upload_image();
                
                function mb() {
                    if ($('#image-widget .image').length) {
                        $('#image-widget').css('margin-bottom', 15);
                    }
                    else
                    {
                        $('#image-widget').css('margin-bottom', 0);
                    }
                } mb();
                /*
                *
                */
                function delimage() {
                    $('.image-delete').on('click', function(){
                        var self = $(this);
                        $.post(self.attr('href'), function(){
                            self.parent().remove();
                        })
                        mb();
                        return false;
                    });
                } delimage();
                
           });
       }
   })(jQuery);
</script>