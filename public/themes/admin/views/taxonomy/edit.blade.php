@extends('admin::layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
	<h4 class="pull-left page-title">{{ MetaTag::set('title', 'Редактировать словарь') }}</h4>
	<ol class="breadcrumb pull-right">
	    <li><a href="/admin">Админка</a></li>
	    <li><a href="#">Структура</a></li>
            <li><a href="/admin/taxonomy">Словари таксономии</a></li>
	    <li class="active">Редактировать словарь</li>
	</ol>
    </div>
</div>
<div class="row">
    <!-- Basic example -->
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                {!! Form::open(array('route' => ['admin.taxonomy.update', $taxonomy->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form')) !!}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Назание меню *</label>
                        {!! Form::text('name', $taxonomy->name, array('class' => 'form-control')) !!}
                    </div>
                        
                    <div class="form-group">
                       {!! Form::submit('Сохранить', array('class' => 'btn btn-primary btn-flat')) !!}
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- panel-body -->
        </div>
        <!-- panel -->
    </div>
    <!-- col-->
</div>

@stop