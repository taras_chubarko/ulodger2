@extends('admin::layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
	<h4 class="pull-left page-title">{{ MetaTag::set('title', 'Создать термин таксономии') }}</h4>
	<ol class="breadcrumb pull-right">
	    <li><a href="/admin">Админка</a></li>
	    <li><a href="#">Структура</a></li>
            <li><a href="/admin/taxonomy">Словари таксономии</a></li>
	    <li class="active">Создать термин таксономии</li>
	</ol>
    </div>
</div>
<div class="row">
    <!-- Basic example -->
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                {!! Form::open(array('route' => 'admin.term.store', 'role' => 'form', 'class' => 'form')) !!}
                    
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Название *</label>
			{!! Form::text('name', null, array('class' => 'form-control')) !!}
                    </div>
			
                    <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
                        <label for="parent_id">Родитель *</label>
                        {!! Form::select('parent_id', TaxonomyTerm::itemsArray(Request::get('taxonomy_id')), (Request::get('term')) ? Request::get('term') : null, array('class' => 'form-control selectpicker')) !!}
                    </div>
			
		    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description">Описание</label>
			{!! Form::textarea('description', null, array('class' => 'form-control', 'rows' => 3)) !!}
                    </div>
		    
                         
                    <div class="form-group">
                        <label for="order">Позиция в списке</label>
                        {!! Form::selectRange('sort', 0, 50, 0, array('class' => 'form-control selectpicker')) !!}
                    </div>
			
                    <div class="form-group">
                        {!! Form::hidden('taxonomy_id', Request::get('taxonomy_id')) !!}
                        {!! Form::submit('Сохранить', array('class' => 'btn btn-primary btn-flat')) !!}
                    </div>
                    
                {!! Form::close() !!}
            </div>
            <!-- panel-body -->
        </div>
        <!-- panel -->
    </div>
    <!-- col-->
</div>

@stop