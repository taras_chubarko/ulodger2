<ol class="dd-list">
    @foreach($lists as $list)
        @if($list->children->count())
            <li class="dd-item dd3-item" data-id="{{ $list->id }}">
                <div class="dd-handle dd3-handle"></div>
                <div class="dd3-content">
                    <a href="">{{ $list->name }} :: {{ $list->id }} {!! $list->chk !!}</a>
                    @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.delete'))
                    <a
                    href="{{ route('admin.term.destroy', $list->id) }}" 
                    data-toggle="tooltip"
                    data-placement="top"
                    title="Удалить"
                    data-msg-error="Вы не сможете восстановить этот термин!" data-msg-success="Ваш термин удален."
                    class="eac delete pull-right text-danger sa-warning"><i class="fa fa-fw fa-trash-o"></i></a>
                    @endif
                    @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.edit'))
                    <a href="{{ route('admin.term.edit', $list->id) }}" class="eac pull-right" data-toggle="tooltip" data-placement="top" title="Редактировать"><i class="fa fa-fw fa-edit"></i></a>    
                    @endif
                    <a href="{{ route('admin.term.create') }}?taxonomy_id={{ $list->taxonomy_id }}&term={{ $list->id }}" class="eac pull-right text-success" data-toggle="tooltip" data-placement="top" title="Новый"><i class="fa fa-fw fa-file-text-o"></i></a>
                </div>
                @include('admin::taxonomy.terms.lists', ['lists' => $list->children])
            </li>
        @else
            <li class="dd-item dd3-item" data-id="{{ $list->id }}">
                <div class="dd-handle dd3-handle"></div>
                <div class="dd3-content">
                    <a href="">{{ $list->name }} :: {{ $list->id }}  {!! $list->chk !!}</a>
                    @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.delete'))
                    <a
                    href="{{ route('admin.term.destroy', $list->id) }}" 
                    data-toggle="tooltip"
                    data-placement="top"
                    title="Удалить"
                    data-msg-error="Вы не сможете восстановить этот термин!" data-msg-success="Ваш термин удален."
                    class="eac delete pull-right text-danger sa-warning"><i class="fa fa-fw fa-trash-o"></i></a>
                    @endif
                    @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.edit'))
                    <a href="{{ route('admin.term.edit', $list->id) }}" class="eac pull-right" data-toggle="tooltip" data-placement="top" title="Редактировать"><i class="fa fa-fw fa-edit"></i></a>    
                    @endif
                    <a href="{{ route('admin.term.create') }}?taxonomy_id={{ $list->taxonomy_id }}&term={{ $list->id }}" class="eac pull-right text-success" data-toggle="tooltip" data-placement="top" title="Новый"><i class="fa fa-fw fa-file-text-o"></i></a>
                </div>
            </li>
        @endif
    @endforeach
</ol>