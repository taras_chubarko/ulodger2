@extends('admin::layouts.master')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <h4 class="pull-left page-title">{{ MetaTag::set('title', 'Блоки') }}</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="/admin">Админка</a></li>
            <li><a href="#">Структура</a></li>
            <li class="active">Блоки</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ route('admin.block.create') }}" class="btn btn-success waves-effect waves-light" id="addToTable">Создать <i class="fa fa-plus"></i></a>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        @if($blocks->count())
                        <table id="datatable-editable" class="table table-bordered table-striped table-condensed dataTable no-footer">
                            <thead>
                                <tr>
                                    <th width="20">#</th>
                                    <th>Название</th>
                                    <th width="150">Дата создания</th>
                                    <th width="120">Статус</th>
                                    <th width="70" class="text-center"><i class="fa fa-tasks" aria-hidden="true"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($blocks as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</a></td>
				    <td>{{ $item->created_at->format('d.m.Y') }}</td>
                                    <td>{!! $item->fieldStatus->label !!}</td>
                                    <td class="actions">
                                        <a class="on-default edit-row" href="{{ route('admin.block.edit', $item->id) }}"><i class="fa fa-pencil"></i></a>
                                        <a class="on-default remove-row sa-warning" href="{{ route('admin.block.destroy', $item->id) }}" data-msg-error="Вы не сможете восстановить этот блок!" data-msg-success="Блок удален."><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $blocks->render() !!}
                        @else
                        <p>Нет блоков.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop