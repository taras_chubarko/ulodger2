<div id="panel-modal-filter" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                    <h3 class="panel-title">Фильтр</h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('url' => Request::url(), 'role' => 'form', 'class' => 'form', 'method' => 'GET')) !!}
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="from_user">ФИО исполнителя</label>
                                    {!! Form::select('user_id', User::getArray(), Request::get('from_user'), array('class' => 'form-control selectpicker', 'title' => '- выбрать -', 'data-live-search' => true, 'data-size' => 5)) !!}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="jobs_id">Задание</label>
                                    {!! Form::select('jobs_id', Jobs::getArray(), Request::get('to_user'), array('class' => 'form-control selectpicker', 'title' => '- выбрать -', 'data-live-search' => true, 'data-size' => 5)) !!}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="status">Статус</label>
                                    {!! Form::select('status', config('bids.status'), Request::get('status'), array('class' => 'form-control selectpicker', 'title' => '- выбрать -')) !!}
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                {!! Form::submit('Применить', array('class' => 'btn btn-primary')) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
    
@section('script')
    @parent
    <script src="/themes/libs/select.category.js"></script>
@stop