@extends('admin::layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h4 class="pull-left page-title">{{ MetaTag::set('title', 'Users Provide') }}</h4>
            <ol class="breadcrumb pull-right">
                <li><a href="/admin">Admin panel</a></li>
                <li><a href="#">Users</a></li>
                <li class="active">Users Provide</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                    <!-- include('user::admin.users.tabs')
                     include('user::admin.users.filter')-->
                </div>
                <div class="panel-body">
                    <!-- include('admin::user.tabs')-->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            @if($users->count())
                                <table id="datatable-editable" class="table table-bordered table-striped dataTable no-footer">
                                    <thead>
                                    <tr>
                                        <th width="10">ID</th>
                                        <th width="120">Photo</th>
                                        <th>Nmae</th>
                                        <th>E-mail</th>
                                        <th width="150">Date</th>
                                        <th width="120">Status</th>
                                        <th width="100">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $item)
                                        @if($item->user)
                                            <tr>
                                                <td>{{ $item->id }}</td>
                                                <td>
                                                    @if($item->photo)
                                                        <a href="/uploads/provide/{{ $item->photo->filename }}" class="image-popup">
                                                            <img src="{{ config('app.url') }}/filem/provide/40x40/{{ $item->photo->filename }}" alt="">
                                                        </a>
                                                    @endif
                                                </td>
                                                <td><a href="{{ route('user.slug', $item->user->slug) }}" target="_blank">{{ $item->user->fullName }}</a></td>
                                                <td>{{ $item->user->email }}</td>
                                                <td>{{ $item->created_at->format('d.m.Y') }}</td>
                                                <td>{!! $item->flag !!}</td>
                                                <td>
                                                    <a href="{{ route('admin.users.provide.edit', $item->id) }}" class="on-default edit-row m-r-10"><i class="fa fa-pencil"></i></a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $users->render() !!}
                            @else
                                <p>No provide found.</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop