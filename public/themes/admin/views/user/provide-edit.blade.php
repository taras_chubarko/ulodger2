@extends('admin::layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <h4 class="pull-left page-title">{{ MetaTag::set('title', 'Provide user') }}</h4>
	<ol class="breadcrumb pull-right">
	    <li><a href="/admin">Admin panel</a></li>
	    <li><a href="/admin/users">Users</a></li>
            <li><a href="/admin/users/provide/list">Users Provide</a></li>
	    <li class="active">Provide user</li>
	</ol>
    </div>
</div>
    
<div class="row">
    <!-- Basic example -->
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <img src="/uploads/provide/{{ $provide->photo->filename }}" alt="" class="img-responsive">
                    </div>
                    <div class="col-lg-6">
                        {!! Form::open(array('route' => ['admin.users.provide.edit', $provide->id], 'role' => 'form', 'class' => 'form')) !!}
                            <div class="form-group">
                                <label for="name">Nmae</label>
                                {!! Form::text('name', $provide->user->fullName, array('class' => 'form-control', 'disabled')) !!}
                            </div>
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                {!! Form::text('email', $provide->user->email, array('class' => 'form-control', 'disabled')) !!}
                            </div>
                            <div class="form-group">
                                <label for="comment">Comment</label>
                                {!! Form::textarea('comment', $provide->comment, array('class' => 'form-control', 'rows' => 3)) !!}
                            </div>
                                
                            <div class="form-group">
                                {!! Form::hidden('status', 0) !!}
                                <div class="checkbox checkbox-success">
                                    {!! Form::checkbox('status', 1, ($provide->status == 1) ? true : false, ['id' => 'status']) !!}
                                    <label for="status">
                                        Provide ID
                                    </label>
                                </div>
                            </div>
                                
                            <div class="form-group">
                                <div class="checkbox checkbox-primary">
                                    {!! Form::checkbox('send_user_msg', 1, false, ['id' => 'send_user_msg']) !!}
                                    <label for="send_user_msg">
                                        Send user message about Provide ID status.
                                    </label>
                                </div>
                            </div>
                                
                            <div class="form-group">
                                {!! Form::submit('Save', array('class' => 'btn btn-primary btn-flat')) !!}
                            </div>
                        
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- panel-body -->
        </div>
        <!-- panel -->
    </div>
    <!-- col-->
</div>
@stop