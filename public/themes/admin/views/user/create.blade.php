@extends('admin::layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <h4 class="pull-left page-title">{{ MetaTag::set('title', 'Создать пользователя') }}</h4>
	<ol class="breadcrumb pull-right">
	    <li><a href="/admin">Панель управления</a></li>
	    <li><a href="/admin/users">Пользователи</a></li>
	    <li class="active">Создать пользователя</li>
	</ol>
    </div>
</div>
    
<div class="row">
    <!-- Basic example -->
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body">
                {!! Form::open(array('route' => 'admin.users.store', 'role' => 'form', 'class' => 'form', 'files'=>'true')) !!}
		    
                    <div class="row">
			
			<div class="col-lg-12">
			    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
				<label for="first_name">Имя</label>
				{!! Form::text('first_name', null, array('class' => 'form-control')) !!}
			    </div>
			</div>
			    
			<div class="col-lg-12">
			    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
				<label for="last_name">Фамилия</label>
				{!! Form::text('last_name', null, array('class' => 'form-control')) !!}
			    </div>
			</div>
			
                        <div class="col-lg-12">
			    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				<label for="email">E-mail</label>
				{!! Form::email('email', null, array('class' => 'form-control')) !!}
			    </div>
			</div>
			    
			<div class="col-lg-12">
			    <div class="form-group{{ $errors->has('telefon') ? ' has-error' : '' }}">
				<label for="telefon">Телефон</label>
				{!! Form::text('telefon', null, array('class' => 'form-control')) !!}
			    </div>
			</div>
			    
			<div class="col-lg-12">
			    <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
				<label for="role_id">Роль</label>
				{!! Form::select('role_id[]', User::getRolesArray(), null, array('class' => 'form-control selectpicker', 'title' => '- choose -', 'multiple' => true)) !!}
			    </div>
			</div>
			    
			<div class="col-lg-12">
			    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
				<label for="password">Пароль</label>
				{!! Form::password('password', array('class' => 'form-control')) !!}
			    </div>
			</div>
                        <div class="col-lg-12">
			    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
				<label for="password_confirmation">Повтор пароля</label>
				{!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
			    </div>
			</div>
		    </div>
		    {!! Form::submit('Сохранить', array('class' => 'btn btn-sm btn-success')) !!}
		{!! Form::close() !!}
            </div>
            <!-- panel-body -->
        </div>
        <!-- panel -->
    </div>
    <!-- col-->
</div>
@stop