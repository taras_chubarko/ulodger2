$(document).ready(function() {
    AddFoto.init();
});
//
AddFoto = {
    init: function()
    {
        this.remove();
        $('.add-foto').on('click', function(){
            $('#input-foto').click();
        });
        //
        $('#input-foto').on('change', function(){
            AddFoto.uploadFoto(this.files);
        });
    },
    uploadFoto: function(files)
    {
        var form_data = new FormData();
        
        var countFiles = files.length;
        
        $.each(files, function(k, file){
            form_data.append('image', file);
            $.ajax({
                url: '/admin/products/upload/foto',
                dataType: 'json',
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                beforeSend:function(data){
                   $('.add-foto').html('<i class="fa fa-spinner fa-pulse" aria-hidden="true"></i> Добавить фото');
                },
                success: function(data){
                    if (countFiles == k+1) {
                        $('.add-foto').html('<i class="fa fa-camera" aria-hidden="true"></i> Добавить фото');
                        $.Notification.autoHideNotify('success','top center', 'Отлично', 'Все фото загружены!');
                    }
                    $('#foto-wrp').append(data.image);
                    AddFoto.remove();
                },
                error: function(data){
                    $('.add-foto').html('<i class="fa fa-camera" aria-hidden="true"></i> Добавить фото');
                }
            });
        });
    },
    remove: function()
    {
        $('.delete-foto').on('click', function(){
            var _this = $(this);
            
            $.ajax({
                url: _this.attr('href'),  
                type: 'post',
                beforeSend:function(data){
                    _this.parent().addClass('op');
                },
                success: function(data){
                    $.Notification.autoHideNotify('success','top center', 'Отлично', 'Фото удалено!');
                    _this.parent().remove();
                },
                error: function(data){
                    _this.parent().removeClass('op');
                }
            });
            return false;
        });
    }
    
};